<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * @property integer $id
 * @property string $name
 * @property integer $commission_amount
 * @property string $created_at
 * @property string $updated_at
 * @property CargoRequest[] $cargoRequests
 * @property DiscountCargoType[] $discountCargoTypes
 */
class CargoType extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'cargo_type';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'commission_amount', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cargoRequests()
    {
        return $this->hasMany('App\CargoRequest');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function discountCargoTypes()
    {
        return $this->hasMany('App\DiscountCargoType');
    }

    public static function getAll()
    {
        return CargoType::all();
    }

    public static function deleteCargoType(Request $request)
    {
        CargoType::where('id', $request->get('id'))->delete();
    }

    public static function addOrEdit(Request $request)
    {
        $cargoType = null;
        if (!isset($request->id)) {
            $cargoType = new CargoType();
        } else {
            $cargoType = CargoType::where('id', $request->get('id'))->first();
        }
        if (isset($request->name)) $cargoType->name = $request->get('name');
        if (isset($request->commission_amount)) $cargoType->commission_amount = $request->get('commission_amount');

        $cargoType->save();
        return $cargoType;

    }
}
