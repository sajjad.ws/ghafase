<?php

namespace App;

use App\Http\Helpers\FirebaseHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * @property integer $id
 * @property integer $operator_id
 * @property string $title
 * @property string $content
 * @property string $sent_at
 * @property boolean $for_carriers
 * @property boolean $for_messages
 * @property boolean $for_cargo_owners
 * @property string $created_at
 * @property string $updated_at
 * @property Operator $operator
 */
class Message extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'message';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['operator_id', 'title', 'content', 'sent_at', 'for_carriers', 'for_messages', 'for_cargo_owners', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function operator()
    {
        return $this->belongsTo('App\Operator');
    }

    public static function getCargoOwnerMessages()
    {
        $messages = Message::where('for_cargo_owners', 1)->get();
        return $messages;
    }

    public static function getDriverMessages()
    {
        $messages = Message::where('for_drivers', 1)->get();
        return $messages;
    }

    public static function getCarrierMessages()
    {
        $messages = Message::where('for_carriers', 1)->get();
        return $messages;
    }

    public static function getMarketerMessages()
    {
        $messages = Message::where('for_marketers', 1)->get();
        return $messages;
    }

    public static function removeByID($id)
    {
        Message::whereIn('id', $id)->delete();
    }

    //

    public static function getAll(Request $request)
    {
        $messages = Message::where('id', '>', 0);

        if (isset($request->start_time)) $messages->where('sent_at', '>', $request->get('start_time'));
        if (isset($request->end_time)) $messages->where('sent_at', '<', $request->get('end_time'));
        if (isset($request->user_id)) {
            $messages->where('user_id', $request->get('user_id'))->where('user_type', $request->get('user_type'));
        }

        return $messages->get();
    }

    public static function deleteMessage(Request $request)
    {
        Message::where('id', $request->get('id'))->delete();
    }

    public static function changeStatus(Request $request)
    {
        $message = Message::where('id', $request->get('id'))->first();
        if (isset($request->status)) {
            if ($request->get('status') === 0) $message->status = 1;
            else  $message->status = 0;
        };
        $message->save();
        return $message;
    }

    public static function addOrEdit(Request $request)
    {
        $message = null;
        if (!isset($request->id)) {
            $message = new Message();
        } else {
            $message = Message::where('id', $request->get('id'))->first();
        }
        if (isset($request->title)) $message->title = $request->get('title');
        if (!empty($request->get('content'))) $message->content = $request->get('content');
        if (isset($request->sent_at)) $message->sent_at = $request->get('sent_at');
        if (isset($request->status)) $message->status = $request->get('status');
        if (isset($request->for_carriers)) $message->for_carriers = $request->get('for_carriers');
        if (isset($request->for_drivers)) $message->for_drivers = $request->get('for_drivers');
        if (isset($request->for_cargo_owners)) $message->for_cargo_owners = $request->get('for_cargo_owners');
        if (isset($request->for_marketers)) $message->for_marketers = $request->get('for_marketers');
        if (isset($request->operator_id)) $message->operator_id = $request->get('operator_id');
        $message->save();
        if (!isset($request->id)) {
            $data = [
                'title' => $message->title,
                'content' => $message->content
            ];
            if ($message->for_carriers === true) FirebaseHelper::sendFcmNotificationMessage(Carrier::all()->pluck('push_notification_token'), $message->title, $message->content,config('fcmsettings.message'));
            if ($message->for_cargo_owners === true) FirebaseHelper::sendFcmNotificationMessage(CargoOwner::all()->pluck('push_notification_token'), $message->title, $message->content,config('fcmsettings.message'));
            if ($message->for_drivers === true) FirebaseHelper::sendFcmNotificationMessage(Driver::all()->pluck('push_notification_token'), $message->title, $message->content,config('fcmsettings.message'));
        }
        return $message;

    }
}
