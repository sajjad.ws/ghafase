<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * @property integer $id
 * @property string $title
 * @property string $start_time
 * @property string $end_time
 * @property boolean $discount_type
 * @property int $percent_value
 * @property boolean $for_carriers
 * @property boolean $for_drivers
 * @property boolean $for_cargo_owners
 * @property string $created_at
 * @property string $updated_at
 * @property DiscountCargoType[] $discountCargoTypes
 * @property DiscountVehicleGroup[] $discountVehicleGroups
 */
class Discount extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'discount';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['title', 'start_time', 'end_time', 'discount_type', 'percent_value', 'for_carriers', 'for_drivers', 'for_cargo_owners', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function discountCargoTypes()
    {
        return $this->hasMany('App\DiscountCargoType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function discountVehicleGroups()
    {
        return $this->hasMany('App\DiscountVehicleGroup');
    }

    //

    public static function getAll()
    {
        return Discount::all();
    }

    public static function deleteDiscount(Request $request) {
        Discount::where('id',$request->get('id'))->delete();
    }

    public static function changeStatus(Request $request) {
        $discount = Discount::where('id',$request->get('id'))->first();
        if (isset($request->status)){
            if ($request->get('status') === 0) $discount->status = 1;
            else  $discount->status = 0;
        };
        $discount->save();
        return $discount;
    }

    public static function addOrEdit(Request $request)
    {
        $discount = null;
        if (!isset($request->id)){
            $discount = new Discount();
        } else {
            $discount = Discount::where('id', $request->get('id'))->first();
        }
        if (isset($request->title)) $discount->title = $request->get('title');
        if (isset($request->start_time)) $discount->start_time = $request->get('start_time');
        if (isset($request->end_time)) $discount->end_time = $request->get('end_time');
        if (isset($request->discount_type)) $discount->discount_type = $request->get('discount_type');
        if (isset($request->percent_value)) $discount->percent_value = $request->get('percent_value');
        if (isset($request->for_carriers)) $discount->for_carriers = $request->get('for_carriers');
        if (isset($request->for_drivers)) $discount->for_drivers = $request->get('for_drivers');
        if (isset($request->for_cargo_owners)) $discount->for_cargo_owners = $request->get('for_cargo_owners');

        $discount->save();
        if (isset($request->vehicle)){
            $vehicles = $request->get('vehicle');
            for ($i = 0;$i < sizeof($vehicles);$i++){
                $discountVehicle = new DiscountVehicleGroup();
                $discountVehicle->discount_id = $discount->id;
                $discountVehicle->vehicle_group_id = $vehicles[$i]['id'];
                $discountVehicle->save();
            }
        }
        if (isset($request->cargo)){
            $cargos = $request->get('cargo');
            for ($i = 0;$i < sizeof($cargos);$i++) {
                $discountCargo = new DiscountCargoType();
                $discountCargo->discount_id = $discount->id;
                $discountCargo->cargo_type_id = $cargos[$i]['id'];
                $discountCargo->save();
            }
        }
        return $discount;
    }
}
