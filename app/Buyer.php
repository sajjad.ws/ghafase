<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

/**
 * @property integer $id
 * @property integer $city_id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone_number
 * @property string $sms_validation
 * @property string $status
 * @property string $email
 * @property string $email_verified_at
 * @property string $password
 * @property int $credit
 * @property string $api_token
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 * @property City $city
 * @property Order[] $orders
 */
class Buyer extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'phone_number', 'sms_validation', 'status', 'email', 'email_verified_at', 'validation_code', 'credit', 'city_id', 'api_token', 'push_notification_token', 'remember_token', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('App\Order');
    }


    //////////////////////


    public static function updateOrInsertByPhone($phoneNumber, $pushNotificationToken)
    {
        $buyer = Buyer::where('phone_number', $phoneNumber)->first();

        if (is_null($buyer)) {
            $buyer = new Buyer();
            $buyer->phone_number = $phoneNumber;

        }
        $buyer->push_notification_token = $pushNotificationToken;
        $buyer->api_token = self::generateToken();
        $buyer->validation_code = self::generateVerifyNumber();
        $buyer->save();

        return $buyer;
    }


    public static function generateToken()
    {
        $token = Str::random(60);
        $customer = Buyer::where('api_token', $token)->first();
        if (!is_null($customer)) {
            $token = self::generateToken();
        }
        return $token;
    }

    /**
     * generates verify number for sms verification
     * @return int
     */
    public static function generateVerifyNumber()
    {
        return mt_rand(20000, 99999);
    }


    public static function verify($phoneNumber, $tempCode)
    {
        $CargoOwner = Buyer::where('phone_number', $phoneNumber)->where('validation_code', $tempCode)->first();
        if (!is_null($CargoOwner)) {
            return $CargoOwner;
        } else {
            return false;
        }
    }


    public static function getProducts(Request $request)
{
    $products = Product::with('category','store')->has('category');

    if (isset($request->discount)) {
        if ($request->discount == "order") {
            $products->orderBy('discount', 'DESC');

        } else {
            $products->where('discount', $request->get('discount'))->orderBy('discount', 'DESC');

        }
    }

    if (isset($request->search)) {
        $products->where('name','like',"%". $request->get('search')."%");

    }


    return $products->get();

}

    public static function getStores(Request $request)
    {
        $stores = new Store();
/*
        if (isset($request->discount)) {
            if ($request->discount == "order") {
                $products->orderBy('discount', 'DESC');

            } else {
                $products->where('discount', $request->get('discount')->orderBy('discount', 'DESC'));

            }
        }*/

        if (isset($request->search)) {

            $stores = Store::where('name','like',"%". $request->get('search')."%");

        }

        return $stores->get();

    }

}
