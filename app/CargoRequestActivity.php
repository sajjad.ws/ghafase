<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $cargo_request_id
 * @property boolean $status_from
 * @property boolean $status_to
 * @property boolean $receiver_type
 * @property integer $receiver_id
 * @property string $created_at
 * @property string $updated_at
 * @property CargoRequest $cargoRequest
 */
class CargoRequestActivity extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'cargo_request_activity';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['cargo_request_id', 'status_from', 'status_to', 'receiver_type', 'receiver_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cargoRequest()
    {
        return $this->belongsTo('App\CargoRequest');
    }
}
