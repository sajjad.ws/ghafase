<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

/**
 * @property integer $id
 * @property integer $marketer_id
 * @property integer $city_id
 * @property string $first_name
 * @property string $last_name
 * @property string $company_name
 * @property string $national_code
 * @property string $address
 * @property float $latitude
 * @property float $longitude
 * @property string $postal_code
 * @property string $telephone_number
 * @property string $email
 * @property boolean $status
 * @property string $validation_code
 * @property integer $credit
 * @property string $created_at
 * @property string $updated_at
 * @property City $city
 * @property Marketer $marketer
 */
class CargoOwner extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cargo_owner';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['marketer_id', 'city_id', 'first_name', 'last_name', 'company_name', 'national_code', 'address', 'latitude', 'longitude', 'postal_code', 'telephone_number', 'email', 'status', 'validation_code', 'credit', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function marketer()
    {
        return $this->belongsTo('App\Marketer');
    }

    public static function check(Request $request){
        return CargoOwner::where('phone_number',$request->get('phone_number'))
            ->orWhere('national_code',$request->get('national_code'))->get()->count();
    }

    public static function searchByPhoneNumber(Request $request)
    {
        $owner = CargoOwner::where('phone_number', 'LIKE', '%' . $request->get('phone_number') . '%')
            ->select("cargo_owner.*", DB::raw("CONCAT(cargo_owner.first_name,' ',cargo_owner.last_name) as name"))->get();
        return $owner;
    }


    public static function updateOrInsertByPhone($phoneNumber, $pushNotificationToken)
    {
        $CargoOwner = CargoOwner::where('phone_number', $phoneNumber)->first();

        if (is_null($CargoOwner)) {
            $CargoOwner = new CargoOwner();
            $CargoOwner->phone_number = $phoneNumber;

        }
        $CargoOwner->push_notification_token = $pushNotificationToken;
        $CargoOwner->api_token = self::generateToken();
        $CargoOwner->validation_code = self::generateVerifyNumber();
        $CargoOwner->save();

        return $CargoOwner;
    }

    public static function generateToken()
    {
        $token = Str::random(60);
        $customer = CargoOwner::where('api_token', $token)->first();
        if (!is_null($customer)) {
            $token = self::generateToken();
        }
        return $token;
    }

    /**
     * generates verify number for sms verification
     * @return int
     */
    public static function generateVerifyNumber()
    {
        return mt_rand(20000, 99999);
    }

    public static function verify($phoneNumber, $tempCode)
    {
        $CargoOwner = CargoOwner::where('phone_number', $phoneNumber)->where('validation_code', $tempCode)->first();
        if (!is_null($CargoOwner)) {
            return $CargoOwner;
        } else {
            return false;
        }
    }


    public static function editProfile(Request $request)
    {
        $cargoOwner = CargoOwner::where('id', $request->User()->id)->first();

        if (isset($request->first_name)) $cargoOwner->first_name = $request->get('first_name');
        if (isset($request->last_name)) $cargoOwner->last_name = $request->get('last_name');
        if (isset($request->company_name)) $cargoOwner->company_name = $request->get('company_name');
        if (isset($request->national_code)) $cargoOwner->national_code = $request->get('national_code');
        if (isset($request->address)) $cargoOwner->address = $request->get('address');
        if (isset($request->latitude)) $cargoOwner->latitude = $request->get('latitude');
        if (isset($request->longitude)) $cargoOwner->longitude = $request->get('longitude');
        if (isset($request->postal_code)) $cargoOwner->postal_code = $request->get('postal_code');
        if (isset($request->phone_number)) $cargoOwner->phone_number = $request->get('phone_number');

        if (isset($request->telephone_number)) $cargoOwner->telephone_number = $request->get('telephone_number');
        if (isset($request->email)) $cargoOwner->email = $request->get('email');
        if (isset($request->status)) $cargoOwner->status = $request->get('status');
        if (isset($request->credit)) $cargoOwner->credit = $request->get('credit');
        if (isset($request->marketer_id)) $cargoOwner->marketer_id = $request->get('marketer_id');
        if (isset($request->city_id)) $cargoOwner->city_id = $request->get('city_id');

        $cargoOwner->save();
        return $cargoOwner;

    }


    public static function getCargoOwnerByID($cargoOwnerID)
    {
        $cargoOwner = CargoOwner::where('id', $cargoOwnerID)->first();
        return $cargoOwner;
    }

    public static function addByMarketer(Request $request)
    {
        $cargoOwner = new CargoOwner();
        if (isset($request->phone_number)) $cargoOwner->phone_number = $request->get('phone_number');
        if (isset($request->first_name)) $cargoOwner->first_name = $request->get('first_name');
        if (isset($request->last_name)) $cargoOwner->last_name = $request->get('last_name');
        if (isset($request->company_name)) $cargoOwner->company_name = $request->get('company_name');
        if (isset($request->national_code)) $cargoOwner->national_code = $request->get('national_code');
        if (isset($request->address)) $cargoOwner->address = $request->get('address');
        if (isset($request->latitude)) $cargoOwner->latitude = $request->get('latitude');
        if (isset($request->longitude)) $cargoOwner->longitude = $request->get('longitude');
        if (isset($request->postal_code)) $cargoOwner->postal_code = $request->get('postal_code');
        if (isset($request->telephone_number)) $cargoOwner->telephone_number = $request->get('telephone_number');
        if (isset($request->email)) $cargoOwner->email = $request->get('email');
        if (isset($request->status)) $cargoOwner->status = $request->get('status');
        if (isset($request->credit)) $cargoOwner->credit = $request->get('credit');
        if (isset($request->city_id)) $cargoOwner->city_id = $request->get('city_id');
        $cargoOwner->marketer_id = $request->User()->id;
        $cargoOwner->save();
        return $cargoOwner;
    }


    public static function getCargoOwnersByMarketer(Request $request)
    {
        $cargoOwners = CargoOwner::where('marketer_id', $request->User()->id);
        if (isset($request->city_id)) {
            $cargoOwners->where('city_id', $request->get('city_id'));
        }
        if (isset($request->word)) $cargoOwners->where(DB::raw('concat(first_name," ",last_name)') , 'LIKE' , "%{$request->get('word')}%");
        if (isset($request->name)) {
            $cargoOwners->where('first_name', $request->get('name'))
                ->orWhere('last_name', $request->get('name'));
        }
        $cargoOwners = $cargoOwners->get();
        if (sizeof($cargoOwners) > 0) {
            for ($i = 0; $i < sizeof($cargoOwners); $i++) {
                $cargoOwners[$i]['cargo_request_count'] = CargoRequest::where('owner_id',$cargoOwners[$i]['id'])
                    ->where('owner_type',config('owner.cargoOwner'))->get()->count();

            }
        }
        return $cargoOwners;
    }

    //

    public static function getAll(Request $request)
    {

        $cargoOwners = CargoOwner::with(['City']);

        if (isset($request->search)){
            $cargoOwners->where("first_name", 'LIKE', '%' . $request->get('search') . '%')
                ->orWhere("last_name", 'LIKE', '%' . $request->get('search') . '%')
                ->orWhere("phone_number", 'LIKE', '%' . $request->get('search') . '%')
                ->orWhere("national_code", 'LIKE', '%' . $request->get('search') . '%');
        }

        return $cargoOwners->get();
    }

    public static function deleteCargoOwner(Request $request)
    {
        CargoOwner::where('id', $request->get('id'))->delete();
    }

    public static function changeStatus(Request $request)
    {
        $cargoOwner = CargoOwner::where('id', $request->get('id'))->first();
        if (isset($request->status)) {
            if ($request->get('status') === 0) $cargoOwner->status = 1;
            else  $cargoOwner->status = 0;
        };
        $cargoOwner->save();
        return $cargoOwner;
    }

    public static function addOrEdit(Request $request)
    {
        $cargoOwner = null;
        if (!isset($request->id)) {
            $cargoOwner = new CargoOwner();
        } else {
            $cargoOwner = CargoOwner::where('id', $request->get('id'))->first();
        }
        if (isset($request->first_name)) $cargoOwner->phone_number = $request->get('phone_number');
        if (isset($request->first_name)) $cargoOwner->first_name = $request->get('first_name');
        if (isset($request->last_name)) $cargoOwner->last_name = $request->get('last_name');
        if (isset($request->company_name)) $cargoOwner->company_name = $request->get('company_name');
        if (isset($request->national_code)) $cargoOwner->national_code = $request->get('national_code');
        if (isset($request->address)) $cargoOwner->address = $request->get('address');
        if (isset($request->latitude)) $cargoOwner->latitude = $request->get('latitude');
        if (isset($request->longitude)) $cargoOwner->longitude = $request->get('longitude');
        if (isset($request->postal_code)) $cargoOwner->postal_code = $request->get('postal_code');
        if (isset($request->telephone_number)) $cargoOwner->telephone_number = $request->get('telephone_number');
        if (isset($request->email)) $cargoOwner->email = $request->get('email');
        if (isset($request->status)) $cargoOwner->status = $request->get('status');
        if (isset($request->credit)) $cargoOwner->credit = $request->get('credit');
        if (isset($request->marketer_id)) $cargoOwner->marketer_id = $request->get('marketer_id');
        if (isset($request->city_id)) $cargoOwner->city_id = $request->get('city_id');

        $cargoOwner->save();
        return $cargoOwner;

    }

    public static function getAllByMarketer(Request $request)
    {

        $cargoOwners = CargoOwner::with(['City']);
        $cargoOwners->where("marketer_id",$request->get("marketer_id"));

        if (isset($request->search)){
            $cargoOwners->where("first_name", 'LIKE', '%' . $request->get('search') . '%')
                ->orWhere("last_name", 'LIKE', '%' . $request->get('search') . '%')
                ->orWhere("phone_number", 'LIKE', '%' . $request->get('search') . '%');
        }
        $cargoOwners = $cargoOwners->get();
        if (sizeof($cargoOwners) > 0) {
            for ($i = 0; $i < sizeof($cargoOwners); $i++) {
                $cargoOwners[$i]['cargo_request'] = CargoRequest::where('owner_id', $cargoOwners[$i]['id'])->count();
            }
        }

        return $cargoOwners;
    }



    public static function sumCredit()
    {
        return CargoOwner::sum('credit');
    }

}
