<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * @property integer $id
 * @property string $name
 * @property boolean $cargo_owners_permission
 * @property boolean $drivers_permission
 * @property boolean $carriers_permission
 * @property boolean $marketers_permission
 * @property boolean $messages_permission
 * @property boolean $vehicle_permission
 * @property boolean $cargo_type_permission
 * @property boolean $discount_permission
 * @property boolean $role_permission
 * @property boolean $order_report_permission
 * @property boolean $transaction_report_permission
 * @property boolean $dashboard_permission
 * @property boolean $advertising_box_permission
 * @property boolean $static_content_permission
 * @property boolean $operator_permission
 * @property boolean $regulation_permission
 * @property string $created_at
 * @property string $updated_at
 * @property Operator[] $operators
 */
class Role extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'role';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'cargo_owners_permission', 'drivers_permission', 'carriers_permission', 'marketers_permission', 'messages_permission', 'vehicle_permission', 'cargo_type_permission', 'discount_permission', 'role_permission', 'order_report_permission', 'transaction_report_permission', 'dashboard_permission', 'advertising_box_permission', 'static_content_permission', 'role_permission', 'operator_permission', 'regulation_permission', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function operators()
    {
        return $this->hasMany('App\Operator');
    }


    public static function getAll()
    {
        return Role::all();
    }

    public static function deleteRole(Request $request)
    {
        Role::where('id', $request->get('id'))->delete();
    }

    public static function addOrEdit(Request $request)
    {
        $role = null;
        if (!isset($request->id)) {
            $role = new Role();
        } else {
            $role = Role::where('id', $request->get('id'))->first();
        }
        if (isset($request->name)) $role->name = $request->get('name');
        if (isset($request->cargo_owners_permission)) $role->cargo_owners_permission = $request->get('cargo_owners_permission');
        if (isset($request->drivers_permission)) $role->drivers_permission = $request->get('drivers_permission');
        if (isset($request->carriers_permission)) $role->carriers_permission = $request->get('carriers_permission');
        if (isset($request->marketers_permission)) $role->marketers_permission = $request->get('marketers_permission');
        if (isset($request->messages_permission)) $role->messages_permission = $request->get('messages_permission');
        if (isset($request->vehicle_permission)) $role->vehicle_permission = $request->get('vehicle_permission');
        if (isset($request->cargo_type_permission)) $role->cargo_type_permission = $request->get('cargo_type_permission');
        if (isset($request->discount_permission)) $role->discount_permission = $request->get('discount_permission');
        if (isset($request->setting_permission)) $role->setting_permission = $request->get('setting_permission');
        if (isset($request->order_report_permission)) $role->order_report_permission = $request->get('order_report_permission');
        if (isset($request->transaction_report_permission)) $role->transaction_report_permission = $request->get('transaction_report_permission');
        if (isset($request->dashboard_permission)) $role->dashboard_permission = $request->get('dashboard_permission');
        if (isset($request->advertising_box_permission)) $role->advertising_box_permission = $request->get('advertising_box_permission');
        if (isset($request->static_content_permission)) $role->static_content_permission = $request->get('static_content_permission');
        if (isset($request->role_permission)) $role->role_permission = $request->get('role_permission');
        if (isset($request->operator_permission)) $role->operator_permission = $request->get('operator_permission');
        if (isset($request->regulation_permission)) $role->regulation_permission = $request->get('regulation_permission');

        $role->save();
        return $role;

    }
}
