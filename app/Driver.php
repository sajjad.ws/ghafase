<?php

namespace App;

use App\Http\Helpers\UploadHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/**
 * @property integer $id
 * @property integer $marketer_id
 * @property integer $city_id
 * @property integer $vehicle_id
 * @property string $first_name
 * @property string $last_name
 * @property string $plate1
 * @property string $plate2
 * @property string $plate3
 * @property string $plate4
 * @property string $international_plate1
 * @property string $international_plate2
 * @property string $international_plate3
 * @property string $international_plate4
 * @property integer $credit
 * @property boolean $point
 * @property boolean $is_aboard_eager
 * @property string $phone_number
 * @property boolean $status
 * @property string $validation_code
 * @property string $api_token
 * @property string $push_notification_token
 * @property string $created_at
 * @property string $updated_at
 * @property City $city
 * @property Marketer $marketer
 * @property Vehicle $vehicle
 */
class Driver extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'driver';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['marketer_id', 'city_id', 'vehicle_id', 'first_name', 'last_name', 'plate1', 'plate2', 'plate3', 'plate4', 'international_plate1', 'international_plate2', 'international_plate3', 'international_plate4', 'credit', 'point', 'is_aboard_eager', 'phone_number', 'status', 'validation_code', 'api_token', 'push_notification_token', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function marketer()
    {
        return $this->belongsTo('App\Marketer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }

    public static function searchCountOfVehicle(Request $request)
    {
        return Driver::where('vehicle_id', $request->get('id'))->get()->count();
    }

    public static function searchByPhoneNumber(Request $request)
    {
        $driver = Driver::where('phone_number', 'LIKE', '%' . $request->get('phone_number') . '%')
            ->select("driver.*", DB::raw("CONCAT(driver.first_name,' ',driver.last_name) as name"))->get();
        return $driver;
    }

    public static function updateOrInsertByPhone($phoneNumber, $pushNotificationToken)
    {
        $Driver = Driver::where('phone_number', $phoneNumber)->first();

        if (is_null($Driver)) {
            $Driver = new Driver();
        }
        $Driver->push_notification_token = $pushNotificationToken;
        $Driver->phone_number = $phoneNumber;
        $Driver->api_token = self::generateToken();
        $Driver->validation_code = self::generateVerifyNumber();
        $Driver->save();

        return $Driver;
    }

    public static function generateToken()
    {
        $token = Str::random(60);
        $customer = Driver::where('api_token', $token)->first();
        if (!is_null($customer)) {
            $token = self::generateToken();
        }
        return $token;
    }

    /**
     * generates verify number for sms verification
     * @return int
     */
    public static function generateVerifyNumber()
    {
        return mt_rand(20000, 99999);
    }

    public static function verify($phoneNumber, $tempCode)
    {
        $Driver = Driver::where('phone_number', $phoneNumber)->where('validation_code', $tempCode)->first();
        if (!is_null($Driver)) {
            return $Driver;
        } else {
            return false;
        }
    }

    public static function editProfile(Request $request)
    {
        $driver = Driver::where('id', $request->User()->id)->first();

        if (isset($request->first_name)) $driver->first_name = $request->get('first_name');
        if (isset($request->last_name)) $driver->last_name = $request->get('last_name');
        if (isset($request->latitude)) $driver->latitude = $request->get('latitude');
        if (isset($request->longitude)) $driver->longitude = $request->get('longitude');
        if (isset($request->plate1)) $driver->plate1 = $request->get('plate1');
        if (isset($request->plate2)) $driver->plate2 = $request->get('plate2');
        if (isset($request->plate3)) $driver->plate3 = $request->get('plate3');
        if (isset($request->plate4)) $driver->plate4 = $request->get('plate4');
        if (isset($request->international_plate1)) $driver->international_plate1 = $request->get('international_plate1');
        if (isset($request->international_plate2)) $driver->international_plate2 = $request->get('international_plate2');
        if (isset($request->international_plate3)) $driver->international_plate3 = $request->get('international_plate3');
        if (isset($request->international_plate4)) $driver->international_plate4 = $request->get('international_plate4');
        if (isset($request->credit)) $driver->credit = $request->get('credit');
        if (isset($request->point)) $driver->point = $request->get('point');
        if (isset($request->national_code)) $driver->national_code = $request->get('national_code');
        if (isset($request->photo_url)) $driver->photo_url = $request->get('photo_url');

        if (isset($request->is_aboard_eager)) $driver->is_aboard_eager = $request->get('is_aboard_eager');
        if (isset($request->phone_number)) $driver->phone_number = $request->get('phone_number');
        if (isset($request->status)) $driver->status = $request->get('status');
        if (isset($request->marketer_id)) $driver->marketer_id = $request->get('marketer_id');
        if (isset($request->vehicle_id)) $driver->vehicle_id = $request->get('vehicle_id');
        if (isset($request->city_id)) $driver->city_id = $request->get('city_id');

        $driver->save();
        return $driver;

    }

    public static function getDriverByID($DriverID)
    {
        $driver = Driver::where('id', $DriverID)->first();
        return $driver;
    }

    public static function addByMarketer(Request $request)
    {
        $driver = new Driver();
        if (isset($request->first_name)) $driver->first_name = $request->get('first_name');
        if (isset($request->last_name)) $driver->last_name = $request->get('last_name');
        if (isset($request->photo_url)) $driver->photo_url = $request->get('photo_url');
        if (isset($request->latitude)) $driver->latitude = $request->get('latitude');
        if (isset($request->longitude)) $driver->longitude = $request->get('longitude');
        if (isset($request->plate1)) $driver->plate1 = $request->get('plate1');
        if (isset($request->plate2)) $driver->plate2 = $request->get('plate2');
        if (isset($request->plate3)) $driver->plate3 = $request->get('plate3');
        if (isset($request->plate4)) $driver->plate4 = $request->get('plate4');
        if (isset($request->national_code)) $driver->national_code = $request->get('national_code');
        if (isset($request->international_plate1)) $driver->international_plate1 = $request->get('international_plate1');
        if (isset($request->international_plate2)) $driver->international_plate2 = $request->get('international_plate2');
        if (isset($request->international_plate3)) $driver->international_plate3 = $request->get('international_plate3');
        if (isset($request->international_plate4)) $driver->international_plate4 = $request->get('international_plate4');
        if (isset($request->credit)) $driver->credit = $request->get('credit');
        if (isset($request->point)) $driver->point = $request->get('point');
        if (isset($request->is_aboard_eager)) $driver->is_aboard_eager = $request->get('is_aboard_eager');
        if (isset($request->phone_number)) $driver->phone_number = $request->get('phone_number');
        if (isset($request->status)) $driver->status = $request->get('status');
        if (isset($request->vehicle_id)) $driver->vehicle_id = $request->get('vehicle_id');
        if (isset($request->city_id)) $driver->city_id = $request->get('city_id');
        $driver->marketer_id = $request->User()->id;
        if (isset($request->photo)){
            try {
                $image = UploadHelper::uploadFile($request->photo, 'driver_photo');
                if (!empty($image)) {
                    $driver->photo_url = $image;
                } else {
                    return null;
                }
            } catch (\Exception $e) {
                return $e;
            }
        }
        $driver->save();
        return $driver;
    }

    public static function getDriversByMarketer(Request $request)
    {
        $drivers = Driver::where('marketer_id', $request->User()->id);
        if (isset($request->city_id)) $drivers->where('city_id', $request->get('city_id'));
        if (isset($request->word)) $drivers->where(DB::raw('concat(first_name," ",last_name)') , 'LIKE' , "%{$request->get('word')}%");

        if (isset($request->city_id)) {
            $drivers->where('city_id', $request->get('city_id'));
        }
        if (isset($request->name)) {
            $drivers->where('first_name', $request->get('name'))
                ->orWhere('last_name', $request->get('name'));
        }

        $drivers = $drivers->get();
        if (sizeof($drivers) > 0) {
            for ($i = 0; $i < sizeof($drivers); $i++) {
                $drivers[$i]['cargo_request_count'] = CargoRequest::where('receiver_id',$drivers[$i]['id'])
                    ->where('receiver_type',config('receiver.driver'))->get()->count();
            }
        }

        return $drivers;
    }

    //
    public static function getAll(Request $request)
    {

        $drivers = Driver::with(['City', 'Vehicle']);

        if (isset($request->search)) {
            $drivers->where("first_name", 'LIKE', '%' . $request->get('search') . '%')
                ->orWhere("last_name", 'LIKE', '%' . $request->get('search') . '%')
                ->orWhere("national_code", 'LIKE', '%' . $request->get('search') . '%')
                ->orWhere("phone_number", 'LIKE', '%' . $request->get('search') . '%');
        }

        return $drivers->get();
    }

    public static function deleteDriver(Request $request)
    {
        Driver::where('id', $request->get('id'))->delete();
    }

    public static function changeStatus(Request $request)
    {
        $driver = Driver::where('id', $request->get('id'))->first();
        if (isset($request->status)) {
            if ($request->get('status') === 0) $driver->status = 1;
            else  $driver->status = 0;
        };
        $driver->save();
        return $driver;
    }

    public static function addOrEdit(Request $request)
    {
        $driver = null;
        if (!isset($request->id)) {
            $driver = new Driver();
        } else {
            $driver = Driver::where('id', $request->get('id'))->first();
        }
        if (isset($request->first_name)) $driver->first_name = $request->get('first_name');
        if (isset($request->last_name)) $driver->last_name = $request->get('last_name');
        if (isset($request->plate1)) $driver->plate1 = $request->get('plate1');
        if (isset($request->plate2)) $driver->plate2 = $request->get('plate2');
        if (isset($request->plate3)) $driver->plate3 = $request->get('plate3');
        if (isset($request->plate4)) $driver->plate4 = $request->get('plate4');
        if (isset($request->latitude)) $driver->latitude = $request->get('latitude');
        if (isset($request->longitude)) $driver->longitude = $request->get('longitude');
        if (isset($request->international_plate1)) $driver->international_plate1 = $request->get('international_plate1');
        if (isset($request->international_plate2)) $driver->international_plate2 = $request->get('international_plate2');
        if (isset($request->international_plate3)) $driver->international_plate3 = $request->get('international_plate3');
        if (isset($request->international_plate4)) $driver->international_plate4 = $request->get('international_plate4');
        if (isset($request->credit)) $driver->credit = $request->get('credit');
        if (isset($request->point)) $driver->point = $request->get('point');
        if (isset($request->national_code)) $driver->national_code = $request->get('national_code');
        if (isset($request->is_aboard_eager)) $driver->is_aboard_eager = $request->get('is_aboard_eager');
        if (isset($request->phone_number)) $driver->phone_number = $request->get('phone_number');
        if (isset($request->status)) $driver->status = $request->get('status');
        if (isset($request->vehicle_id)) $driver->vehicle_id = $request->get('vehicle_id');
        if (isset($request->marketer_id)) $driver->marketer_id = $request->get('marketer_id');
        if (isset($request->photo_url)) $driver->photo_url = $request->get('photo_url');

        $driver->save();
        return $driver;

    }

    public static function removePhoto(Request $request)
    {
        $driver = Driver::where('id', $request->get('id'))->first();
        $driver->photo_url = null;
        $driver->save();
        return $driver;

    }

    public static function updateProfilePhoto($id, $photo)
    {
        try {
            $image = UploadHelper::uploadFile($photo, 'driver_photo');
            if (!empty($image)) {
                $driver = Driver::where('id', $id)->first();
                $driver->photo_url = $image;
                $driver->save();
                return $image;
            } else {
                return null;
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    //
    public static function getAllByMarketer(Request $request)
    {

        $drivers = Driver::with(['City', 'Vehicle']);
        $drivers->where("marketer_id",$request->get("marketer_id"));

        if (isset($request->search)) {
            $drivers->where("first_name", 'LIKE', '%' . $request->get('search') . '%')
                ->orWhere("last_name", 'LIKE', '%' . $request->get('search') . '%')
                ->orWhere("phone_number", 'LIKE', '%' . $request->get('search') . '%');
        }
        $drivers = $drivers->get();
        if (sizeof($drivers) > 0) {
            for ($i = 0; $i < sizeof($drivers); $i++) {
                $drivers[$i]['cargo_request'] = CargoRequest::where('receiver_id', $drivers[$i]['id'])->count();
            }
        }
        return $drivers;
    }

    public static function sumCredit()
    {
        return Driver::sum('credit');
    }
}
