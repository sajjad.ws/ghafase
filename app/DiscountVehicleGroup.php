<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $discount_id
 * @property integer $vehicle_group_id
 * @property string $created_at
 * @property string $updated_at
 * @property Discount $discount
 * @property VehicleGroup $vehicleGroup
 */
class DiscountVehicleGroup extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'discount_vehicle_group';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['discount_id', 'vehicle_group_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function discount()
    {
        return $this->belongsTo('App\Discount');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vehicleGroup()
    {
        return $this->belongsTo('App\VehicleGroup');
    }

    public static function getAll()
    {
        return DiscountVehicleGroup::all();
    }
}
