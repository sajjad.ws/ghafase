<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $order_id
 * @property integer $product_id
 * @property Order $order
 * @property Product $product
 */
class OrderProduct extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'order_product';

    /**
     * @var array
     */
    protected $fillable = ['order_id', 'product_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
