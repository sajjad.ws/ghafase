<?php

namespace App;

use App\Http\Helpers\UploadHelper;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $vehicle_id
 * @property integer $cargo_request_id
 * @property integer $carrier_id
 * @property string $first_name
 * @property string $last_name
 * @property string $plate1
 * @property string $plate2
 * @property string $plate3
 * @property string $plate4
 * @property string $created_at
 * @property string $updated_at
 * @property CargoRequest $cargoRequest
 * @property Carrier $carrier
 * @property Vehicle $vehicle
 */
class CarrierDriver extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'carrier_driver';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['vehicle_id', 'cargo_request_id', 'carrier_id', 'first_name', 'last_name', 'plate1', 'plate2', 'plate3', 'plate4', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cargoRequest()
    {
        return $this->belongsTo('App\CargoRequest');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function carrier()
    {
        return $this->belongsTo('App\Carrier');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }

    public static function add($request)
    {
        $carrierDriver = new CarrierDriver();
        $carrierDriver->carrier_id = $request->User()->id;

        if (isset($request->first_name)) $carrierDriver->first_name = $request->get('first_name');
        if (isset($request->last_name)) $carrierDriver->last_name = $request->get('last_name');
        if (isset($request->plate1)) $carrierDriver->plate1 = $request->get('plate1');
        if (isset($request->plate2)) $carrierDriver->plate2 = $request->get('plate2');
        if (isset($request->plate3)) $carrierDriver->plate3 = $request->get('plate3');
        if (isset($request->plate4)) $carrierDriver->plate4 = $request->get('plate4');
        if (isset($request->vehicle_id)) $carrierDriver->vehicle_id = $request->get('vehicle_id');
        if (isset($request->cargo_request_id)) $carrierDriver->cargo_request_id = $request->get('cargo_request_id');
        if (isset($request->request_id)) $carrierDriver->cargo_request_id = $request->get('request_id');

        if (isset($request->photo)){
            try {
                $image = UploadHelper::uploadFile($request->photo, 'driver_photo');
                if (!empty($image)) {
                    $carrierDriver->photo_url = $image;
                } else {
                    return null;
                }
            } catch (\Exception $e) {
                return $e;
            }
        }
        return $carrierDriver->save();
    }
}
