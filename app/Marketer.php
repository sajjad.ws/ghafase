<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $username
 * @property string $password
 * @property string $phone_number
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 * @property CargoOwner[] $cargoOwners
 * @property Marketer[] $marketers
 * @property Driver[] $drivers
 */
class Marketer extends Model implements Authenticatable
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'marketer';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'username', 'password', 'phone_number', 'status', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cargoOwners()
    {
        return $this->hasMany('App\CargoOwner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function marketers()
    {
        return $this->hasMany('App\Marketer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function drivers()
    {
        return $this->hasMany('App\Driver');
    }

    public static function addMarketerTest($username, $password)
    {
        $marketer = new Marketer();
        $marketer->username = $username;
        $marketer->password = Hash::make($password);
        $marketer->api_token = self::generateToken();
        $marketer->save();
        return $marketer;
    }

    public static function logIn($username , $password)
    {
        $marketer = Marketer::where('username', $username)->first();
        if (Hash::check($password, $marketer->password)){
            $marketer->api_token = self::generateToken();
            $marketer->save();
            return $marketer;
        }
        else return null;
    }

    public static function generateToken()
    {
        $token = Str::random(60);
        $customer = Marketer::where('api_token', $token)->first();
        if (!is_null($customer)) {
            $token = self::generateToken();
        }
        return $token;
    }

    /**
     * generates verify number for sms verification
     * @return int
     */
    public static function generateRandomPassword()
    {
        return mt_rand(200000, 999999);
    }

    public static function forgetPassword($username)
    {
        $marketer = Marketer::where('username', $username)->first();
        $randomPass = strval(self::generateRandomPassword());
        $marketer->password = Hash::make($randomPass);
        $marketer->save();
        return $randomPass;

    }

    public static function editProfile(Request $request)
    {
        $marketer = Marketer::where('id', $request->User()->id)->first();

        if (isset($request->freight_name)) $marketer->freight_name = $request->get('freight_name');
        if (isset($request->username)) $marketer->username = $request->get('username');
        if (isset($request->password)) $marketer->password = Hash::make($request->get('password'));
        if (isset($request->owner_first_name)) $marketer->owner_first_name = $request->get('owner_first_name');
        if (isset($request->owner_last_name)) $marketer->owner_last_name = $request->get('owner_last_name');
        if (isset($request->owner_phone_number)) $marketer->owner_phone_number = $request->get('owner_phone_number');
        if (isset($request->telephone_number)) $marketer->telephone_number = $request->get('telephone_number');
        if (isset($request->point)) $marketer->point = $request->get('point');
        if (isset($request->email)) $marketer->email = $request->get('email');
        if (isset($request->status)) $marketer->status = $request->get('status');
        if (isset($request->credit)) $marketer->credit = $request->get('credit');
        if (isset($request->marketer_id)) $marketer->national_code = $request->get('marketer_id');
        if (isset($request->city_id)) $marketer->city_id = $request->get('city_id');

        $marketer->save();
        return $marketer;

    }

    public static function getMarketerByID($id)
    {
        $marketer = Marketer::where('id', $id)->first();
        return $marketer;
    }

    //

    public static function getAll(Request $request)
    {
        $marketers = Marketer::where('id','>',0);

        if (isset($request->search)){
            $marketers->where("first_name", 'LIKE', '%' . $request->get('search') . '%')
                ->orWhere("last_name", 'LIKE', '%' . $request->get('search') . '%')
                ->orWhere("national_code", 'LIKE', '%' . $request->get('search') . '%')
                ->orWhere("phone_number", 'LIKE', '%' . $request->get('search') . '%');
        }

        return $marketers->get();
    }

    public static function deleteMarketer(Request $request) {
        Marketer::where('id',$request->get('id'))->delete();
    }

    public static function changeStatus(Request $request) {
        $marketer = Marketer::where('id',$request->get('id'))->first();
        if (isset($request->status)){
            if ($request->get('status') === 0) $marketer->status = 1;
            else  $marketer->status = 0;
        };
        $marketer->save();
        return $marketer;
    }

    public static function addOrEdit(Request $request)
    {
        $marketer = null;
        if (!isset($request->id)){
            $marketer = new Marketer();
        } else {
            $marketer = Marketer::where('id', $request->get('id'))->first();
        }

        if (isset($request->first_name)) $marketer->first_name = $request->get('first_name');
        if (isset($request->last_name)) $marketer->last_name = $request->get('last_name');
        if (isset($request->username)) $marketer->username = $request->get('username');
        if (isset($request->password)) $marketer->password = Hash::make($request->get('password'));
        if (isset($request->phone_number)) $marketer->phone_number = $request->get('phone_number');
        if (isset($request->status)) $marketer->status = $request->get('status');
        if (isset($request->national_code)) $marketer->national_code = $request->get('national_code');

        $marketer->save();
        return $marketer;

    }

    /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName()
    {
        // TODO: Implement getAuthIdentifierName() method.
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        // TODO: Implement getAuthIdentifier() method.
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        // TODO: Implement getAuthPassword() method.
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        // TODO: Implement getRememberToken() method.
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string $value
     * @return void
     */
    public function setRememberToken($value)
    {
        // TODO: Implement setRememberToken() method.
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        // TODO: Implement getRememberTokenName() method.
    }
}
