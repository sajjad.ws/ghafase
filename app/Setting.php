<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 */
class Setting extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'setting';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['created_at', 'updated_at'];


    public static function getAll()
    {
        return Setting::first();
    }

    public static function deleteSetting(Request $request)
    {
        Setting::where('id', $request->get('id'))->delete();
    }

    public static function edit(Request $request)
    {
        $setting = null;
        if (!isset($request->id)) {
            $setting = new Setting();
        } else {
            $setting = Setting::where('id', $request->get('id'))->first();
        }
        if (isset($request->driver_regulation)) $setting->driver_regulation = $request->get('driver_regulation');
        if (isset($request->carrier_regulation)) $setting->carrier_regulation = $request->get('carrier_regulation');
        if (isset($request->cargo_registration_regulation)) $setting->cargo_registration_regulation = $request->get('cargo_registration_regulation');
        if (isset($request->driver_confirmation_regulation)) $setting->driver_confirmation_regulation = $request->get('driver_confirmation_regulation');
        if (isset($request->cost_estimation_status)) $setting->cost_estimation_status = $request->get('cost_estimation_status');
        if (isset($request->cost_estimation_value)) $setting->cost_estimation_value = $request->get('cost_estimation_value');
        if (isset($request->carrier_driver_introduction)) $setting->carrier_driver_introduction = $request->get('carrier_driver_introduction');
        if (isset($request->cargo_cancelable_time)) $setting->cargo_cancelable_time = $request->get('cargo_cancelable_time');
        if (isset($request->driver_confirmation_time)) $setting->driver_confirmation_time = $request->get('driver_confirmation_time');
        if (isset($request->system_status)) $setting->system_status = $request->get('system_status');
        if (isset($request->is_active_registrar_commission)) $setting->is_active_registrar_commission = $request->get('is_active_registrar_commission');
        if (isset($request->is_active_receiver_commission)) $setting->is_active_receiver_commission = $request->get('is_active_receiver_commission');

        $setting->save();
        return $setting;

    }

}
