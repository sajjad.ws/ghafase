<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @property integer $id
 * @property integer $marketer_id
 * @property integer $city_id
 * @property string $freight_name
 * @property string $username
 * @property string $password
 * @property string $owner_first_name
 * @property string $owner_last_name
 * @property string $owner_phone_number
 * @property string $telephone_number
 * @property boolean $status
 * @property integer $credit
 * @property boolean $point
 * @property string $created_at
 * @property string $updated_at
 * @property City $city
 * @property Marketer $marketer
 * @property CarrierCarrier[] $carrierCarriers
 */
class Carrier extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'carrier';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['marketer_id', 'city_id', 'freight_name', 'username', 'password', 'owner_first_name', 'owner_last_name', 'owner_phone_number', 'telephone_number', 'status', 'credit', 'point', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function marketer()
    {
        return $this->belongsTo('App\Marketer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function carrierCarriers()
    {
        return $this->hasMany('App\CarrierCarrier');
    }

    public static function searchByPhoneNumber(Request $request)
    {
        $owner = Carrier::where('owner_phone_number', 'LIKE', '%' . $request->get('phone_number') . '%')
            ->orWhere('telephone_number', 'LIKE', '%' . $request->get('phone_number') . '%')
            ->select("carrier.*", DB::raw("CONCAT('باربری',' ',carrier.freight_name) as name"))->get();
        return $owner;
    }

    public static function checkUser(Request $request)
    {

        return Carrier::where('username',$request->get('username'))->first();
    }


    public static function addCarrier(Request $request)
    {
        $carrier = new Carrier();
        $carrier->username = $request->get('username');
        $carrier->password = Hash::make($request->get('password'));
        $carrier->api_token = self::generateToken();


        if (isset($request->freight_name)) $carrier->freight_name = $request->get('freight_name');
        if (isset($request->username)) $carrier->username = $request->get('username');
        if (isset($request->password)) $carrier->password = Hash::make($request->get('password'));
        if (isset($request->owner_first_name)) $carrier->owner_first_name = $request->get('owner_first_name');
        if (isset($request->owner_last_name)) $carrier->owner_last_name = $request->get('owner_last_name');
        if (isset($request->owner_phone_number)) $carrier->owner_phone_number = $request->get('owner_phone_number');
        if (isset($request->telephone_number)) $carrier->telephone_number = $request->get('telephone_number');
        if (isset($request->point)) $carrier->point = $request->get('point');
        if (isset($request->status)) $carrier->status = $request->get('status');
        if (isset($request->credit)) $carrier->credit = $request->get('credit');
        if (isset($request->marketer_id)) $carrier->national_code = $request->get('marketer_id');
        if (isset($request->city_id)) $carrier->city_id = $request->get('city_id');
        if (isset($request->push_notification_token)) $carrier->push_notification_token = $request->get('push_notification_token');

        $carrier->save();
        return $carrier;
    }

    public static function logIn($username, $password, $token)
    {
        $carrier = Carrier::where('username', $username)->first();
        if (Hash::check($password, $carrier->password)) {
            $carrier->api_token = self::generateToken();
            $carrier->push_notification_token = $token;

            $carrier->save();
            return $carrier;
        } else return null;
    }

    public static function generateToken()
    {
        $token = Str::random(60);
        $customer = Carrier::where('api_token', $token)->first();
        if (!is_null($customer)) {
            $token = self::generateToken();
        }
        return $token;
    }

    /**
     * generates verify number for sms verification
     * @return int
     */
    public static function generateRandomPassword()
    {
        return mt_rand(200000, 999999);
    }

    public static function forgetPassword($username)
    {
        $carrier = Carrier::where('username', $username)->first();
        $randomPass = strval(self::generateRandomPassword());
        $carrier->password = Hash::make($randomPass);
        $carrier->save();
        return $randomPass;

    }

    public static function editProfile(Request $request)
    {
        $carrier = Carrier::where('id', $request->User()->id)->first();

        if (isset($request->freight_name)) $carrier->freight_name = $request->get('freight_name');
        if (isset($request->username)) $carrier->username = $request->get('username');
        if (isset($request->password)) $carrier->password = Hash::make($request->get('password'));
        if (isset($request->owner_first_name)) $carrier->owner_first_name = $request->get('owner_first_name');
        if (isset($request->owner_last_name)) $carrier->owner_last_name = $request->get('owner_last_name');
        if (isset($request->owner_phone_number)) $carrier->owner_phone_number = $request->get('owner_phone_number');
        if (isset($request->telephone_number)) $carrier->telephone_number = $request->get('telephone_number');
        if (isset($request->point)) $carrier->point = $request->get('point');
        if (isset($request->status)) $carrier->status = $request->get('status');
        if (isset($request->credit)) $carrier->credit = $request->get('credit');
        if (isset($request->marketer_id)) $carrier->national_code = $request->get('marketer_id');
        if (isset($request->city_id)) $carrier->city_id = $request->get('city_id');

        $carrier->save();
        return $carrier;

    }

    public static function getCarrierByID($id)
    {
        $carrier = Carrier::where('id', $id)->first();
        return $carrier;
    }

    public static function addByMarketer(Request $request)
    {
        $carrier = new Carrier();
        if (isset($request->freight_name)) $carrier->freight_name = $request->get('freight_name');
        if (isset($request->username)) $carrier->username = $request->get('username');
        if (isset($request->password)) $carrier->password = Hash::make($request->get('password'));
        if (isset($request->owner_first_name)) $carrier->owner_first_name = $request->get('owner_first_name');
        if (isset($request->owner_last_name)) $carrier->owner_last_name = $request->get('owner_last_name');
        if (isset($request->owner_phone_number)) $carrier->owner_phone_number = $request->get('owner_phone_number');
        if (isset($request->telephone_number)) $carrier->telephone_number = $request->get('telephone_number');
        if (isset($request->point)) $carrier->point = $request->get('point');
        if (isset($request->status)) $carrier->status = $request->get('status');
        if (isset($request->credit)) $carrier->credit = $request->get('credit');
        if (isset($request->city_id)) $carrier->city_id = $request->get('city_id');
        $carrier->marketer_id = $request->User()->id;
        $carrier->save();
        return $carrier;
    }


    public static function getCarriersByMarketer(Request $request)
    {
        $carriers = Carrier::where('marketer_id', $request->User()->id);
        if (isset($request->city_id)) {
            $carriers->where('city_id', $request->get('city_id'));
        }
        if (isset($request->word)) $carriers->where('freight_name', 'LIKE', "%{$request->get('word')}%");

        if (isset($request->name)) {
            $carriers->where('freight_name', $request->get('name'));
        }


        $carriers = $carriers->get();
        if (sizeof($carriers) > 0) {
            for ($i = 0; $i < sizeof($carriers); $i++) {
                $carriers[$i]['done_cargo_request_count'] = CargoRequest::where('receiver_id', $carriers[$i]['id'])
                    ->where('receiver_type', config('receiver.carrier'))
                    ->whereIn('last_status', [
                        config('request.acceptByCarrier'),
                        config('request.introductionDriverByCarrier'),
                        config('request.acceptCarrierByOwner'),
                        config('request.start'),
                        config('request.end'),
                        config('request.confirmByOwner'),
                    ])
                    ->get()->count();
                $carriers[$i]['all_cargo_request_count'] = CargoRequest::where('owner_id', $carriers[$i]['id'])
                    ->where('owner_type', config('owner.carrier'))->get()->count();

            }
        }

        return $carriers;
    }

    //

    public static function getAll(Request $request)
    {
        $carrier = Carrier::with(['City']);

        if (isset($request->search)) {
            $carrier->where("freight_name", 'LIKE', '%' . $request->get('search') . '%')
                ->orWhere("telephone_number", 'LIKE', '%' . $request->get('search') . '%');
        }
        if (isset($request->selected_city_id)) {
            $carrier->where("city_id", 'LIKE', '%' . $request->get('selected_city_id') . '%');
        }

        return $carrier->get();
    }

    public static function deleteCarrier(Request $request)
    {
        Carrier::where('id', $request->get('id'))->delete();
    }

    public static function changeStatus(Request $request)
    {
        $carrier = Carrier::where('id', $request->get('id'))->first();
        if (isset($request->status)) {
            if ($request->get('status') === 0) $carrier->status = 1;
            else  $carrier->status = 0;
        };
        $carrier->save();
        return $carrier;
    }

    public static function addOrEdit(Request $request)
    {
        $carrier = null;
        if (!isset($request->id)) {
            $carrier = new Carrier();
        } else {
            $carrier = Carrier::where('id', $request->get('id'))->first();
        }

        if (isset($request->freight_name)) $carrier->freight_name = $request->get('freight_name');
        if (isset($request->username)) $carrier->username = $request->get('username');
        if (isset($request->password)) $carrier->password = Hash::make($request->get('password'));
        if (isset($request->owner_first_name)) $carrier->owner_first_name = $request->get('owner_first_name');
        if (isset($request->owner_last_name)) $carrier->owner_last_name = $request->get('owner_last_name');
        if (isset($request->owner_phone_number)) $carrier->owner_phone_number = $request->get('owner_phone_number');
        if (isset($request->telephone_number)) $carrier->telephone_number = $request->get('telephone_number');
        if (isset($request->point)) $carrier->point = $request->get('point');
        if (isset($request->status)) $carrier->status = $request->get('status');
        if (isset($request->credit)) $carrier->credit = $request->get('credit');
        if (isset($request->marketer_id)) $carrier->national_code = $request->get('marketer_id');
        if (isset($request->city_id)) $carrier->city_id = $request->get('city_id');

        $carrier->save();
        return $carrier;

    }

    //

    public static function getAllByMarketer(Request $request)
    {
        $carrier = Carrier::with(['City']);
        $carrier->where("marketer_id", $request->get("marketer_id"));

        if (isset($request->search)) {
            $carrier->where("freight_name", 'LIKE', '%' . $request->get('search') . '%')
                ->orWhere("telephone_number", 'LIKE', '%' . $request->get('search') . '%');
        }
        $carrier = $carrier->get();
        if (sizeof($carrier) > 0) {
            for ($i = 0; $i < sizeof($carrier); $i++) {
                $carrier[$i]['cargo_request'] = CargoRequest::where('receiver_id', $carrier[$i]['id'])->count();
            }
        }

        return $carrier;
    }

    public static function sumCredit()
    {
        return Carrier::sum('credit');
    }
}
