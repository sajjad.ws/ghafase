<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $lat_long
 * @property string $sms_validation
 * @property int $status
 * @property int $payment_status
 * @property string $owner_name
 * @property string $owner_phone
 * @property string $image_url
 * @property string $created_at
 * @property string $updated_at
 * @property Order[] $orders
 * @property Product[] $products
 */
class Store extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';
    protected $appends = ['product_count'];

    /**
     * @var array
     */
    protected $fillable = ['name', 'address', 'lat_long', 'sms_validation', 'status', 'payment_status', 'owner_name', 'owner_phone', 'image_url', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('App\Product');
    }


    public function getProductCountAttribute()
    {
        $count = sizeof(Product::where('store_id', $this->id)->get());
        return $count;

    }

}
