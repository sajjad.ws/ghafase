<?php

namespace App;

use App\Http\Helpers\UploadHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * @property integer $id
 * @property string $title
 * @property int $size
 * @property string $media_url
 * @property string $created_at
 * @property string $updated_at
 */
class AdvertisingBox extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'advertising_box';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['title', 'size', 'media_url', 'created_at', 'updated_at'];


    public static function getAll()
    {
        return AdvertisingBox::all();
    }

    public static function deleteAdvertisingBox(Request $request)
    {
        AdvertisingBox::where('id', $request->get('id'))->delete();
    }

    public static function addOrEdit(Request $request)
    {
        $advertisingBox = null;
        if (isset($request->id)) {
            $advertisingBox = AdvertisingBox::where('id', $request->get('id'))->first();
        } else {
            $advertisingBox = new AdvertisingBox();
        }

        if ($request->image !== null)  {
            $media_url = UploadHelper::uploadImage($request->image, 'image', 120, 120);
            $advertisingBox->media_url = $media_url;
        }

        if (isset($request->title)) $advertisingBox->title = $request->get('title');
        if (isset($request->size)) $advertisingBox->size = $request->get('size');

        $advertisingBox->save();
        return $advertisingBox;

    }
}
