<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * @property integer $id
 * @property boolean $user_type
 * @property integer $user_id
 * @property boolean $type
 * @property integer $amount
 * @property int $discount_percent
 * @property integer $summation
 * @property integer $value
 * @property integer $ref_id
 * @property string $created_at
 * @property string $updated_at
 */
class CreditTransaction extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'credit_transaction';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['user_type', 'user_id', 'type', 'amount', 'discount_percent', 'summation', 'value', 'ref_id', 'created_at', 'updated_at'];

    public static function getCargoOwnerTransactionsHistory(Request $request)
    {
        $cargoRequest = CreditTransaction::where('user_id', $request->User()->id)
            ->where('user_type',config('transactionUserType.cargoOwner'))
            ->get();
        return $cargoRequest;
    }

    public static function getDriverTransactionsHistory(Request $request)
    {
        $cargoRequest = CreditTransaction::where('user_id', $request->User()->id)
            ->where('user_type',config('transactionUserType.driver'))
            ->get();
        return $cargoRequest;
    }

    public static function getCarrierTransactionsHistory(Request $request)
    {
        $cargoRequest = CreditTransaction::where('user_id', $request->User()->id)
            ->where('user_type',config('transactionUserType.carrier'))
            ->get();
        return $cargoRequest;
    }


    public static function getAll(Request $request)
    {
        $transactions = CreditTransaction::where('id','>',0);

        if (isset($request->start_time)) $transactions->where('created_at','>',$request->get('start_time'));
        if (isset($request->end_time)) $transactions->where('created_at','<',$request->get('end_time'));
        if (isset($request->user_id)) {
            $transactions->where('user_id',$request->get('user_id'))->where('user_type',$request->get('user_type'));
        }

        return $transactions->get();
    }

    public static function deleteCreditTransaction(Request $request)
    {
        CreditTransaction::where('id', $request->get('id'))->delete();
    }

    public static function addOrEdit(Request $request)
    {
        $creditTransaction = null;
        if (!isset($request->id)) {
            $creditTransaction = new CreditTransaction();
        } else {
            $creditTransaction = CreditTransaction::where('id', $request->get('id'))->first();
        }
        if (isset($request->user_type)) $creditTransaction->user_type = $request->get('user_type');
        if (isset($request->user_id)) $creditTransaction->user_id = $request->get('user_id');
        if (isset($request->type)) $creditTransaction->type = $request->get('type');
        if (isset($request->amount)) $creditTransaction->amount = $request->get('amount');
        if (isset($request->discount_percent)) $creditTransaction->discount_percent = $request->get('discount_percent');
        if (isset($request->summation)) $creditTransaction->summation = $request->get('summation');
        if (isset($request->value)) $creditTransaction->media_url = $request->get('value');
        if (isset($request->ref_id)) $creditTransaction->media_url = $request->get('ref_id');

        $creditTransaction->save();
        return $creditTransaction;

    }
}
