<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $discount_id
 * @property integer $cargo_type_id
 * @property string $created_at
 * @property string $updated_at
 * @property CargoType $cargoType
 * @property Discount $discount
 */
class DiscountCargoType extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'discount_cargo_type';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['discount_id', 'cargo_type_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cargoType()
    {
        return $this->belongsTo('App\CargoType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function discount()
    {
        return $this->belongsTo('App\Discount');
    }

    public static function getAll()
    {
        return DiscountCargoType::all();
    }
}
