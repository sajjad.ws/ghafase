<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\VehicleGroup;
/**
 * @property integer $id
 * @property integer $vehicle_group_id
 * @property string $name
 * @property int $max_capacity
 * @property string $created_at
 * @property string $updated_at
 * @property VehicleGroup $vehicleGroup
 * @property CargoRequest[] $cargoRequests
 * @property CarrierDriver[] $carrierDrivers
 * @property Driver[] $drivers
 */
class Vehicle extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'vehicle';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['vehicle_group_id', 'name', 'max_capacity', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vehicleGroup()
    {
        return $this->belongsTo('App\VehicleGroup');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cargoRequests()
    {
        return $this->hasMany('App\CargoRequest');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function carrierDrivers()
    {
        return $this->hasMany('App\CarrierDriver');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function drivers()
    {
        return $this->hasMany('App\Driver');
    }

    public static function getAll()
    {
        return Vehicle::with('VehicleGroup')->get();
    }

    public static function deleteVehicle(Request $request) {
        Vehicle::where('id',$request->get('id'))->delete();
    }

    public static function addOrEdit(Request $request)
    {
        $vehicle = null;
        if (!isset($request->id)){
            $vehicle = new Vehicle();
        } else {
            $vehicle = Vehicle::where('id', $request->get('id'))->first();
        }
        if (isset($request->name)) $vehicle->name = $request->get('name');
        if (isset($request->max_capacity)) $vehicle->max_capacity = $request->get('max_capacity');
        if (isset($request->vehicle_group_id)) $vehicle->vehicle_group_id = $request->get('vehicle_group_id');

        $vehicle->save();
        return $vehicle;

    }

    public static function searchCountOfVehicle(Request $request)
    {
        return Vehicle::where('vehicle_group_id', $request->get('id'))->get()->count();
    }
}
