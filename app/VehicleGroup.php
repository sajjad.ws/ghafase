<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * @property integer $id
 * @property string $name
 * @property integer $commission_amount
 * @property string $created_at
 * @property string $updated_at
 * @property DiscountVehicleGroup[] $discountVehicleGroups
 * @property Vehicle[] $vehicles
 */
class VehicleGroup extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'vehicle_group';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'commission_amount', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function discountVehicleGroups()
    {
        return $this->hasMany('App\DiscountVehicleGroup');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vehicles()
    {
        return $this->hasMany('App\Vehicle');
    }

    public static function getAll()
    {
        return VehicleGroup::all();
    }

    public static function deleteVehicleGroup(Request $request) {
        VehicleGroup::where('id',$request->get('id'))->delete();
    }

    public static function addOrEdit(Request $request)
    {
        $vehicleGroup = null;
        if (!isset($request->id)){
            $vehicleGroup = new VehicleGroup();
        } else {
            $vehicleGroup = VehicleGroup::where('id', $request->get('id'))->first();
        }
        if (isset($request->name)) $vehicleGroup->name = $request->get('name');
        if (isset($request->commission_amount)) $vehicleGroup->commission_amount = $request->get('commission_amount');

        $vehicleGroup->save();
        return $vehicleGroup;

    }
}
