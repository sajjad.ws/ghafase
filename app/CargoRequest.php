<?php

namespace App;

use App\Http\Helpers\FirebaseHelper;
use Carbon\Traits\Date;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * @property integer $id
 * @property integer $cargo_type_id
 * @property integer $vehicle_id
 * @property integer $origin_city_id
 * @property integer $destination_city_id
 * @property string $origin_address
 * @property string $destination_address
 * @property float $origin_latitude
 * @property float $origin_longitude
 * @property float $destination_latitude
 * @property float $destination_longitude
 * @property int $cargo_weight
 * @property int $cargo_cost
 * @property boolean $owner_type
 * @property integer $owner_id
 * @property string $description
 * @property string $message_to_driver
 * @property string $loading_start_time
 * @property string $loading_end_time
 * @property string $evacuation_start_time
 * @property string $evacuation_end_time
 * @property boolean $last_status
 * @property boolean $receiver_type
 * @property integer $receiver_id
 * @property string $created_at
 * @property string $updated_at
 * @property CargoType $cargoType
 * @property City $cityDestination
 * @property City $cityOrigin
 * @property Vehicle $vehicle
 * @property CargoRequestActivity[] $cargoRequestActivities
 * @property CarrierDriver[] $carrierDrivers
 * @property Comment[] $comments
 */
class CargoRequest extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cargo_request';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['cargo_type_id', 'vehicle_id', 'origin_city_id', 'destination_city_id', 'origin_address', 'destination_address', 'origin_latitude', 'origin_longitude', 'destination_latitude', 'destination_longitude', 'cargo_weight', 'cargo_cost', 'owner_type', 'owner_id', 'description', 'message_to_driver', 'loading_start_time', 'loading_end_time', 'evacuation_start_time', 'evacuation_end_time', 'last_status', 'receiver_type', 'receiver_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cargoType()
    {
        return $this->belongsTo('App\CargoType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cityDestination()
    {
        return $this->belongsTo('App\City', 'destination_city_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cityOrigin()
    {
        return $this->belongsTo('App\City', 'origin_city_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cargoRequestActivities()
    {
        return $this->hasMany('App\CargoRequestActivity');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function carrierDrivers()
    {
        return $this->hasMany('App\CarrierDriver');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public static function searchCountOfCargoType(Request $request)
    {
        return Vehicle::where('cargo_type_id', $request->get('id'))->get()->count();
    }

    public static function getPrice($request)
    {
        return 1000;
    }

    public static function addRequestByCargoOwner(Request $request)
    {
        $cargoRequest = null;

        if (isset($request->id)) {
            $cargoRequest = CargoRequest::where('id', $request->id)->first();
        } else {
            $cargoRequest = new CargoRequest();
            $cargoRequest->last_status = config('request.add');
            $cargoRequest->owner_id = $request->User()->id;
            $cargoRequest->owner_type = config('owner.cargoOwner');
        }

        if (isset($request->origin_address)) $cargoRequest->origin_address = $request->get('origin_address');
        if (isset($request->destination_address)) $cargoRequest->destination_address = $request->get('destination_address');
        if (isset($request->origin_latitude)) $cargoRequest->origin_latitude = $request->get('origin_latitude');
        if (isset($request->origin_longitude)) $cargoRequest->origin_longitude = $request->get('origin_longitude');
        if (isset($request->destination_latitude)) $cargoRequest->destination_latitude = $request->get('destination_latitude');
        if (isset($request->destination_longitude)) $cargoRequest->destination_longitude = $request->get('destination_longitude');
        if (isset($request->cargo_weight)) $cargoRequest->cargo_weight = $request->get('cargo_weight');
        if (isset($request->cargo_cost)) $cargoRequest->cargo_cost = $request->get('cargo_cost');
        if (isset($request->description)) $cargoRequest->description = $request->get('description');
        if (isset($request->message_to_driver)) $cargoRequest->message_to_driver = $request->get('message_to_driver');
        if (isset($request->loading_start_time)) $cargoRequest->loading_start_time = $request->get('loading_start_time');
        if (isset($request->loading_end_time)) $cargoRequest->loading_end_time = $request->get('loading_end_time');
        if (isset($request->evacuation_start_time)) $cargoRequest->evacuation_start_time = $request->get('evacuation_start_time');
        if (isset($request->evacuation_end_time)) $cargoRequest->evacuation_end_time = $request->get('evacuation_end_time');
        if (isset($request->receiver_type)) $cargoRequest->receiver_type = $request->get('receiver_type');
        if (isset($request->receiver_id)) $cargoRequest->receiver_id = $request->get('receiver_id');
        if (isset($request->cargo_type_id)) $cargoRequest->cargo_type_id = $request->get('cargo_type_id');
        if (isset($request->vehicle_id)) $cargoRequest->vehicle_id = $request->get('vehicle_id');
        if (isset($request->origin_city_id)) $cargoRequest->origin_city_id = $request->get('origin_city_id');
        if (isset($request->destination_city_id)) $cargoRequest->destination_city_id = $request->get('destination_city_id');
        if (isset($request->price)) $cargoRequest->price = $request->get('price');

        if (isset($request->cargo_cost_min)) $cargoRequest->cargo_cost_min = $request->get('cargo_cost_min');
        if (isset($request->cargo_cost_max)) $cargoRequest->cargo_cost_max = $request->get('cargo_cost_max');
        if (isset($request->cargo_weight_min)) $cargoRequest->cargo_weight_min = $request->get('cargo_weight_min');
        if (isset($request->cargo_weight_max)) $cargoRequest->cargo_weight_max = $request->get('cargo_weight_max');

        $cargoRequest->save();

        $drivers = Driver::where('city_id',$cargoRequest->origin_city_id)->where('vehicle_id',$cargoRequest->vehicle_id)->pluck('push_notification_token');
        FirebaseHelper::sendFcmNotificationMessage($drivers,$cargoRequest,'یک بار جدید منتظر شماست',config('fcmsettings.request'));

        return $cargoRequest;

    }

    public static function addRequestByCarrier(Request $request)
    {
        $cargoRequest = null;

        if (isset($request->id)) {
            $cargoRequest = CargoRequest::where('id', $request->id)->first();
        } else {
            $cargoRequest = new CargoRequest();
            $cargoRequest->last_status = config('request.add');
            $cargoRequest->owner_id = $request->User()->id;
            $cargoRequest->owner_type = config('owner.carrier');
        }

        if (isset($request->origin_address)) $cargoRequest->origin_address = $request->get('origin_address');
        if (isset($request->destination_address)) $cargoRequest->destination_address = $request->get('destination_address');
        if (isset($request->origin_latitude)) $cargoRequest->origin_latitude = $request->get('origin_latitude');
        if (isset($request->origin_longitude)) $cargoRequest->origin_longitude = $request->get('origin_longitude');
        if (isset($request->destination_latitude)) $cargoRequest->destination_latitude = $request->get('destination_latitude');
        if (isset($request->destination_longitude)) $cargoRequest->destination_longitude = $request->get('destination_longitude');
        if (isset($request->cargo_weight)) $cargoRequest->cargo_weight = $request->get('cargo_weight');
        if (isset($request->cargo_cost)) $cargoRequest->cargo_cost = $request->get('cargo_cost');
        if (isset($request->description)) $cargoRequest->description = $request->get('description');
        if (isset($request->message_to_driver)) $cargoRequest->message_to_driver = $request->get('message_to_driver');
        if (isset($request->loading_start_time)) $cargoRequest->loading_start_time = $request->get('loading_start_time');
        if (isset($request->loading_end_time)) $cargoRequest->loading_end_time = $request->get('loading_end_time');
        if (isset($request->evacuation_start_time)) $cargoRequest->evacuation_start_time = $request->get('evacuation_start_time');
        if (isset($request->evacuation_end_time)) $cargoRequest->evacuation_end_time = $request->get('evacuation_end_time');
        if (isset($request->receiver_type)) $cargoRequest->receiver_type = $request->get('receiver_type');
        if (isset($request->receiver_id)) $cargoRequest->receiver_id = $request->get('receiver_id');
        if (isset($request->cargo_type_id)) $cargoRequest->cargo_type_id = $request->get('cargo_type_id');
        if (isset($request->vehicle_id)) $cargoRequest->vehicle_id = $request->get('vehicle_id');
        if (isset($request->origin_city_id)) $cargoRequest->origin_city_id = $request->get('origin_city_id');
        if (isset($request->destination_city_id)) $cargoRequest->destination_city_id = $request->get('destination_city_id');
        if (isset($request->price)) $cargoRequest->price = $request->get('price');

        if (isset($request->cargo_cost_min)) $cargoRequest->cargo_cost_min = $request->get('cargo_cost_min');
        if (isset($request->cargo_cost_max)) $cargoRequest->cargo_cost_max = $request->get('cargo_cost_max');
        if (isset($request->cargo_weight_min)) $cargoRequest->cargo_weight_min = $request->get('cargo_weight_min');
        if (isset($request->cargo_weight_max)) $cargoRequest->cargo_weight_max = $request->get('cargo_weight_max');

        $cargoRequest->save();
        return $cargoRequest;

    }

    public static function edit(Request $request)
    {
        $cargoRequest = null;

        if (isset($request->id)) {
            $cargoRequest = CargoRequest::where('id', $request->id)->first();
            if (isset($request->origin_address)) $cargoRequest->origin_address = $request->get('origin_address');
            if (isset($request->destination_address)) $cargoRequest->destination_address = $request->get('destination_address');
            if (isset($request->origin_latitude)) $cargoRequest->origin_latitude = $request->get('origin_latitude');
            if (isset($request->origin_longitude)) $cargoRequest->origin_longitude = $request->get('origin_longitude');
            if (isset($request->destination_latitude)) $cargoRequest->destination_latitude = $request->get('destination_latitude');
            if (isset($request->destination_longitude)) $cargoRequest->destination_longitude = $request->get('destination_longitude');
            if (isset($request->cargo_weight)) $cargoRequest->cargo_weight = $request->get('cargo_weight');
            if (isset($request->cargo_cost)) $cargoRequest->cargo_cost = $request->get('cargo_cost');
            if (isset($request->description)) $cargoRequest->description = $request->get('description');
            if (isset($request->message_to_driver)) $cargoRequest->message_to_driver = $request->get('message_to_driver');
            if (isset($request->loading_start_time)) $cargoRequest->loading_start_time = $request->get('loading_start_time');
            if (isset($request->loading_end_time)) $cargoRequest->loading_end_time = $request->get('loading_end_time');
            if (isset($request->evacuation_start_time)) $cargoRequest->evacuation_start_time = $request->get('evacuation_start_time');
            if (isset($request->evacuation_end_time)) $cargoRequest->evacuation_end_time = $request->get('evacuation_end_time');
            if (isset($request->receiver_type)) $cargoRequest->receiver_type = $request->get('receiver_type');
            if (isset($request->receiver_id)) $cargoRequest->receiver_id = $request->get('receiver_id');
            if (isset($request->cargo_type_id)) $cargoRequest->cargo_type_id = $request->get('cargo_type_id');
            if (isset($request->vehicle_id)) $cargoRequest->vehicle_id = $request->get('vehicle_id');
            if (isset($request->origin_city_id)) $cargoRequest->origin_city_id = $request->get('origin_city_id');
            if (isset($request->destination_city_id)) $cargoRequest->destination_city_id = $request->get('destination_city_id');
            if (isset($request->price)) $cargoRequest->price = $request->get('price');

            if (isset($request->cargo_cost_min)) $cargoRequest->cargo_cost = $request->get('cargo_cost_min');
            if (isset($request->cargo_cost_max)) $cargoRequest->cargo_cost_max = $request->get('cargo_cost_max');
            if (isset($request->cargo_weight_min)) $cargoRequest->cargo_weight_min = $request->get('cargo_weight_min');
            if (isset($request->cargo_weight_max)) $cargoRequest->cargo_weight_max = $request->get('cargo_weight_max');

            $cargoRequest->save();
        }

        return $cargoRequest;

    }

    public static function acceptRequestByDriver($request)
    {
        $cargoRequest = CargoRequest::where('id', $request->get('request_id'))->first();
        $cargoRequest->last_status = config('request.acceptByDriver');
        $cargoRequest->receiver_type = config('receiver.driver');
        $cargoRequest->receiver_id = $request->User()->id;
        $cargoRequest->save();
        $owner = null;
        if ($cargoRequest->owner_type === config('owner.cargoOwner'))
            $owner = CargoOwner::where('id', $cargoRequest->owner_id)->first();
        else if ($cargoRequest->owner_type === config('owner.carrier'))
            $owner = Carrier::where('id', $cargoRequest->owner_id)->first();
        FirebaseHelper::sendFcmNotificationMessage([$owner->push_notification_token],$cargoRequest,'درخواست شما برای حمل بار پذیرفته شد',config('fcmsettings.request'));
    }

    public static function acceptRequestByCarrier($request)
    {
        $cargoRequest = CargoRequest::where('id', $request->get('request_id'))->first();
        $cargoRequest->last_status = config('request.acceptByCarrier');
        $cargoRequest->receiver_type = config('receiver.carrier');
        $cargoRequest->receiver_id = $request->User()->id;
        $cargoRequest->save();
        $owner = null;
        if ($cargoRequest->owner_type === config('owner.cargoOwner'))
            $owner = CargoOwner::where('id', $cargoRequest->owner_id)->first();
        else if ($cargoRequest->owner_type === config('owner.carrier'))
            $owner = Carrier::where('id', $cargoRequest->owner_id)->first();
        FirebaseHelper::sendFcmNotificationMessage([$owner->push_notification_token],$cargoRequest,'درخواست شما برای حمل بار پذیرفته شد',config('fcmsettings.request'));

    }

    public static function introductionDriverByCarrier($request)
    {
        $cargoRequest = CargoRequest::where('id', $request->get('request_id'))->first();
        $cargoRequest->last_status = config('request.introductionDriverByCarrier');
        return $cargoRequest->save();
    }

    public static function acceptDriverByOwner($request)
    {
        $cargoRequest = CargoRequest::where('id', $request->get('request_id'))->first();
        $cargoRequest->last_status = config('request.acceptDriverByOwner');
        $cargoRequest->save();
        $driver = Driver::where('id',$cargoRequest->receiver_id)->first();
        FirebaseHelper::sendFcmNotificationMessage([$driver->push_notification_token],$cargoRequest,'درخواست شما برای حمل بار تایید شد.برای ادامه به برنامه مراجعه بفرمایید',config('fcmsettings.request'));
    }

    public static function acceptCarrierByOwner($request)
    {
        $cargoRequest = CargoRequest::where('id', $request->get('request_id'))->first();
        $cargoRequest->last_status = config('request.acceptCarrierByOwner');
        $cargoRequest->save();
        $carrier = Carrier::where('id',$cargoRequest->receiver_id)->first();
        FirebaseHelper::sendFcmNotificationMessage([$carrier->push_notification_token],$cargoRequest,'درخواست شما برای حمل بار تایید شد.برای ادامه به برنامه مراجعه بفرمایید',config('fcmsettings.request'));
    }

    public static function rejectDriverByOwner($request)
    {
        $cargoRequest = CargoRequest::where('id', $request->get('request_id'))->first();
        $cargoRequest->last_status = config('request.add');
        $cargoRequest->save();
        $driver = Driver::where('id',$cargoRequest->receiver_id)->first();
        FirebaseHelper::sendFcmNotificationMessage([$driver->push_notification_token],$cargoRequest,'درخواست شما برای حمل بار رد شد',config('fcmsettings.request'));
    }

    public static function cancelByOwner($request)
    {
        $cargoRequest = CargoRequest::where('id', $request->get('request_id'))->first();
        $cargoRequest->last_status = config('request.cancelByOwner');
        $cargoRequest->save();

        $receiver = null;
        if ($cargoRequest->receiver_type === config('receiver.driver'))
            $receiver = Driver::where('id', $cargoRequest->receiver_id)->first();
        else if ($cargoRequest->receiver_type === config('receiver.carrier'))
            $receiver = Carrier::where('id', $cargoRequest->receiver_id)->first();
        if($receiver != null){
            FirebaseHelper::sendFcmNotificationMessage([$receiver->push_notification_token],$cargoRequest,'درخواست شما برای حمل بار لغو شد',config('fcmsettings.request'));

        }
    }

    public static function cancelByDriver($request)
    {
        $cargoRequest = CargoRequest::where('id', $request->get('request_id'))->first();
        $cargoRequest->last_status = config('request.cancelByDriver');
        $cargoRequest->save();
        $owner = null;
        if ($cargoRequest->owner_type === config('owner.cargoOwner'))
            $owner = CargoOwner::where('id', $cargoRequest->owner_id)->first();
        else if ($cargoRequest->owner_type === config('owner.carrier'))
            $owner = Carrier::where('id', $cargoRequest->owner_id)->first();
        FirebaseHelper::sendFcmNotificationMessage([$owner->push_notification_token],$cargoRequest,'درخواست شما برای حمل بار لغو شد',config('fcmsettings.request'));
    }

    public static function cancelByCarrier($request)
    {
        $cargoRequest = CargoRequest::where('id', $request->get('request_id'))->first();
        $cargoRequest->last_status = config('request.cancelByCarrier');
        $cargoRequest->save();
        $owner = null;
        if ($cargoRequest->owner_type === config('owner.cargoOwner'))
            $owner = CargoOwner::where('id', $cargoRequest->owner_id)->first();
        else if ($cargoRequest->owner_type === config('owner.carrier'))
            $owner = Carrier::where('id', $cargoRequest->owner_id)->first();
        FirebaseHelper::sendFcmNotificationMessage([$owner->push_notification_token],$cargoRequest,'درخواست شما برای حمل بار لغو شد',config('fcmsettings.request'));
    }

    public static function start($request)
    {
        $cargoRequest = CargoRequest::where('id', $request->get('request_id'))->first();
        $cargoRequest->last_status = config('request.start');
        $cargoRequest->save();
        $owner = null;
        if ($cargoRequest->owner_type === config('owner.cargoOwner'))
            $owner = CargoOwner::where('id', $cargoRequest->owner_id)->first();
        else if ($cargoRequest->owner_type === config('owner.carrier'))
            $owner = Carrier::where('id', $cargoRequest->owner_id)->first();
        FirebaseHelper::sendFcmNotificationMessage([$owner->push_notification_token],$cargoRequest,'فرایند حمل بار شما آغاز شد',config('fcmsettings.request'));

    }

    public static function end($request)
    {
        $cargoRequest = CargoRequest::where('id', $request->get('request_id'))->first();
        $cargoRequest->last_status = config('request.end');
        $cargoRequest->save();
        $owner = null;
        if ($cargoRequest->owner_type === config('owner.cargoOwner'))
            $owner = CargoOwner::where('id', $cargoRequest->owner_id)->first();
        else if ($cargoRequest->owner_type === config('owner.carrier'))
            $owner = Carrier::where('id', $cargoRequest->owner_id)->first();
        FirebaseHelper::sendFcmNotificationMessage([$owner->push_notification_token],$cargoRequest,'بار به مقصد رسید',config('fcmsettings.request'));
    }

    public static function confirmByOwner($request)
    {
        $cargoRequest = CargoRequest::where('id', $request->get('cargo_request_id'))->first();
        $cargoRequest->last_status = config('request.confirmByOwner');
        $cargoRequest->save();
    }

    public static function confirmByCarrier($request)
    {
        $cargoRequest = CargoRequest::where('id', $request->get('cargo_request_id'))->first();
        $cargoRequest->last_status = config('request.end');
        $cargoRequest->save();
    }


    public static function getCargoOwnerRequestHistory(Request $request)
    {
        $cargoRequest = CargoRequest::where('owner_id', $request->User()->id)
            ->where('owner_type', config('owner.cargoOwner'))
            ->whereIn('last_status', [
                config('request.confirmByOwner'),
                config('request.cancelByOwner'),
                config('request.end'),
            ])
            ->get();
        return $cargoRequest;
    }

    public static function getCargoOwnerRequestByMarketer(Request $request)
    {
        $cargoRequest = CargoRequest::where('owner_type', config('owner.cargoOwner'))
            ->where('owner_id', $request->get('cargo_owner_id'));


        if (isset($request->create_date)) $cargoRequest->whereDate('created_at', $request->get('create_date'));

        return $cargoRequest->orderBy('id', 'DESC')->get();
    }

    public static function getDriverRequestHistoryByMarketer(Request $request)
    {


        $cargoRequest = CargoRequest::where('receiver_type', config('receiver.driver'))
            ->where('receiver_id', $request->get('receiver_id'));


        if (isset($request->create_date)) $cargoRequest->whereDate('created_at', $request->get('create_date'));

        return $cargoRequest->orderBy('id', 'DESC')->get();


    }

    public static function getCarrierRequestHistoryByMarketer(Request $request)
    {
        $cargoRequest = CargoRequest::where('receiver_type', config('receiver.carrier'))
            ->where('receiver_id', $request->get('receiver_id'));



        if (isset($request->create_date)) $cargoRequest->whereDate('created_at', $request->get('create_date'));
        if ($request->type === "1") {
            $cargoRequest->whereIn('last_status', [
                    config('request.acceptByCarrier'),
                    config('request.introductionDriverByCarrier'),
                    config('request.acceptCarrierByOwner'),
                    config('request.start'),
                    config('request.end'),
                    config('request.confirmByOwner'),
                ]);
        }
        return $cargoRequest->orderBy('id', 'DESC')->get();
    }


    public static function getCargoOwnerHoverRequests(Request $request)
    {
        $cargoRequest = CargoRequest::where('owner_id', $request->User()->id)
            ->where('owner_type', config('owner.cargoOwner'))
            ->whereIn('last_status', [
                config('request.add'),
            ])->orderBy('id', 'DESC')
            ->get();
        return $cargoRequest;
    }

    public static function getCargoOwnerActiveRequests(Request $request)
    {
        $cargoRequest = CargoRequest::where('owner_id', $request->User()->id)
            ->where('owner_type', config('owner.cargoOwner'))
            ->whereIn('last_status', [
                config('request.acceptByDriver'),
                config('request.acceptByCarrier'),
                config('request.introductionDriverByCarrier'),
                config('request.acceptDriverByOwner'),
                config('request.acceptCarrierByOwner'),
                config('request.start'),
                config('request.end'),
            ])->orderBy('id', 'DESC')
            ->get();
        if (sizeof($cargoRequest) > 0) {
            for ($i = 0; $i < sizeof($cargoRequest); $i++) {
                if ($cargoRequest[$i]['receiver_type'] === config('receiver.driver'))
                    $cargoRequest[$i]['receiver'] = Driver::where('id', $cargoRequest[$i]['receiver_id'])->first();
                else if ($cargoRequest[$i]['receiver_type'] === config('receiver.carrier')) {
                    $cargoRequest[$i]['receiver'] = Carrier::where('id', $cargoRequest[$i]['receiver_id'])->first();
                    $cargoRequest[$i]['carrier_driver'] = CarrierDriver::where('cargo_request_id', $cargoRequest[$i]['id'])->first();
                }
            }
        }

        return $cargoRequest;
    }

    public static function changeRequestStatus($requestID, $status, $receiverID, $score)
    {
        $cargoRequest = CargoRequest::where('id', $requestID);

        $cargoRequestActivity = new CargoRequestActivity();
        $cargoRequestActivity->status_from = $cargoRequest->last_status;

        $cargoRequest->last_status = $status;
        //TODO Set Types & Status
        if ($receiverID !== null && $status = '') {
            $cargoRequest->receiver_id = $receiverID;
            $cargoRequest->receiver_type = '';

            $cargoRequestActivity->receiver_type = $cargoRequest->receiver_type;
            $cargoRequestActivity->receiver_id = $cargoRequest->receiver_id;
        }
        if ($score !== null) {
            //TODO Score
        }
        $cargoRequest->save();
        //
        $cargoRequestActivity->status_to = $status;
        $cargoRequestActivity->save();
    }

    public static function getDriverRequestHistory(Request $request)
    {
        $cargoRequest = CargoRequest::where('receiver_id', $request->User()->id)
            ->where('receiver_type', config('receiver.driver'))
            ->whereIn('last_status', [
                config('request.confirmByOwner'),
                config('request.cancelByDriver'),
                config('request.end'),
            ]);


        if (isset($request->vehicle_id)) $cargoRequest->where('vehicle_id', $request->get('vehicle_id'));
        if (isset($request->cargo_type_id)) $cargoRequest->where('cargo_type_id', $request->get('cargo_type_id'));
        if (isset($request->origin_city_id)) $cargoRequest->where('origin_city_id', $request->get('origin_city_id'));
        if (isset($request->destination_city_id)) $cargoRequest->where('destination_city_id', $request->get('destination_city_id'));
        if (isset($request->cargo_weight_min)) $cargoRequest->where('cargo_weight_min', '>=', $request->get('cargo_weight_min'));
        if (isset($request->cargo_weight_max)) $cargoRequest->where('cargo_weight_max', '<=', $request->get('cargo_weight_max'));
        if (isset($request->loading_start_time)) {
            $cargoRequest->whereDate('loading_start_time', $request->get('loading_start_time'));
        }
        if (isset($request->evacuation_start_time)) {
            $cargoRequest->whereDate('evacuation_start_time', $request->get('evacuation_start_time'));
        }


        return $cargoRequest->orderBy('id', 'DESC')->get();
    }

    public static function getDriverHoverRequests(Request $request)
    {
        $cargoRequest = CargoRequest::whereIn('last_status', [
            config('request.add'),
        ]);

        if (isset($request->vehicle_id)) $cargoRequest->where('vehicle_id', $request->get('vehicle_id'));
        if (isset($request->cargo_type_id)) $cargoRequest->where('cargo_type_id', $request->get('cargo_type_id'));
        if (isset($request->origin_city_id)) $cargoRequest->where('origin_city_id', $request->get('origin_city_id'));
        if (isset($request->destination_city_id)) $cargoRequest->where('destination_city_id', $request->get('destination_city_id'));
        if (isset($request->cargo_weight_min)) $cargoRequest->where('cargo_weight_min', '>=', $request->get('cargo_weight_min'));
        if (isset($request->cargo_weight_max)) $cargoRequest->where('cargo_weight_max', '<=', $request->get('cargo_weight_max'));
        if (isset($request->loading_start_time)) {
            $cargoRequest->whereDate('loading_start_time', $request->get('loading_start_time'));
        }
        if (isset($request->evacuation_start_time)) {
            $cargoRequest->whereDate('evacuation_start_time', $request->get('evacuation_start_time'));
        }

        return $cargoRequest->orderBy('id', 'DESC')->get();
    }

    public static function getDriverActiveRequests(Request $request)
    {
        $cargoRequest = CargoRequest::where('receiver_id', $request->User()->id)
            ->where('receiver_type', config('receiver.driver'))
            ->whereIn('last_status', [
                config('request.acceptByDriver'),
                config('request.acceptDriverByOwner'),
                config('request.start'),
            ])->orderBy('id', 'DESC')
            ->get();

        if (sizeof($cargoRequest) > 0) {
            for ($i = 0; $i < sizeof($cargoRequest); $i++) {
                if ($cargoRequest[$i]['owner_type'] === config('owner.cargoOwner'))
                    $cargoRequest[$i]['owner'] = CargoOwner::where('id', $cargoRequest[$i]['owner_id'])->first();
                else if ($cargoRequest[$i]['owner_type'] === config('owner.carrier')) {
                    $cargoRequest[$i]['owner'] = Carrier::where('id', $cargoRequest[$i]['owner_id'])->first();
                }
            }
        }



        return $cargoRequest;
    }

    public static function getDriverRequests(Request $request)
    {
        //TODO Set Types & Status
        $cargoRequest = CargoRequest::where('owner_id', $request->User()->id)
            ->where('owner_type', '')
            ->where('last_status', '');

        if (isset($request->vehicle_id)) {
            $cargoRequest->where("vehicle_id", '=', $request->get('vehicle_id'));
        }


        return $cargoRequest;
    }

    public static function getCarrierRequestHistoryOwner(Request $request)
    {
        $cargoRequest = CargoRequest::where('owner_id', $request->User()->id)
            ->where('owner_type', config('owner.carrier'))
            ->whereIn('last_status', [
                config('request.confirmByOwner'),
                config('request.cancelByCarrier'),
                config('request.end'),
            ])
            ->get();
        return $cargoRequest;
    }

    public static function getCarrierRequestHistoryReceiver(Request $request)
    {
        $cargoRequest = CargoRequest::where('receiver_id', $request->User()->id)
            ->where('receiver_type', config('receiver.carrier'))
            ->whereIn('last_status', [
                config('request.confirmByOwner'),
                config('request.cancelByCarrier'),
                config('request.end'),
            ]);


        if (isset($request->vehicle_id)) $cargoRequest->where('vehicle_id', $request->get('vehicle_id'));
        if (isset($request->cargo_type_id)) $cargoRequest->where('cargo_type_id', $request->get('cargo_type_id'));
        if (isset($request->origin_city_id)) $cargoRequest->where('origin_city_id', $request->get('origin_city_id'));
        if (isset($request->destination_city_id)) $cargoRequest->where('destination_city_id', $request->get('destination_city_id'));
        if (isset($request->cargo_cost_min)) $cargoRequest->where('cargo_cost_min', '>', $request->get('cargo_cost_min'));
        if (isset($request->cargo_cost_max)) $cargoRequest->where('cargo_cost_max', '<', $request->get('cargo_cost_max'));
        if (isset($request->loading_start_time)) $cargoRequest->where('loading_start_time', $request->get('loading_start_time'));
        if (isset($request->evacuation_start_time)) $cargoRequest->where('evacuation_start_time', $request->get('evacuation_start_time'));
        if (isset($request->last_status)) $cargoRequest->where('last_status', $request->get('last_status'));
        if (isset($request->start_time)) $cargoRequest->where('created_at', '>', $request->get('start_time'));
        if (isset($request->end_time)) $cargoRequest->where('created_at', '<', $request->get('end_time'));
        if (isset($request->owner_id)) $cargoRequest->where('owner_id', $request->get('owner_id'))->where('owner_type', $request->get('owner_type'));
        if (isset($request->receiver_id)) $cargoRequest->where('receiver_id', $request->get('receiver_id'))->where('receiver_type', $request->get('receiver_type'));


        return $cargoRequest->get();
    }

    public static function getReceiverCarrierHoverRequests(Request $request)
    {
        $cargoRequest = CargoRequest::whereIn('last_status', [
            config('request.add'),
        ]);



        if (isset($request->vehicle_id)) $cargoRequest->where('vehicle_id', $request->get('vehicle_id'));
        if (isset($request->cargo_type_id)) $cargoRequest->where('cargo_type_id', $request->get('cargo_type_id'));
        if (isset($request->origin_city_id)) $cargoRequest->where('origin_city_id', $request->get('origin_city_id'));
        if (isset($request->destination_city_id)) $cargoRequest->where('destination_city_id', $request->get('destination_city_id'));
        if (isset($request->cargo_cost_min)) $cargoRequest->where('cargo_cost_min', '>', $request->get('cargo_cost_min'));
        if (isset($request->cargo_cost_max)) $cargoRequest->where('cargo_cost_max', '<', $request->get('cargo_cost_max'));
        if (isset($request->loading_start_time)) $cargoRequest->where('loading_start_time', $request->get('loading_start_time'));
        if (isset($request->evacuation_start_time)) $cargoRequest->where('evacuation_start_time', $request->get('evacuation_start_time'));
        if (isset($request->last_status)) $cargoRequest->where('last_status', $request->get('last_status'));
        if (isset($request->start_time)) $cargoRequest->where('created_at', '>', $request->get('start_time'));
        if (isset($request->end_time)) $cargoRequest->where('created_at', '<', $request->get('end_time'));
        if (isset($request->owner_id)) $cargoRequest->where('owner_id', $request->get('owner_id'))->where('owner_type', $request->get('owner_type'));
        if (isset($request->receiver_id)) $cargoRequest->where('receiver_id', $request->get('receiver_id'))->where('receiver_type', $request->get('receiver_type'));


        return $cargoRequest->get();
    }

    public static function getReceiverCarrierActiveRequests(Request $request)
    {
        $cargoRequest = CargoRequest::where('receiver_id', $request->User()->id)
            ->where('receiver_type', config('receiver.carrier'))
            ->whereIn('last_status', [
                config('request.acceptByCarrier'),
                config('request.introductionDriverByCarrier'),
                config('request.acceptCarrierByOwner'),
                config('request.start'),
                    config('request.end')
            ])
            ->get();
        if (sizeof($cargoRequest) > 0) {
            for ($i = 0; $i < sizeof($cargoRequest); $i++) {
                if ($cargoRequest[$i]['owner_type'] === config('owner.cargoOwner'))
                    $cargoRequest[$i]['owner'] = CargoOwner::where('id', $cargoRequest[$i]['owner_id'])->first();
                else if ($cargoRequest[$i]['owner_type'] === config('owner.carrier')) {
                    $cargoRequest[$i]['owner'] = Carrier::where('id', $cargoRequest[$i]['owner_id'])->first();
                }
            }
        }
        return $cargoRequest;
    }

    public static function getOwnerCarrierHoverRequests(Request $request)
    {
        $cargoRequest = CargoRequest::where('owner_id', $request->User()->id)
            ->where('owner_type', config('owner.carrier'))
            ->whereIn('last_status', [
                config('request.add'),
            ])
            ->get();
        return $cargoRequest;
    }

    public static function getOwnerCarrierActiveRequests(Request $request)
    {
        $cargoRequest = CargoRequest::where('owner_id', $request->User()->id)
            ->where('owner_type', config('owner.carrier'))
            ->whereIn('last_status', [
                config('request.acceptByCarrier'),
                config('request.acceptByDriver'),
                config('request.introductionDriverByCarrier'),
                config('request.acceptCarrierByOwner'),
                config('request.acceptDriverByOwner'),
                config('request.start'),
                config('request.end'),
            ])
            ->get();
        if (sizeof($cargoRequest) > 0) {
            for ($i = 0; $i < sizeof($cargoRequest); $i++) {
                if ($cargoRequest[$i]['receiver_type'] === config('receiver.driver'))
                    $cargoRequest[$i]['receiver'] = Driver::where('id', $cargoRequest[$i]['receiver_id'])->first();
                else if ($cargoRequest[$i]['receiver_type'] === config('receiver.carrier')) {
                    $cargoRequest[$i]['receiver'] = Carrier::where('id', $cargoRequest[$i]['receiver_id'])->first();
                    $cargoRequest[$i]['carrier_driver'] = CarrierDriver::where('cargo_request_id', $cargoRequest[$i]['id'])->first();
                }
            }
        }
        return $cargoRequest;
    }

    public static function getCarrierRequests(Request $request)
    {
        //TODO Set Types & Status
        $cargoRequest = CargoRequest::where('owner_id', $request->User()->id)
            ->where('owner_type', '')
            ->where('last_status', '');
        return $cargoRequest;
    }

    public static function getCargoOwnerRequestHistoryByCargoRequest(Request $request)
    {
        $cargoRequest = CargoRequest::where('owner_id', $request->get('cargo_owner_id'))
            ->where('owner_type', config('owner.cargoOwner'))
            ->whereIn('last_status', [
                config('request.confirmByOwner'),
                config('request.cancelByOwner'),
                config('request.end'),
            ])->orderBy('id', 'DESC')
            ->get();
        return $cargoRequest;
    }

    public static function getDriverRequestHistoryByCargoRequest(Request $request)
    {
        $cargoRequest = CargoRequest::where('receiver_id', $request->get('driver_id'))
            ->where('receiver_type', config('receiver.driver'))
            ->whereIn('last_status', [
                config('request.confirmByOwner'),
                config('request.cancelByDriver'),
                config('request.end'),
            ])
            ->get();
        return $cargoRequest;
    }

    public static function getCarrierRequestHistoryByCargoRequest(Request $request)
    {
        //TODO
        $cargoRequest = CargoRequest::where('owner_id', $request->get('cargo_owner_id'))
            ->where('owner_type', '')
            ->whereIn('last_status', [])
            ->get();
        return $cargoRequest;
    }

    //

    public static function getAll(Request $request)
    {
        $cargoRequests = CargoRequest::with(['Vehicle', 'CargoType', 'cityDestination', 'cityOrigin']);

        if (isset($request->vehicle_id)) $cargoRequests->where('vehicle_id', $request->get('vehicle_id'));
        if (isset($request->cargo_type_id)) $cargoRequests->where('cargo_type_id', $request->get('cargo_type_id'));
        if (isset($request->origin_city_id)) $cargoRequests->where('origin_city_id', $request->get('origin_city_id'));
        if (isset($request->destination_city_id)) $cargoRequests->where('destination_city_id', $request->get('destination_city_id'));
        if (isset($request->cargo_cost_min)) $cargoRequests->where('cargo_cost_min', '>', $request->get('cargo_cost_min'));
        if (isset($request->cargo_cost_max)) $cargoRequests->where('cargo_cost_max', '<', $request->get('cargo_cost_max'));
        if (isset($request->loading_start_time)) $cargoRequests->where('loading_start_time', $request->get('loading_start_time'));
        if (isset($request->evacuation_start_time)) $cargoRequests->where('evacuation_start_time', $request->get('evacuation_start_time'));
        if (isset($request->last_status)) $cargoRequests->where('last_status', $request->get('last_status'));
        if (isset($request->start_time)) $cargoRequests->where('created_at', '>', $request->get('start_time'));
        if (isset($request->end_time)) $cargoRequests->where('created_at', '<', $request->get('end_time'));
        if (isset($request->owner_id)) $cargoRequests->where('owner_id', $request->get('owner_id'))->where('owner_type', $request->get('owner_type'));
        if (isset($request->receiver_id)) $cargoRequests->where('receiver_id', $request->get('receiver_id'))->where('receiver_type', $request->get('receiver_type'));

        $cargoRequests = $cargoRequests->get();
        if (sizeof($cargoRequests) > 0) {
            for ($i = 0; $i < sizeof($cargoRequests); $i++) {
                if ($cargoRequests[$i]['owner_type'] === config('owner.cargoOwner')) {
                    $cargoRequests[$i]['owner'] = CargoOwner::where('id', $cargoRequests[$i]['owner_id'])
                        ->select("cargo_owner.*", DB::raw("CONCAT(cargo_owner.first_name,' ',cargo_owner.last_name) as name"),
                            DB::raw("CONCAT(cargo_owner.phone_number) as phone_number"))->first();
                } else if ($cargoRequests[$i]['owner_type'] === config('owner.carrier')) {
                    $cargoRequests[$i]['owner'] = Carrier::where('id', $cargoRequests[$i]['owner_id'])
                        ->select("carrier.*", DB::raw("CONCAT('باربری',' ',carrier.freight_name) as name"),
                            DB::raw("CONCAT(cargo_owner.telephone_number) as phone_number"))->first();
                } else {
                    $cargoRequests[$i]['owner'] = null;
                }
                //
                if ($cargoRequests[$i]['receiver_type'] === config('receiver.driver')) {
                    $cargoRequests[$i]['receiver'] = Driver::where('id', $cargoRequests[$i]['receiver_id'])
                        ->select("driver.*", DB::raw("CONCAT(driver.first_name,' ',driver.last_name) as name"))->first();
                } else if ($cargoRequests[$i]['receiver_type'] === config('receiver.carrier')) {
                    $cargoRequests[$i]['receiver'] = Carrier::where('id', $cargoRequests[$i]['receiver_id'])
                        ->select("carrier.*", DB::raw("CONCAT('باربری',' ',carrier.freight_name) as name"))->first();
                } else {
                    $cargoRequests[$i]['receiver'] = null;
                }
            }
        }
        return $cargoRequests;
    }

    public static function deleteCargoRequest(Request $request)
    {
        CargoRequest::where('id', $request->get('id'))->delete();
    }

    public static function changeStatus(Request $request)
    {
        $cargoRequest = CargoRequest::where('id', $request->get('id'))->first();
        if (isset($request->status)) {
            if ($request->get('status') === 0) $cargoRequest->status = 1;
            else  $cargoRequest->status = 0;
        };
        $cargoRequest->save();
        return $cargoRequest;
    }

    public static function addOrEdit(Request $request)
    {
        $cargoRequest = null;
        if (!isset($request->id)) {
            $cargoRequest = new CargoRequest();
        } else {
            $cargoRequest = CargoRequest::where('id', $request->get('id'))->first();
        }

        if (isset($request->origin_address)) $cargoRequest->origin_address = $request->get('origin_address');
        if (isset($request->destination_address)) $cargoRequest->destination_address = $request->get('destination_address');
        if (isset($request->origin_latitude)) $cargoRequest->origin_latitude = $request->get('origin_latitude');
        if (isset($request->origin_longitude)) $cargoRequest->origin_longitude = $request->get('origin_longitude');
        if (isset($request->destination_latitude)) $cargoRequest->destination_latitude = $request->get('destination_latitude');
        if (isset($request->destination_longitude)) $cargoRequest->destination_longitude = $request->get('destination_longitude');
        if (isset($request->cargo_weight)) $cargoRequest->cargo_weight = $request->get('cargo_weight');
        if (isset($request->cargo_cost)) $cargoRequest->cargo_cost = $request->get('cargo_cost');
        if (isset($request->cargo_cost_min)) $cargoRequest->cargo_cost = $request->get('cargo_cost_min');
        if (isset($request->cargo_cost_max)) $cargoRequest->cargo_cost_max = $request->get('cargo_cost_max');
        if (isset($request->cargo_weight_min)) $cargoRequest->cargo_weight_min = $request->get('cargo_weight_min');
        if (isset($request->cargo_weight_max)) $cargoRequest->cargo_weight_max = $request->get('cargo_weight_max');

        if (isset($request->description)) $cargoRequest->description = $request->get('description');
        if (isset($request->message_to_driver)) $cargoRequest->message_to_driver = $request->get('message_to_driver');
        if (isset($request->loading_start_time)) $cargoRequest->loading_start_time = $request->get('loading_start_time');
        if (isset($request->loading_end_time)) $cargoRequest->loading_end_time = $request->get('loading_end_time');
        if (isset($request->evacuation_start_time)) $cargoRequest->evacuation_start_time = $request->get('evacuation_start_time');
        if (isset($request->evacuation_end_time)) $cargoRequest->evacuation_end_time = $request->get('evacuation_end_time');
        if (isset($request->receiver_type)) $cargoRequest->receiver_type = $request->get('receiver_type');
        if (isset($request->receiver_id)) $cargoRequest->receiver_id = $request->get('receiver_id');
        if (isset($request->cargo_type_id)) $cargoRequest->cargo_type_id = $request->get('cargo_type_id');
        if (isset($request->vehicle_id)) $cargoRequest->vehicle_id = $request->get('vehicle_id');
        if (isset($request->origin_city_id)) $cargoRequest->origin_city_id = $request->get('origin_city_id');
        if (isset($request->destination_city_id)) $cargoRequest->destination_city_id = $request->get('destination_city_id');
        if (isset($request->owner_type)) $cargoRequest->owner_type = $request->get('owner_type');
        if (isset($request->owner_id)) $cargoRequest->owner_id = $request->get('owner_id');

        $cargoRequest->save();
        return $cargoRequest;

    }

    public static function getCountByDate($date)
    {
        if ($date === null) {
            return CargoRequest::all()->count();
        } else {
            return CargoRequest::where('created_at', '>', $date)->count();
        }
    }

}
