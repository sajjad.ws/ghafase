<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property integer $id
 * @property integer $role_id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $created_at
 * @property string $updated_at
 * @property Role $role
 * @property Message[] $messages
 */
class Operator extends Authenticatable implements JWTSubject
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'operator';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['role_id', 'first_name', 'last_name', 'email', 'password', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany('App\Message');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    public static function getByEmailAndPassword(Request $request)
    {
        $operator = Operator::where('email', $request->get('email'))->first();

        if (Hash::check($request->get('password'), $operator->password)) return $operator;
        else return null;
    }

    public static function changePassword(Request $request)
    {
        $operator = Operator::where('reset_token', $request->get('username'))->first();

        $operator->password = Hash::make($request->get('password'));
        $operator->save();
    }

    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $demo;

    public function sendForgetPasswordMail(Request $request)
    {
        $this->demo = $request->get('email');
        Mail::send([], [], function ($message) {
            $operator = Operator::where('email', $this->demo)->first();
            $operator->reset_token = self::generateToken();
            $operator->save();
            $message->to(trim($operator->email), 'Tutorials Point')
                ->subject('subject')
                ->setBody('http://194.5.175.196//resetPassword?key=' . $operator->reset_token, 'text/html');
        });
    }


    public static function generateToken()
    {
        return Str::random(60);
    }

    public static function add(Request $request)
    {
        $operator = new Operator();
        if (!empty($request->get('email'))) {
            $operator->email = $request->get('email');
        }
        if (!empty($request->get('password'))) {
            $operator->password = Hash::make($request->get('password'));
        }
        if (!empty($request->get('first_name'))) {
            $operator->first_name = $request->get('first_name');
        }
        if (!empty($request->get('last_name'))) {
            $operator->last_name = $request->get('last_name');
        }
        if (!empty($request->get('role_id'))) {
            $operator->role_id = $request->get('role_id');
        }
        $operator->save();

        return $operator;
    }

    //

    public static function getAll()
    {
        return Operator::with('Role')->get();
    }

    public static function deleteOperator(Request $request)
    {
        Operator::where('id', $request->get('id'))->delete();
    }

    public static function changeStatus(Request $request)
    {
        $operator = Operator::where('id', $request->get('id'))->first();
        if (isset($request->status)) {
            if ($request->get('status') === 0) $operator->status = 1;
            else  $operator->status = 0;
        };
        $operator->save();
        return $operator;
    }

    public static function addOrEdit(Request $request)
    {
        $operator = null;
        if (!isset($request->id)) {
            $operator = new Operator();
        } else {
            $operator = Operator::where('id', $request->get('id'))->first();
        }
        if (isset($request->first_name)) $operator->first_name = $request->get('first_name');
        if (isset($request->last_name)) $operator->last_name = $request->get('last_name');
        if (isset($request->email)) $operator->email = $request->get('email');
        if (isset($request->status)) $operator->status = $request->get('status');
        if (isset($request->role_id)) $operator->role_id = $request->get('role_id');
        if (isset($request->password)) $operator->password = Hash::make($request->get('password'));

        $operator->save();
        return $operator;

    }
}
