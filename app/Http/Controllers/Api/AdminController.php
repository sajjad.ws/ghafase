<?php

namespace App\Http\Controllers\Api;

use App\AdvertisingBox;
use App\CargoOwner;
use App\CargoRequest;
use App\CargoType;
use App\Carrier;
use App\City;
use App\CreditTransaction;
use App\Discount;
use App\DiscountCargoType;
use App\DiscountVehicleGroup;
use App\Driver;
use App\Mail\HDTutoMail;
use App\Marketer;
use App\Message;
use App\Role;
use App\Setting;
use App\Vehicle;
use App\VehicleGroup;
use App\Http\Controllers\Controller;
use App\Operator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{

    public static function responseJson($errors, $status, $data)
    {
        return response()->json(array_combine(config('app.response_keys'), [$errors, $status, $data]), $status);
    }

    public function login(Request $request)
    {
        $token = $this->guard($request->email, $request->password);

        if ($token) {

            $operator = Operator::getByEmailAndPassword($request);

            if ($operator !== null)
                return $this->responseJson(null, Response::HTTP_OK, $operator)->header('Authorization', $token);
            else
                return $this->responseJson(null, Response::HTTP_OK, config('messages.fail'));

        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function sendMail(Request $request)
    {
        try {
            (new \App\Operator)->sendForgetPasswordMail($request);
            return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $e) {
            return $this->responseJson(null, Response::HTTP_OK, $e->getMessage());
        }
    }

    public function changePassword(Request $request)
    {
        try {
            Operator::changePassword($request);
            return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $e) {
            return $this->responseJson(null, Response::HTTP_OK, $e->getMessage());
        }
    }

    private function guard($email, $password)
    {
        return Auth::guard('operator')->attempt(array('email' => $email, 'password' => $password));
    }

    public function register(Request $request)
    {

        try {
            Operator::add($request);

            return self::responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $e) {

        }


    }

    //
    public function readCargoOwners(Request $request)
    {
        try {

            $data = [
                'cargoOwner' => CargoOwner::getAll($request),
                'province' => City::getProvince(),
            ];
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    public function addOrEditCargoOwner(Request $request)
    {
        if (isset($request->first_name)) {
            try {
                if (!isset($request->id) && CargoOwner::check($request) > 0) {
                    return $this->responseJson(null, Response::HTTP_IM_USED, config('messages.cargoOwnerCheck'));
                } else {
                    $user = CargoOwner::addOrEdit($request);
                }
                return $this->responseJson(null, Response::HTTP_OK, $user);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function deleteCargoOwner(Request $request)
    {
        if (isset($request->id)) {
            try {
                CargoOwner::deleteCargoOwner($request);
                return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function changeStatusCargoOwner(Request $request)
    {
        if (isset($request->id) && isset($request->status)) {
            try {
                CargoOwner::changeStatus($request);
                return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }
    }

    //
    public function readDrivers(Request $request)
    {
        try {
            $data = [
                'driver' => Driver::getAll($request),
                'province' => City::getProvince(),
                'vehicle' => Vehicle::getAll()
            ];
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    public function addOrEditDriver(Request $request)
    {
        if (isset($request->first_name)) {
            try {
                $user = Driver::addOrEdit($request);
                return $this->responseJson(null, Response::HTTP_OK, $user);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function removePhotoDriver(Request $request)
    {
        if (isset($request->id)) {
            try {
                $user = Driver::removePhoto($request);
                return $this->responseJson(null, Response::HTTP_OK, $user);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function deleteDriver(Request $request)
    {
        if (isset($request->id)) {
            try {
                Driver::deleteDriver($request);
                return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function changeStatusDriver(Request $request)
    {
        if (isset($request->id) && isset($request->status)) {
            try {
                Driver::changeStatus($request);
                return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }
    }

    //
    public function readCarriers(Request $request)
    {
        try {
            $data = [
                'carrier' => Carrier::getAll($request),
                'province' => City::getProvince()
            ];
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }


    public function getCityByProvince(Request $request)
    {
        try {
            $data = City::getCityByProvince($request);
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    public function addOrEditCarrier(Request $request)
    {
        if (isset($request->owner_first_name)) {
            try {
                $user = Carrier::addOrEdit($request);
                return $this->responseJson(null, Response::HTTP_OK, $user);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function deleteCarrier(Request $request)
    {
        if (isset($request->id)) {
            try {
                Carrier::deleteCarrier($request);
                return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function changeStatusCarrier(Request $request)
    {
        if (isset($request->id) && isset($request->status)) {
            try {
                Carrier::changeStatus($request);
                return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }
    }

    //
    public function readMarketers(Request $request)
    {
        try {
            $data = Marketer::getAll($request);
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    public function addOrEditMarketer(Request $request)
    {
        if (isset($request->first_name)) {
            try {
                $user = Marketer::addOrEdit($request);
                return $this->responseJson(null, Response::HTTP_OK, $user);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function deleteMarketer(Request $request)
    {
        if (isset($request->id)) {
            try {
                Marketer::deleteMarketer($request);
                return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function changeStatusMarketer(Request $request)
    {
        if (isset($request->id) && isset($request->status)) {
            try {
                Marketer::changeStatus($request);
                return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }
    }

    //
    public function readVehicleGroups()
    {
        try {
            $data = VehicleGroup::getAll();
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    public function addOrEditVehicleGroup(Request $request)
    {
        if (isset($request->name)) {
            try {
                $user = VehicleGroup::addOrEdit($request);
                return $this->responseJson(null, Response::HTTP_OK, $user);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function deleteVehicleGroup(Request $request)
    {
        if (isset($request->id)) {
            try {
                if (Vehicle::searchCountOfVehicle($request) > 0) {
                    return $this->responseJson(null, Response::HTTP_IM_USED, config('messages.vehicleDelete'));
                } else {
                    VehicleGroup::deleteVehicleGroup($request);
                    return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
                }
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    //
    public function readVehicles()
    {
        try {
            $vehicleGroup = VehicleGroup::getAll();
            $vehicles = Vehicle::getAll();

            $data = (object)[
                'vehicle' => $vehicles,
                'vehicle_group' => $vehicleGroup,
            ];
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    public function addOrEditVehicle(Request $request)
    {
        if (isset($request->name)) {
            try {
                $user = Vehicle::addOrEdit($request);
                return $this->responseJson(null, Response::HTTP_OK, $user);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function deleteVehicle(Request $request)
    {
        if (isset($request->id)) {
            try {
                if (Driver::searchCountOfVehicle($request) > 0) {
                    return $this->responseJson(null, Response::HTTP_IM_USED, config('messages.vehicleDelete'));
                } else {
                    Vehicle::deleteVehicle($request);
                    return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
                }
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    //
    public function readCargoRequests(Request $request)
    {
        try {

            $data = [
                'cargo_request' => CargoRequest::getAll($request),
                'province' => City::getProvince(),
                'vehicle' => Vehicle::getAll()
            ];
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    public function addOrEditCargoRequest(Request $request)
    {

        try {
            $user = CargoRequest::addOrEdit($request);
            return $this->responseJson(null, Response::HTTP_OK, $user);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }

    }

    public function deleteCargoRequest(Request $request)
    {
        if (isset($request->id)) {
            try {
                CargoRequest::deleteCargoRequest($request);
                return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function changeStatusCargoRequest(Request $request)
    {
        if (isset($request->id) && isset($request->last_status)) {
            try {
                CargoRequest::changeStatus($request);
                return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }
    }

    public function searchOwners(Request $request)
    {
        if (isset($request->phone_number)) {
            try {
                $cargoOwners = CargoOwner::searchByPhoneNumber($request);
                $carriers = Carrier::searchByPhoneNumber($request);
                $owners = $cargoOwners->merge($carriers);
                return $this->responseJson(null, Response::HTTP_OK, $owners);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }
    }

    public function searchReceivers(Request $request)
    {
        if (isset($request->phone_number)) {
            try {
                $drivers = Driver::searchByPhoneNumber($request);
                $carriers = Carrier::searchByPhoneNumber($request);
                $receivers = $drivers->merge($carriers);
                return $this->responseJson(null, Response::HTTP_OK, $receivers);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }
    }

    public function searchDrivers(Request $request)
    {
        if (isset($request->phone_number)) {
            try {
                $drivers = Driver::searchByPhoneNumber($request);
                return $this->responseJson(null, Response::HTTP_OK, $drivers);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }
    }

    public function searchCarriers(Request $request)
    {
        if (isset($request->phone_number)) {
            try {
                $carriers = Carrier::searchByPhoneNumber($request);
                return $this->responseJson(null, Response::HTTP_OK, $carriers);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }
    }

    //
    public function readDiscounts()
    {
        try {
            $discount = Discount::getAll();
            $discount_cargo = CargoType::getAll();
            $discount_vehicle = VehicleGroup::getAll();


            $data = [
                'discount' => $discount,
                'cargo' => $discount_cargo,
                'vehicle' => $discount_vehicle
            ];
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    public function addOrEditDiscount(Request $request)
    {
        if (isset($request->title)) {
            try {
                $user = Discount::addOrEdit($request);
                return $this->responseJson(null, Response::HTTP_OK, $user);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function deleteDiscount(Request $request)
    {
        if (isset($request->id)) {
            try {
                Discount::deleteDiscount($request);
                return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function changeStatusDiscount(Request $request)
    {
        if (isset($request->id) && isset($request->status)) {
            try {
                Discount::changeStatus($request);
                return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }
    }

    //
    public function readDashboard()
    {
        try {
            $all = CargoRequest::getCountByDate(null);
            $today = CargoRequest::getCountByDate(Carbon::today()->subDays(0));
            $week = CargoRequest::getCountByDate(Carbon::today()->subDays(7));
            $month = CargoRequest::getCountByDate(Carbon::today()->subDays(30));
            $cargoOwner = CargoOwner::sumCredit(Carbon::today()->subDays(30));
            $driver = Driver::sumCredit(Carbon::today()->subDays(30));
            $carrier = Carrier::sumCredit(Carbon::today()->subDays(30));

            $data = [
                "all" => "$all درخواست ",
                "today" => "$today درخواست ",
                "week" => "$week درخواست ",
                "month" => "$month درخواست ",
                "cargo_owner" => "$cargoOwner تومان ",
                "driver" => "$driver تومان ",
                "carrier" => "$carrier تومان ",
            ];
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    //
    public function readSetting()
    {
        try {
            $data = Setting::getAll();
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    public function editSetting(Request $request)
    {
        if (isset($request->id)) {
            try {
                $user = Setting::edit($request);
                return $this->responseJson(null, Response::HTTP_OK, $user);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    //
    public function readRoles()
    {
        try {
            $data = Role::getAll();
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    public function addOrEditRole(Request $request)
    {
        if (isset($request->name)) {
            try {
                $user = Role::addOrEdit($request);
                return $this->responseJson(null, Response::HTTP_OK, $user);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function deleteRole(Request $request)
    {
        if (isset($request->id)) {
            try {
                Role::deleteRole($request);
                return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    //

    public function readCargoTypes()
    {
        try {
            $data = CargoType::getAll();
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    public function addOrEditCargoType(Request $request)
    {
        if (isset($request->name)) {
            try {
                $user = CargoType::addOrEdit($request);
                return $this->responseJson(null, Response::HTTP_OK, $user);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function deleteCargoType(Request $request)
    {
        if (isset($request->id)) {
            try {
                if (CargoRequest::searchCountOfCargoType($request) > 0) {
                    return $this->responseJson(null, Response::HTTP_IM_USED, config('messages.cargo'));
                } else {
                    CargoType::deleteCargoType($request);
                    return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
                }
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    //

    public function readOperators()
    {
        try {
            $operator = Operator::getAll();
            $role = Role::getAll();
            $data = [
                "operator" => $operator,
                "role" => $role
            ];
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    public function addOrEditOperator(Request $request)
    {
        if (isset($request->first_name)) {
            try {
                $user = Operator::addOrEdit($request);
                return $this->responseJson(null, Response::HTTP_OK, $user);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function deleteOperator(Request $request)
    {
        if (isset($request->id)) {
            try {
                Operator::deleteOperator($request);
                return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function changeStatusOperator(Request $request)
    {
        if (isset($request->id)) {
            try {
                CargoRequest::changeRequestStatus($request->get('id'), $request->get('status'), null, null);
                return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }
    }

    //


    public function readAdvertisingBoxes()
    {
        try {
            $data = AdvertisingBox::getAll();
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    public function addOrEditAdvertisingBox(Request $request)
    {
        if (isset($request->title)) {
            try {
                $user = AdvertisingBox::addOrEdit($request);
                return $this->responseJson(null, Response::HTTP_OK, $user);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function deleteAdvertisingBox(Request $request)
    {
        if (isset($request->id)) {
            try {
                AdvertisingBox::deleteAdvertisingBox($request);
                return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }


    public function readCreditTransactions(Request $request)
    {
        try {
            $data = [
                'credit_transaction' => CreditTransaction::getAll($request),
                'province' => City::getProvince()
            ];
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    public function addOrEditCreditTransaction(Request $request)
    {
        if (isset($request->amount)) {
            try {
                $user = CreditTransaction::addOrEdit($request);
                return $this->responseJson(null, Response::HTTP_OK, $user);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function deleteCreditTransaction(Request $request)
    {
        if (isset($request->id)) {
            try {
                CreditTransaction::deleteCreditTransaction($request);
                return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    //
    public function readMessages(Request $request)
    {
        try {
            $data = Message::getAll($request);
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    public function addOrEditMessage(Request $request)
    {
        if (isset($request->title)) {
            try {
                $user = Message::addOrEdit($request);
                return $this->responseJson(null, Response::HTTP_OK, $user);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function deleteMessage(Request $request)
    {
        if (isset($request->id)) {
            try {
                Message::deleteMessage($request);
                return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function changeStatusMessage(Request $request)
    {
        if (isset($request->id) && isset($request->status)) {
            try {
                Message::changeStatus($request);
                return $this->responseJson(null, Response::HTTP_OK, config('messages.success'));
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }
    }

    public function updateDriverProfilePhoto(Request $request)
    {
        try {
            if (isset($request->img)) {
                $image = Driver::updateProfilePhoto($request->get('id'), $request->img);
                if ($image === null) {
                    return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
                } else {
                    return self::responseJson(null, Response::HTTP_OK, $image);
                }
            } else {
                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    /**======================== Marketer Panel =============================*/

    public function readDashboardMarketer()
    {
        try {
            $all = CargoRequest::getCountByDate(null);
            $today = CargoRequest::getCountByDate(Carbon::today()->subDays(0));
            $week = CargoRequest::getCountByDate(Carbon::today()->subDays(7));
            $month = CargoRequest::getCountByDate(Carbon::today()->subDays(30));

            $data = [
                "all" => "$all درخواست ",
                "today" => "$today درخواست ",
                "week" => "$week درخواست ",
                "month" => "$month درخواست ",
            ];
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    //
    public function readCargoOwnersMarketer(Request $request)
    {
        try {
            $data = [
                'cargoOwner' => CargoOwner::getAllByMarketer($request),
                'province' => City::getProvince(),
            ];
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    //
    public function readCarriersMarketer(Request $request)
    {
        try {
            $data = [
                'carrier' => Carrier::getAllByMarketer($request),
                'province' => City::getProvince()
            ];
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    //
    public function readDriversMarketer(Request $request)
    {
        try {
            $data = [
                'driver' => Driver::getAllByMarketer($request),
                'province' => City::getProvince(),
                'vehicle' => Vehicle::getAll()
            ];
            return $this->responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $e) {
            return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }
}
