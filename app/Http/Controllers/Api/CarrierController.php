<?php

namespace App\Http\Controllers\Api;

use App\CargoRequest;
use App\CargoType;
use App\Carrier;
use App\CarrierDriver;
use App\City;
use App\Comment;
use App\CreditTransaction;
use App\Driver;
use App\Http\Controllers\Controller;
use App\Http\Helpers\SmsPanelHelper;
use App\Message;
use App\Setting;
use App\Vehicle;
use App\VehicleGroup;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CarrierController extends Controller
{
    public static function responseJson($errors, $status, $data)
    {
        return response()->json(array_combine(config('app.response_keys'), [$errors, $status, $data]), $status);
    }

    public function getInitialData()
    {
        try {
            $vehicleGroup = VehicleGroup::getAll();
            $cargoTypes = CargoType::getAll();
            $vehicles = Vehicle::getAll();
            $cities = City::getAll();
            $setting = Setting::getAll();

            $data = (object)[
                'vehicle' => $vehicles,
                'city' => $cities,
                'vehicle_group' => $vehicleGroup,
                'cargo_type' => $cargoTypes,
                'setting' => $setting
            ];
            return self::responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }

    public function addCarrier(Request $request)
    {
        try {

            if (isset($request->username) && isset($request->password)) {

                if (Carrier::checkUser($request) != null){
                    return self::responseJson(null, Response::HTTP_CONFLICT, config('messages.fail'));
                }
                $Carrier = Carrier::addCarrier($request);

                return self::responseJson(null, Response::HTTP_OK, $Carrier);

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }

        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function logIn(Request $request)
    {
        try {

            if (isset($request->username) && isset($request->password)) {

                $Carrier = Carrier::logIn($request->get('username'), $request->get('password'), $request->get('push_notification_token'));

                return self::responseJson(null, Response::HTTP_OK, $Carrier);

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }

        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function forgetPassword(Request $request)
    {
        try {

            if (isset($request->username)) {

                $verificationData = Carrier::forgetPassword($request->get('username'));

                if ($verificationData !== false) {
                    SmsPanelHelper::sendSmsForgetPassword($verificationData->owner_phone_number, $verificationData);

                    return self::responseJson(null, Response::HTTP_OK, config('messages.success'));

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getRequests(Request $request)
    {
        try {

            $CarrierActiveRequests = CargoRequest::getCarrierRequests($request);

            if ($CarrierActiveRequests !== false) {

                return self::responseJson(null, Response::HTTP_OK, $CarrierActiveRequests);

            } else {

                return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function editProfile(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $user = Carrier::editProfile($request);

                if ($user !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $user);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getCarrier(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $user = Carrier::getCarrierByID($request->User()->id);

                if ($user !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $user);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function editCargoRequest(Request $request)
    {
        if (isset($request->api_token)) {
            try {
                $cargoRequest = CargoRequest::edit($request);
                return $this->responseJson(null, Response::HTTP_OK, $cargoRequest);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function getPrice(Request $request)
    {
        if (isset($request->api_token)) {
            try {
                $cargoRequest = CargoRequest::getPrice($request);
                return $this->responseJson(null, Response::HTTP_OK, $cargoRequest);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function addRequestByCarrier(Request $request)
    {
        if (isset($request->api_token)) {
            try {
                $cargoRequest = CargoRequest::addRequestByCarrier($request);
                return $this->responseJson(null, Response::HTTP_OK, $cargoRequest);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function getCarrierMessages(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $CarrierMessages = Message::getCarrierMessages();

                if ($CarrierMessages !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $CarrierMessages);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function removeMessage(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                Message::removeByID($request->get('id'));
                return self::responseJson(null, Response::HTTP_OK, config('messages.success'));

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getCarrierRequestHistoryOwner(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $CarrierRequestHistory = CargoRequest::getCarrierRequestHistoryOwner($request);

                if ($CarrierRequestHistory !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $CarrierRequestHistory);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getCarrierRequestHistoryReceiver(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $CarrierRequestHistory = CargoRequest::getCarrierRequestHistoryReceiver($request);

                if ($CarrierRequestHistory !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $CarrierRequestHistory);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getReceiverCarrierHoverRequests(Request $request)
    {
        try {

            $requests = CargoRequest::getReceiverCarrierHoverRequests($request);

            if ($requests !== false) {

                return self::responseJson(null, Response::HTTP_OK, $requests);

            } else {

                return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getReceiverCarrierActiveRequests(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $requests = CargoRequest::getReceiverCarrierActiveRequests($request);

                if ($requests !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $requests);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getOwnerCarrierHoverRequests(Request $request)
    {
        try {



                $requests = CargoRequest::getOwnerCarrierHoverRequests($request);

                if ($requests !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $requests);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }


        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getOwnerCarrierActiveRequests(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $requests = CargoRequest::getOwnerCarrierActiveRequests($request);

                if ($requests !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $requests);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }


    public function getCarrierTransactionsHistory(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $ownerTransactionsHistory = CreditTransaction::getCarrierTransactionsHistory($request);

                if ($ownerTransactionsHistory !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $ownerTransactionsHistory);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function changeRequestStatus(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                CargoRequest::changeRequestStatus($request->User()->id, $request->get('status'), null, null);
                return self::responseJson(null, Response::HTTP_OK, config('messages.success'));

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function acceptDriverByOwner(Request $request)
    {
        try {
            CargoRequest::acceptDriverByOwner($request);
            return self::responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }

    public function acceptCarrierByOwner(Request $request)
    {
        try {
            CargoRequest::acceptCarrierByOwner($request);
            return self::responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }

    public function rejectDriverByOwner(Request $request)
    {
        try {
            CargoRequest::rejectDriverByOwner($request);
            return self::responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }

    public function cancelByOwner(Request $request)
    {
        try {
            CargoRequest::cancelByOwner($request);
            return self::responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }

    public function cancelByCarrier(Request $request)
    {
        try {
            CargoRequest::cancelByCarrier($request);
            return self::responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }

    public function start(Request $request)
    {
        try {
            CargoRequest::start($request);
            return self::responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }

    public function end(Request $request)
    {
        try {
            CargoRequest::end($request);
            return self::responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }

    public function confirmByOwner(Request $request)
    {
        try {
            CargoRequest::confirmByOwner($request);
            return self::responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }

    public function acceptRequestByCarrier(Request $request)
    {
        try {
            CargoRequest::acceptRequestByCarrier($request);
            return self::responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }

    public function introductionDriverByCarrier(Request $request)
    {
        try {
            $carrier = CarrierDriver::add($request);
            $status = CargoRequest::introductionDriverByCarrier($request);
            if ($status){
                return self::responseJson(null, Response::HTTP_OK, $carrier);
            } else {
                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
            }
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }

    public function addCommentOwner(Request $request)
    {
        try {

            if (isset($request->api_token)) {
                CargoRequest::confirmByOwner($request);
                $comment = Comment::addComment($request);
                if ($comment === null) {
                    return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
                } else {
                    return self::responseJson(null, Response::HTTP_OK, $comment);
                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function addCommentCarrier(Request $request)
    {
        try {

            if (isset($request->api_token)) {
                CargoRequest::confirmByCarrier($request);
                $comment = Comment::addComment($request);
                if ($comment === null) {
                    return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
                } else {
                    return self::responseJson(null, Response::HTTP_OK, $comment);
                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getDriver(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $user = Driver::getDriverByID($request->get('driver_id'));

                if ($user !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $user);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

}
