<?php

namespace App\Http\Controllers\Api;

use App\Buyer;
use App\CargoOwner;
use App\CargoRequest;
use App\CargoType;
use App\Category;
use App\City;
use App\File;
use App\Http\Controllers\Controller;
use App\Http\Helpers\SmsPanelHelper;
use App\Setting;
use App\Vehicle;
use App\VehicleGroup;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BuyerController extends Controller
{
    public static function responseJson($errors, $status, $data)
    {
        return response()->json(array_combine(config('app.response_keys'), [$errors, $status, $data]), $status);
    }

    public function getInitialData()
    {
        try {

            $cities = City::all();
            $banners = File::where('type',config('file.banner'))->get();
            $categories = Category::all();


            $data = (object)[
                'city' => $cities,
                'banner' => $banners,
                'category' => $categories,

            ];
            return self::responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function receivePhoneNumber(Request $request)
    {
        try {

            if (isset($request->phone_number)) {

                $cargoOwner = Buyer::updateOrInsertByPhone($request->get('phone_number'), $request->get('push_notification_token'));

                SmsPanelHelper::sendSms($request->get('phone_number'), $cargoOwner->validation_code);

                return self::responseJson(null, Response::HTTP_OK, config('messages.success'));

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }

        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function verifyPhoneNumber(Request $request)
    {
        try {

            if (isset($request->phone_number) && isset($request->sms_code)) {

                $verificationData = Buyer::verify(
                    $request->get('phone_number'),
                    $request->get('sms_code')
                );

                if ($verificationData !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $verificationData);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getProducts(Request $request)
    {
        try {

            $products = Buyer::getProducts($request);

            if ($products !== false) {

                return self::responseJson(null, Response::HTTP_OK, $products);

            } else {

                return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getStores(Request $request)
    {
        try {

            $products = Buyer::getStores($request);

            if ($products !== false) {

                return self::responseJson(null, Response::HTTP_OK, $products);

            } else {

                return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }
}


