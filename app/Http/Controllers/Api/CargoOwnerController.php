<?php

namespace App\Http\Controllers\Api;

use App\CargoOwner;
use App\CargoRequest;
use App\CargoType;
use App\City;
use App\Comment;
use App\CreditTransaction;
use App\Driver;
use App\Http\Controllers\Controller;
use App\Http\Helpers\FirebaseHelper;
use App\Http\Helpers\SmsPanelHelper;
use App\Message;
use App\Setting;
use App\Vehicle;
use App\VehicleGroup;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CargoOwnerController extends Controller
{
    public static function responseJson($errors, $status, $data)
    {
        return response()->json(array_combine(config('app.response_keys'), [$errors, $status, $data]), $status);
    }

    public function getInitialData()
    {
        try {
            $vehicleGroup = VehicleGroup::getAll();
            $cargoTypes = CargoType::getAll();
            $vehicles = Vehicle::getAll();
            $cities = City::getAll();
            $setting = Setting::getAll();

            $data = (object)[
                'vehicle' => $vehicles,
                'city' => $cities,
                'vehicle_group' => $vehicleGroup,
                'cargo_type' => $cargoTypes,
                'setting' => $setting
            ];
            return self::responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function receivePhoneNumber(Request $request)
    {
        try {

            if (isset($request->phone_number)) {

                $cargoOwner = CargoOwner::updateOrInsertByPhone($request->get('phone_number'), $request->get('push_notification_token'));

                SmsPanelHelper::sendSms($request->get('phone_number'), $cargoOwner->validation_code);

                return self::responseJson(null, Response::HTTP_OK, config('messages.success'));

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }

        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function verifyPhoneNumber(Request $request)
    {
        try {

            if (isset($request->phone_number) && isset($request->sms_code)) {

                $verificationData = CargoOwner::verify(
                    $request->get('phone_number'),
                    $request->get('sms_code')
                );

                if ($verificationData !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $verificationData);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function editProfile(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $user = CargoOwner::editProfile($request);

                if ($user !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $user);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getCargoOwner(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $user = CargoOwner::getCargoOwnerByID($request->User()->id);

                if ($user !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $user);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function editCargoRequest(Request $request)
    {
        if (isset($request->api_token)) {
            try {
                $cargoRequest = CargoRequest::edit($request);
                return $this->responseJson(null, Response::HTTP_OK, $cargoRequest);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function getPrice(Request $request)
    {
        if (isset($request->api_token)) {
            try {
                $cargoRequest = CargoRequest::getPrice($request);
                return $this->responseJson(null, Response::HTTP_OK, $cargoRequest);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function addRequestByCargoOwner(Request $request)
    {
        if (isset($request->api_token)) {
            try {
                $cargoRequest = CargoRequest::addRequestByCargoOwner($request);
                return $this->responseJson(null, Response::HTTP_OK, $cargoRequest);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function acceptDriverByOwner(Request $request)
    {
        try {
            CargoRequest::acceptDriverByOwner($request);
            return self::responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }


    public function acceptCarrierByOwner(Request $request)
    {
        try {
            CargoRequest::acceptCarrierByOwner($request);
            return self::responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }


    public function rejectDriverByOwner(Request $request)
    {
        try {
            CargoRequest::rejectDriverByOwner($request);
            return self::responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }

    public function cancelByOwner(Request $request)
    {
        try {
            CargoRequest::cancelByOwner($request);
            return self::responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }

    public function confirmByOwner(Request $request)
    {
        try {
            CargoRequest::confirmByOwner($request);
            return self::responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }

    public function getCargoOwnerMessages(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $cargoOwnerMessages = Message::getCargoOwnerMessages();

                if ($cargoOwnerMessages !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $cargoOwnerMessages);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function removeMessage(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                Message::removeByID($request->get('id'));
                return self::responseJson(null, Response::HTTP_OK, config('messages.success'));

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getCargoOwnerRequestHistory(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $cargoOwnerRequestHistory = CargoRequest::getCargoOwnerRequestHistory($request);

                if ($cargoOwnerRequestHistory !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $cargoOwnerRequestHistory);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getCargoOwnerHoverRequests(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $cargoOwnerHoverRequests = CargoRequest::getCargoOwnerHoverRequests($request);

                if ($cargoOwnerHoverRequests !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $cargoOwnerHoverRequests);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getCargoOwnerActiveRequests(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $cargoOwnerActiveRequests = CargoRequest::getCargoOwnerActiveRequests($request);

                if ($cargoOwnerActiveRequests !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $cargoOwnerActiveRequests);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getCargoOwnerTransactionsHistory(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $ownerTransactionsHistory = CreditTransaction::getCargoOwnerTransactionsHistory($request);

                if ($ownerTransactionsHistory !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $ownerTransactionsHistory);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function changeRequestStatus(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                CargoRequest::changeRequestStatus($request->User()->id, $request->get('status'), null, null);
                return self::responseJson(null, Response::HTTP_OK, config('messages.success'));

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function addComment(Request $request)
    {
        try {

            if (isset($request->api_token)) {
                CargoRequest::confirmByOwner($request);
                $comment = Comment::addComment($request);
                if ($comment === null) {
                    return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
                } else {
                    return self::responseJson(null, Response::HTTP_OK, $comment);
                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getDriver(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $driver = Driver::getDriverByID($request->get('driver_id'));
                if ($driver === null) {
                    return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
                } else {
                    return self::responseJson(null, Response::HTTP_OK, $driver);
                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function sendRoute(Request $request)
    {
        try {

            if (isset($request->api_token)) {
                $data = [
                    'origin_lat' => $request->get('origin_lat'),
                    'origin_lng' => $request->get('origin_lng'),
                    'destination_lat' => $request->get('destination_lat'),
                    'destination_lng' => $request->get('destination_lng'),
                    'type' => config('fcmsettings.web')
                ];
                FirebaseHelper::sendFcmNotificationMessage([$request->User()->push_notification_token], $data, $data, config('fcmsettings.web'));
                return self::responseJson(null, Response::HTTP_OK, config('messages.success'));

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }


}
