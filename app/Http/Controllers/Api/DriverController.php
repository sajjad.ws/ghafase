<?php

namespace App\Http\Controllers\Api;

use App\CargoRequest;
use App\CargoType;
use App\City;
use App\Comment;
use App\CreditTransaction;
use App\Driver;
use App\Http\Controllers\Controller;
use App\Http\Helpers\SmsPanelHelper;
use App\Message;
use App\Setting;
use App\Vehicle;
use App\VehicleGroup;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DriverController extends Controller
{
    public static function responseJson($errors, $status, $data)
    {
        return response()->json(array_combine(config('app.response_keys'), [$errors, $status, $data]), $status);
    }

    public function getInitialData()
    {
        try {
            $vehicleGroup = VehicleGroup::getAll();
            $cargoTypes = CargoType::getAll();
            $vehicles = Vehicle::getAll();
            $cities = City::getAll();
            $setting = Setting::getAll();

            $data = (object)[
                'vehicle' => $vehicles,
                'city' => $cities,
                'vehicle_group' => $vehicleGroup,
                'cargo_type' => $cargoTypes,
                'setting' => $setting
            ];
            return self::responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }

    public function receivePhoneNumber(Request $request)
    {
        try {

            if (isset($request->phone_number)) {

                $Driver = Driver::updateOrInsertByPhone($request->get('phone_number'),   $request->get('push_notification_token'));

                SmsPanelHelper::sendSms($request->get('phone_number'), $Driver->validation_code);

                return self::responseJson(null, Response::HTTP_OK, config('messages.success'));

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }

        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function verifyPhoneNumber(Request $request)
    {
        try {

            if (isset($request->phone_number) && isset($request->sms_code)) {

                $verificationData = Driver::verify(
                    $request->get('phone_number'),
                    $request->get('sms_code')
                );

                if ($verificationData !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $verificationData);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getRequests(Request $request)
    {
        try {

            $DriverActiveRequests = CargoRequest::getDriverRequests($request);

            if ($DriverActiveRequests !== false) {

                return self::responseJson(null, Response::HTTP_OK, $DriverActiveRequests);

            } else {

                return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function acceptRequestByDriver(Request $request)
    {
        try {
            CargoRequest::acceptRequestByDriver($request);
            return self::responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }

    public function cancelByDriver(Request $request)
    {
        try {
            CargoRequest::cancelByDriver($request);
            return self::responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }

    public function start(Request $request)
    {
        try {
            CargoRequest::start($request);
            return self::responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }

    public function end(Request $request)
    {
        try {
            CargoRequest::end($request);
            return self::responseJson(null, Response::HTTP_OK, config('messages.success'));
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }


    public function editProfile(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $user = Driver::editProfile($request);

                if ($user !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $user);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getDriver(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $user = Driver::getDriverByID($request->User()->id);

                if ($user !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $user);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function addOrEditCargoRequest(Request $request)
    {
        if (isset($request->phone_number)) {
            try {
                $cargoRequest = CargoRequest::edit($request);
                return $this->responseJson(null, Response::HTTP_OK, $cargoRequest);
            } catch (\Exception $e) {
                return $this->responseJson($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
            }
        } else {
            return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
        }

    }

    public function getDriverMessages(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $DriverMessages = Message::getDriverMessages();

                if ($DriverMessages !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $DriverMessages);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function removeMessage(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                Message::removeByID($request->get('id'));
                return self::responseJson(null, Response::HTTP_OK, config('messages.success'));

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getDriverRequestHistory(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $DriverRequestHistory = CargoRequest::getDriverRequestHistory($request);

                if ($DriverRequestHistory !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $DriverRequestHistory);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getDriverHoverRequests(Request $request)
    {
        try {

            $DriverHoverRequests = CargoRequest::getDriverHoverRequests($request);

            if ($DriverHoverRequests !== false) {

                return self::responseJson(null, Response::HTTP_OK, $DriverHoverRequests);

            } else {

                return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getDriverActiveRequests(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $DriverActiveRequests = CargoRequest::getDriverActiveRequests($request);

                if ($DriverActiveRequests !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $DriverActiveRequests);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getDriverTransactionsHistory(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $ownerTransactionsHistory = CreditTransaction::getDriverTransactionsHistory($request);

                if ($ownerTransactionsHistory !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $ownerTransactionsHistory);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function changeRequestStatus(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                CargoRequest::changeRequestStatus($request->User()->id, $request->get('status'), null, null);
                return self::responseJson(null, Response::HTTP_OK, config('messages.success'));

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function updateProfilePhoto(Request $request)
    {
        try {

            if (isset($request->photo)) {
                $image = Driver::updateProfilePhoto($request->User()->id,$request->photo);
                if ($image === null){
                    return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
                } else {
                    return self::responseJson(null, Response::HTTP_OK, $image);
                }
            } else {
                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function addComment(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $comment = Comment::addComment($request);
                if ($comment === null){
                    return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
                } else {
                    return self::responseJson(null, Response::HTTP_OK, $comment);
                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

}
