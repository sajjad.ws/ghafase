<?php

namespace App\Http\Controllers\Api;

use App\CargoOwner;
use App\CargoRequest;
use App\CargoType;
use App\Carrier;
use App\City;
use App\Comment;
use App\Driver;
use App\Http\Controllers\Controller;
use App\Http\Helpers\SmsPanelHelper;
use App\Marketer;
use App\Message;
use App\Setting;
use App\Vehicle;
use App\VehicleGroup;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MarketerController extends Controller
{

    public static function responseJson($errors, $status, $data)
    {
        $response = response()->json(array_combine(config('app.response_keys'), [$errors, $status, $data]), $status);
/*        $response->header('Access-Control-Allow-Origin', '*');
        $response->header('Access-Control-Allow-Headers', 'Origin, Content-Type, Cookie, X-CSRF-TOKEN, Accept, Authorization, X-XSRF-TOKEN, Access-Control-Allow-Origin');
        $response->header('Access-Control-Expose-Headers', 'Authorization, authenticated');
        $response->header('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, OPTIONS');
        $response->header('Access-Control-Allow-Credentials', 'true');*/
        return $response;
    }


    public function getInitialData()
    {

        try {
            $vehicleGroup = VehicleGroup::getAll();
            $cargoTypes = CargoType::getAll();
            $vehicles = Vehicle::getAll();
            $cities = City::getAll();
            $setting = Setting::getAll();

            $data = (object)[
                'vehicle' => $vehicles,
                'city' => $cities,
                'vehicle_group' => $vehicleGroup,
                'cargo_type' => $cargoTypes,
                'setting' => $setting
            ];
            return self::responseJson(null, Response::HTTP_OK, $data);
        } catch (\Exception $exception) {
            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);
        }
    }


    public function addMarketerTest(Request $request)
    {
        try {

            if (isset($request->username) && isset($request->password)) {

                $marketerTest = Marketer::addMarketerTest($request->get('username'),$request->get('password'));

//                SmsPanelHelper::sendSms($request->get('phone_number'), $customer->temp_code);

                return self::responseJson(null, Response::HTTP_OK, config('messages.success'));

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }

        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function logIn(Request $request)
    {
        try {

            if (isset($request->username) && isset($request->password)) {

                $Marketer = Marketer::logIn($request->get('username'),$request->get('password'));

//                SmsPanelHelper::sendSms($request->get('phone_number'), $customer->temp_code);

                return self::responseJson(null, Response::HTTP_OK, $Marketer);

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }

        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function forgetPassword(Request $request)
    {
        try {

            if (isset($request->username)) {

                $verificationData = Marketer::forgetPassword($request->get('username'));

                if ($verificationData !== false) {
                    SmsPanelHelper::sendSmsForgetPassword($verificationData->phone_number, $verificationData);

                    return self::responseJson(null, Response::HTTP_OK, config('messages.success'));

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function editProfile(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $user = Marketer::editProfile($request);

                if ($user !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $user);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getMarketer(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $user = Marketer::getMarketerByID($request->User()->id);

                if ($user !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $user);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getMarketerMessages(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $MarketerMessages = Message::getMarketerMessages();

                if ($MarketerMessages !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $MarketerMessages);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function removeMessage(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                Message::removeByID($request->get('id'));
                return self::responseJson(null, Response::HTTP_OK, config('messages.success'));

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function addCargoOwner(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $cargoOwner = CargoOwner::addByMarketer($request);

                if ($cargoOwner !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $cargoOwner);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getCargoOwnersByMarketer(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $cargoOwnersByMarketer = CargoOwner::getCargoOwnersByMarketer($request);

                if ($cargoOwnersByMarketer !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $cargoOwnersByMarketer);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getDriversByMarketer(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $driversByMarketer = Driver::getDriversByMarketer($request);

                if ($driversByMarketer !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $driversByMarketer);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getCarriersByMarketer(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $carriersByMarketer = Carrier::getCarriersByMarketer($request);

                if ($carriersByMarketer !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $carriersByMarketer);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    //////

    public function addCarrier(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $carrier = Carrier::addByMarketer($request);

                if ($carrier !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $carrier);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function addDriver(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $driver = Driver::addByMarketer($request);

                if ($driver !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $driver);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    /////

    public function getCargoOwnerRequestHistoryByMarketer(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $cargoOwnerRequestHistoryByMarketer = CargoRequest::getCargoOwnerRequestByMarketer($request);

                if ($cargoOwnerRequestHistoryByMarketer !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $cargoOwnerRequestHistoryByMarketer);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getDriverRequestHistoryByMarketer(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $driverRequestHistoryByMarketer = CargoRequest::getDriverRequestHistoryByMarketer($request);

                if ($driverRequestHistoryByMarketer !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $driverRequestHistoryByMarketer);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function getCarrierRequestHistoryByMarketer(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $carrierRequestHistoryByMarketer = CargoRequest::getCarrierRequestHistoryByMarketer($request);

                if ($carrierRequestHistoryByMarketer !== false) {

                    return self::responseJson(null, Response::HTTP_OK, $carrierRequestHistoryByMarketer);

                } else {

                    return self::responseJson(null, Response::HTTP_OK, config('messages.fail'));

                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }

    public function addComment(Request $request)
    {
        try {

            if (isset($request->api_token)) {

                $comment = Comment::addComment($request);
                if ($comment === null){
                    return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));
                } else {
                    return self::responseJson(null, Response::HTTP_OK, $comment);
                }

            } else {

                return self::responseJson(null, Response::HTTP_BAD_REQUEST, config('messages.fail'));

            }
        } catch (\Exception $exception) {

            return self::responseJson($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, null);

        }
    }


}
