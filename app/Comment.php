<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * @property integer $id
 * @property integer $cargo_request_id
 * @property string $content
 * @property boolean $receiver_type
 * @property integer $receiver_id
 * @property boolean $cargo_request_point
 * @property string $created_at
 * @property string $updated_at
 * @property CargoRequest $cargoRequest
 */
class Comment extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'comment';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['cargo_request_id', 'content', 'receiver_type', 'receiver_id', 'cargo_request_point', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cargoRequest()
    {
        return $this->belongsTo('App\CargoRequest');
    }

    public static function addComment(Request $request)
    {
        $comment = new Comment();
        $cargoRequest = CargoRequest::where('id',$request->get('cargo_request_id'))->first();
        if ($cargoRequest->owner_id === $request->User()->id){
            if ($cargoRequest->receiver_type === config('receiver.driver')){
                $comment->receiver_type = config('commentType.driver');
            } else {
                $comment->receiver_type = config('commentType.carrierReceiver');
            }
            $comment->receiver_id = $cargoRequest->receiver_id;
        } else if ($cargoRequest->receiver_id === $request->User()->id){
            if ($cargoRequest->owner_type === config('owner.cargoOwner')){
                $comment->receiver_type = config('commentType.cargoOwner');
            } else {
                $comment->receiver_type = config('commentType.carrierOwner');
            }
            $comment->receiver_id = $cargoRequest->owner_id;
        } else {
            return null;
        }
        $comment->cargo_request_point = $request->get('score');
        $comment->cargo_request_id = $request->get('cargo_request_id');
        $comment->content = $request->get('content');
        return $comment->save();
    }


}
