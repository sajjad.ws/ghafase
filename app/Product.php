<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $category_id
 * @property integer $store_id
 * @property string $name
 * @property string $image_uri
 * @property integer $price
 * @property string $p_date
 * @property string $e_date
 * @property int $discount
 * @property int $number
 * @property string $created_at
 * @property string $updated_at
 * @property Category $category
 * @property Store $store
 * @property Order[] $orders
 */
class Product extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';
    protected $appends = ['discount_price','time_to_expire'];
    /**
     * @var array
     */
    protected $fillable = ['category_id', 'store_id', 'name', 'image_uri', 'price', 'p_date', 'e_date', 'discount', 'number', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function store()
    {
        return $this->belongsTo('App\Store');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function orders()
    {
        return $this->belongsToMany('App\Order');
    }


    public function getDiscountPriceAttribute()
    {

        $price = Product::where('id', $this->id)->first()->price;
        $discount = Product::where('id', $this->id)->first()->discount;

        $discountPrice = $price - ($price * ($discount/100));

        return $discountPrice;

    }

    public function getTimeToExpireAttribute()
    {

        try {
            $datetime1 = new DateTime($this->e_date);

        $datetime2 = new DateTime($this->p_date);
        $interval = $datetime1->diff($datetime2);
        $days = $interval->format('%a');//now do whatever you like with $days
    } catch (\Exception $e) {
}
        return $days;

    }
}
