<?php
return [
    'add' => 0,
    'acceptByDriver' => 1,
    'acceptByCarrier' => 2,
    'introductionDriverByCarrier' => 3,
    'acceptDriverByOwner' => 4,
    'acceptCarrierByOwner' => 5,
    'cancelByOwner' => 6,
    'cancelByDriver' => 7,
    'cancelByCarrier' => 8,
    'start' => 9,
    'end' => 10,
    'confirmByOwner' => 11,
];
