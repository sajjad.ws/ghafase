<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Defaults
    |--------------------------------------------------------------------------
    |
    | This option controls the default authentication "guard" and password
    | reset options for your application. You may change these defaults
    | as required, but they're a perfect start for most applications.
    |
    */

    'defaults' => [
        'guard' => 'operator'
    ],

    /*
    |--------------------------------------------------------------------------
    | Authentication Guards
    |--------------------------------------------------------------------------
    |
    | Next, you may define every authentication guard for your application.
    | Of course, a great default configuration has been defined for you
    | here which uses session storage and the Eloquent user provider.
    |
    | All authentication drivers have a user provider. This defines how the
    | users are actually retrieved out of your database or other storage
    | mechanisms used by this application to persist your user's data.
    |
    | Supported: "session", "token"
    |
    */

    'guards' => [
        'operator' => [
            'driver' => 'jwt',
            'provider' => 'operator',
        ],
        'carrier' => [
            'driver' => 'token',
            'provider' => 'carrier',
        ],
        'driver' => [
            'driver' => 'token',
            'provider' => 'driver',
        ],
        'cargo_owner' => [
            'driver' => 'token',
            'provider' => 'cargo_owner',
        ],
        'marketer' => [
            'driver' => 'token',
            'provider' => 'marketer',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | User Providers
    |--------------------------------------------------------------------------
    |
    | All authentication drivers have a user provider. This defines how the
    | users are actually retrieved out of your database or other storage
    | mechanisms used by this application to persist your user's data.
    |
    | If you have multiple user tables or models you may configure multiple
    | sources which represent each model / table. These sources may then
    | be assigned to any extra authentication guards you have defined.
    |
    | Supported: "database", "eloquent"
    |
    */

    'providers' => [
        'operator' => [
            'driver' => 'eloquent',
            'model' => App\Operator::class,
        ],
        'carrier' => [
            'driver' => 'eloquent',
            'model' => App\Carrier::class,
        ],
        'driver' => [
            'driver' => 'eloquent',
            'model' => App\Driver::class,
        ],
        'cargo_owner' => [
            'driver' => 'eloquent',
            'model' => App\CargoOwner::class,
        ],
        'marketer' => [
            'driver' => 'eloquent',
            'model' => App\Marketer::class,
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Resetting Passwords
    |--------------------------------------------------------------------------
    |
    | You may specify multiple password reset configurations if you have more
    | than one user table or model in the application and you want to have
    | separate password reset settings based on the specific user types.
    |
    | The expire time is the number of minutes that the reset token should be
    | considered valid. This security feature keeps tokens short-lived so
    | they have less time to be guessed. You may change this as needed.
    |
    */

];
