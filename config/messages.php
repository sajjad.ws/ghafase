<?php
return [
    'success' => 'success',
    'fail' => 'fail',
    'vehicleDelete' => 'این ماشین قابل حذف نمیباشد.',
    'cargoOwnerCheck' => 'َشماره موبایل یا کد ملی تکراری است.',
    'cargo' => 'این بار قابل حذف نمیباشد.',

];
