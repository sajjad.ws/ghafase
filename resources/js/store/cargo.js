import {readCargos, addOrEditCargo, deleteCargo, cancelCargo, getCityByProvince, searchOwners, searchReceivers} from "../data/api";

const state = {
    allData: [],
    province: [],
    city: [],
    searchedOwners: [],
    searchedReceiver: [],
    searchedReceiverAdd: [],
    cargoStatus: [
        {index: 0, text: "در حال جست و جوی راننده"},
        {index: 1, text: "قبول شده توسط راننده"},
        {index: 2, text: "قبول شده توسط باربری"},
        {index: 3, text: "معرفی راننده توسط باربری"},
        {index: 4, text: "تایید راننده توسط صاحب بار"},
        {index: 5, text: "تایید باربری توسط صاحب بار"},
        {index: 6, text: "لغو شده توسط ساحب بار"},
        {index: 7, text: "لغو شده توسط راننده"},
        {index: 8, text: "لغو شده توسط باربری"},
        {index: 9, text: "شروع بارگیری"},
        {index: 10, text: "اتمام سفر"},
        {index: 11, text: "تایید توسط ساحب بار"},
    ],
    cityPending: false,
    allDataLoading: false,
    pending: false,
    AddDialog: false,
    DeleteDialog: false,
    detailsDialog: false,
    selectCity: false,
    isLoading: false,
    isLoadingAdd: false,
    WaitDialog:false,
    selectedProvince: {id: null, name: null, pid: null, created_at: null, updated_at: null},
    selectedCity: {id: null, name: null},
    selectedItem: {
        id: null,
        cargo_cost: null,
        cargo_cost_min: null,
        cargo_cost_max: null,
        cargo_type: null,
        cargo_type_id: null,
        cargo_weight: null,
        cargo_weight_min: null,
        cargo_weight_max: null,
        city_destination: null,
        city_origin: null,
        created_at: null,
        description: null,
        destination_address: null,
        destination_city_id: null,
        destination_latitude: null,
        destination_longitude: null,
        evacuation_end_time: null,
        evacuation_start_time: null,
        last_status: null,
        loading_end_time: null,
        loading_start_time: null,
        message_to_driver: null,
        origin_address: null,
        origin_city_id: null,
        origin_latitude: null,
        origin_longitude: null,
        owner: null,
        owner_id: null,
        point: null,
        price: null,
        receiver: null,
        receiver_id: null,
        updated_at: null,
        vehicle: null,
        vehicle_id: null,
    },
    filter: {
        status: null,
        receiver: null,
        owner: null,
        receiverPhoneNumber: null,
        ownerPhoneNumber: null,
        phone_number: null,
        start_time: null,
        end_time: null
    },
};

// getters
const getters = {};

// actions
const actions = {
    readData({commit}) {
        commit('setAllDataLoading', true);
        commit('setData', []);
        readCargos().then(jsonResponse => {
            commit('setData', jsonResponse.data.data.cargo_request);
            commit('setProvince', jsonResponse.data.data.province);
            commit('setAllDataLoading', false);
            console.log(jsonResponse.data.data.cargo_request);
        }).catch(e => console.log(e));
    },
    addOrEdit({commit, dispatch}) {
        commit('setPending', true);
        addOrEditCargo().then(jsonResponse => {
            commit('setPending', false);
            if (jsonResponse.data.status === 200) {
                commit('hideAddDialog')
                dispatch('readData')
            }
        }).catch(e => {
            commit('setPending', false);
        });
    },
    deleteOne({commit, dispatch}) {
        commit('setPending', true);
        deleteCargo().then(jsonResponse => {
            commit('setPending', false);
            commit('hideDeleteDialog')
            if (jsonResponse.data.status === 200) {
                dispatch('readData')
            }
        });
    },
    cancelCargo({commit, dispatch}) {
        commit('showWaitDialog');
        cancelCargo().then(jsonResponse => {
            commit('hideWaitDialog');
            if (jsonResponse.data.status === 200) {
                dispatch('readData')
            }
        });
    },
    getCityByProvince({commit, state}) {
        commit('showCityPending');
        getCityByProvince(state.selectedProvince.id).then(jsonResponse => {
            commit('hideCityPending');
            if (jsonResponse.data.status === 200) {
                commit('setCity', jsonResponse.data.data);
            }
        });
    },
    searchOwner({commit}, val) {
        state.filter.ownerPhoneNumber = val;
        if (val.toString().length < 4) return;
        commit('setIsLoading', true);
        searchOwners(val).then(jsonResponse => {
            commit('setIsLoading', false);
            commit('setSearchedOwners', jsonResponse.data.data)
        });
    },

    searchReceiver({commit}, val) {
        state.filter.receiverPhoneNumber = val;
        if (val.toString().length < 3) return;
        commit('setIsLoading', true);
        searchReceivers(val).then(jsonResponse => {
            commit('setIsLoading', false);
            commit('setSearchedReceivers', jsonResponse.data.data)
        });
    },

    searchReceiverAdd({commit}, val) {
        state.filter.receiverPhoneNumber = val;
        if (val.toString().length < 3) return;
        commit('setIsLoadingAdd', true);
        searchReceivers(val).then(jsonResponse => {
            commit('setIsLoadingAdd', false);
            commit('setSearchedReceiversAdd', jsonResponse.data.data)
            console.log(jsonResponse.data.data)
        });
    },
    removeFilter({commit, dispatch}){
        commit('removeFilter', true);
        dispatch('readData');
    }
};

// mutations
const mutations = {
    ['removeFilter'](state){
      state.filter = {
          status: null,
          receiver: null,
          owner: null,
          receiverPhoneNumber: null,
          ownerPhoneNumber: null,
          phone_number: null,
          start_time: null,
          end_time: null
      }
    },
    ['setData'](state, data) {
        state.allData = data;
    },
    ['setProvince'](state, data) {
        state.province = data;
    },
    ['setCity'](state, data) {
        state.city = data;
    },
    ['setStatus'](state, data) {
        state.filter.status = data;
    },
    ['setFilterPhoneNumber'](state, data) {
        state.filter.phone_number = data;
    },
    ['setSelectedProvince'](state, data) {
        state.selectedProvince = data;
    },
    ['setSelectedCity'](state, data) {
        state.selectedCity = data;
    },
    ['setSelectedItemCity'](state) {
        state.selectedcity = state.selectedCity;
    },
    ['showAddDialog'](state) {
        state.AddDialog = true;
    },
    ['hideAddDialog'](state) {
        state.AddDialog = false;
    },
    ['showDeleteDialog'](state) {
        state.DeleteDialog = true;
    },
    ['hideDeleteDialog'](state) {
        state.DeleteDialog = false;
    },
    ['setSelectedItem'](state, selectedItem) {
        state.selectedItem = selectedItem;
    },
    ['setPending'](state, value) {
        state.pending = value;
    },
    ['setFreightName'](state, value) {
        state.selectedfreight_name = value;
    },
    ['setFirstName'](state, value) {
        state.selectedowner_first_name = value;
    },
    ['setLastName'](state, value) {
        state.selectedowner_last_name = value;
    },
    ['setUserName'](state, value) {
        state.selectedusername = value;
    },
    ['setPassword'](state, value) {
        state.selectednew_password = value;
    },
    ['setPhoneNumber'](state, value) {
        state.selectedphone_number = value;
    },
    ['setTelephoneNumber'](state, value) {
        state.selectedte = value;
    },
    ['setAllDataLoading'](state, value) {
        state.allDataLoading = value;
    },
    ['showDetailsDialog'](state) {
        state.detailsDialog = true;
    },
    ['hideDetailsDialog'](state) {
        state.detailsDialog = false;
    },
    ['showSelectCity'](state) {
        state.selectCity = true;
    },
    ['hideSelectCity'](state) {
        state.selectCity = false;
    },
    ['showCityPending'](state) {
        state.cityPending = true;
    },
    ['hideCityPending'](state) {
        state.cityPending = false;
    },
    ['setStartTime'](state, value) {
        state.filter.start_time = value;
    },
    ['setEndTime'](state, value) {
        state.filter.end_time = value;
    },
    //
    ['setSearchedOwners'](state, data) {
        state.searchedOwners = data;
    },
    ['setSearchedReceivers'](state, data) {
        state.searchedReceiver = data;
    },
    ['setSelectedOwner'](state, data) {
        state.filter.owner = data;
    },
    ['setSelectedReceiver'](state, data) {
        state.filter.receiver = data;
    },
    ['setIsLoading'](state, value) {
        state.isLoading = value;
    },
    //
    ['setSearchedReceiversAdd'](state, data) {
        state.searchedReceiverAdd = data;
    },
    ['setSelectedReceiverAdd'](state, data) {
        state.selectedreceiver = data;
        state.selectedreceiver_id = data.id;
        state.selectedItem.receiver_id = data.id;
        if (data.hasOwnProperty("freight_name")){
            state.selectedreceiver_type = 1;
            state.selectedItem.receiver_type = 1;
        }
        else {
            state.selectedreceiver_type = 0;
            state.selectedItem.receiver_type = 0;
        }
    },
    ['setIsLoadingAdd'](state, value) {
        state.isLoadingAdd = value;
    },
    ['showWaitDialog'](state) {
        state.WaitDialog = true;
    },
    ['hideWaitDialog'](state) {
        state.WaitDialog = false;
    },
    ['setLoadingStartTime'](state, value) {
        state.selectedItem.loading_start_time = value;
    },
    ['setEvacuationStartTime'](state, value) {
        state.selectedItem.evacuation_start_time = value;
    },
    ['setMessageToDriver'](state, value) {
        state.selectedItem.message_to_driver = value;
    },
    ['setDescription'](state, value) {
        state.selectedItem.description = value;
    },
    ['setCargoWeightMax'](state, value) {
        state.selectedItem.cargo_weight_max = value;
    },
    ['setCargoWeightMin'](state, value) {
        state.selectedItem.cargo_weight_min = value;
    },
    ['setCargoCostMax'](state, value) {
        state.selectedItem.cargo_cost_max = value;
    },
    ['setCargoCostMin'](state, value) {
        state.selectedItem.cargo_cost_min = value;
    },
    ['setDestinationAddress'](state, value) {
        state.selectedItem.destination_address = value;
    },
    ['setOriginAddress'](state, value) {
        state.selectedItem.origin_address = value;
    },

};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
