import {readSetting, editSetting} from "../data/api";

const state = {
    dataLoading: false,
    WaitDialog:false,
    selectedItem: {
        id: null,
        driver_regulation: null,
        carrier_regulation: null,
        cargo_registration_regulation: null,
        driver_confirmation_regulation: null,
        cost_estimation_status: 0,
        cost_estimation_value: null,
        carrier_driver_introduction: null,
        cargo_cancelable_time: null,
        driver_confirmation_time: null,
        system_status: null,
        is_active_registrar_commission: null,
        is_active_receiver_commission: null,
    },
};

// getters
const getters = {};

// actions
const actions = {
    readData({commit}) {

        commit('setDataLoading', true);
        readSetting().then(jsonResponse => {
            commit('setSelectedItem', jsonResponse.data.data);
            console.log(jsonResponse.data.data)
            commit('setDataLoading', false);
        });
    },
    editSetting({commit, dispatch}) {
        if (state.selectedItem.driver_confirmation_regulation.length > 255) return;
        commit('showWaitDialog');
        editSetting().then(jsonResponse => {
            commit('hideWaitDialog');
            if (jsonResponse.data.status === 200) {
                dispatch('readData')
            }
        });
    },
};

// mutations
const mutations = {
    ['setSelectedItem'](state, data) {
        state.selectedItem = data;
    },
    ['setConfirmationTime'](state, data) {
        state.selectedItem.driver_confirmation_time = data;
    },
    ['setCancelableTime'](state, value) {
        state.selectedItem.cargo_cancelable_time = value;
    },
    ['setCostEstimationValue'](state, value) {
        state.selectedItem.cost_estimation_value = value;
    },
    ['setCarrierDriverTime'](state, value) {
        state.selectedItem.carrier_driver_introduction = value;
    },
    ['setDriverConfirmationRegulation'](state, value) {
        state.selectedItem.driver_confirmation_regulation = value;
    },
    ['setCargoRegistrationRegulation'](state, value) {
        state.selectedItem.cargo_registration_regulation = value;
    },
    ['setDriverRegulation'](state, value) {
        state.selectedItem.driver_regulation = value;
    },
    ['setCarrierRegulation'](state, value) {
        state.selectedItem.carrier_regulation = value;
    },
    ['setDataLoading'](state, value) {
        state.dataLoading = value;
    },
    ['showWaitDialog'](state) {
        state.WaitDialog = true;
    },
    ['hideWaitDialog'](state) {
        state.WaitDialog = false;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
