import Vue from "vue"
import Vuex from "vuex"
import axios from 'axios'
import VueAxios from 'vue-axios'
import login from './login';
import main from './main';
import cargoOwner from './cargoOwner';
import driver from './driver';
import advertise from './advertise';
import cargo from './cargo';
import cargoType from './cargoType';
import carrier from './carrier';
import discount from './discount';
import dashboard from './dashboard';
import marketer from './marketer';
import message from './message';
import operator from './operator';
import role from './role';
import setting from './setting';
import transaction from './transaction';
import vehicle from './vehicle';
import vehicleGroup from './vehicleGroup';
import marketerMain from './marketer/main'
import marketerCargoOwner from './marketer/cargoOwner'
import marketerCarrier from './marketer/carrier'
import marketerDriver from './marketer/driver'
import marketerDashboard from './marketer/dashboard'
import forgetPassword from './forgetPassword'
import resetPassword from './resetPassword'
import mapCargoAddress from './cargoOwner/mapCargoAddress'

Vue.use(Vuex, VueAxios, axios);

export default new Vuex.Store({
    namespaced: true,
    modules: {
        main,
        login,
        cargoOwner,
        driver,
        advertise,
        cargo,
        cargoType,
        carrier,
        discount,
        dashboard,
        marketer,
        message,
        operator,
        role,
        setting,
        transaction,
        vehicle,
        vehicleGroup,
        marketerMain,
        marketerCargoOwner,
        marketerCarrier,
        marketerDriver,
        marketerDashboard,
        forgetPassword,
        resetPassword,
        mapCargoAddress
    },
    state: {
        windowHeight: 0,
        windowWidth: 0
    },
    actions: {
        // Shared actions
    },

    mutations: {
        ['setWindowHeight'](state, {windowHeight}) {
            state.windowHeight = windowHeight;
        },
        ['setWindowWidth'](state, {windowWidth}) {
            state.windowWidth = windowWidth;
        }
    },
    getters: {
        // Shared getters
    }
})
