const state = {
    panelVisibleItems: [],
    isLoadingPanelVisibleItems: true
};

// getters
const getters = {};

// actions
const actions = {
    getCurrentOperator({commit}) {
        commit('setPanelVisibleItems');
    }
};

// mutations
const mutations = {
    ['setPanelVisibleItems'](state) {
        state.panelVisibleItems.push({title: 'داشبورد', icon: 'person', path: "dashboard"})
        state.panelVisibleItems.push({title: 'صاحبان بار', icon: 'person', path: "cargoOwner"})
        state.panelVisibleItems.push({title: 'رانندگان', icon: 'person', path: "driver"})
        state.panelVisibleItems.push({title: 'باربری ها', icon: 'person', path: "carrier"})
        state.panelVisibleItems.push({title: 'بازاریابان', icon: 'person', path: "marketer"})
        state.panelVisibleItems.push({title: 'پیام ها', icon: 'person', path: "message"})
        state.panelVisibleItems.push({title: 'انواع خودرو ها', icon: 'person', path: "vehicleGroup"})
        state.panelVisibleItems.push({title: 'خودروها و کامیون ها', icon: 'person', path: "vehicle"})
        state.panelVisibleItems.push({title: 'بارها', icon: 'person', path: "cargoType"})
        state.panelVisibleItems.push({title: 'تخفیف ها', icon: 'person', path: "discount"})
        state.panelVisibleItems.push({title: 'تنظیمات ', icon: 'person', path: "setting"})
        state.panelVisibleItems.push({title: 'درخواست ها', icon: 'person', path: "cargo"})
        state.panelVisibleItems.push({title: 'تراکنش ها و پرداخت ها', icon: 'person', path: "transaction"})
        state.panelVisibleItems.push({title: 'مدیریت نقش ها', icon: 'person', path: "role"})
        state.panelVisibleItems.push({title: 'اپراتورها', icon: 'person', path: "operator"})
        state.panelVisibleItems.push({title: 'مدیریت باکس های تبلیغاتی', icon: 'person', path: "advertise"})

        state.panelVisibleItems.push({title: 'خروج', icon: 'logout', path: null})

    }

};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
