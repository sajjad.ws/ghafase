import {readMessages,addOrEditMessage,deleteMessage,ChangeStatusMessage} from "../data/api";

const state = {
    allData: [],
    allDataLoading: false,
    pending: false,
    AddDialog: false,
    DeleteDialog:false,
    WaitDialog:false,
    selectedItem:{
        id:null,
        title:null,
        content:null,
        sent_at:null,
        status:null,
        for_carriers:null,
        for_drivers:null,
        for_cargo_owners:null,
        for_marketers:null,
        operator_id:null
    },
    filter: {
        start_time: null,
        end_time: null
    },
};

// getters
const getters = {};

// actions
const actions = {
    readData({commit}) {
        commit('setAllDataLoading', true);
        commit('setData', []);
        readMessages().then(jsonResponse => {
            commit('setData', jsonResponse.data.data);
            commit('setAllDataLoading', false);
        }).catch(e=>console.log(e));
    },
    addOrEdit({commit,dispatch}){
        commit('setPending',true);
        addOrEditMessage().then(jsonResponse => {
            commit('setPending',false);
            if (jsonResponse.data.status === 200){
                commit('hideAddDialog')
                dispatch('readData')
            }
        }).catch(e => {
            commit('setPending', false);
        });
    },
    deleteOne({commit,dispatch}){
        commit('setPending',true);
        deleteMessage().then(jsonResponse => {
            commit('setPending',false);
            commit('hideDeleteDialog')
            if (jsonResponse.data.status === 200){
                dispatch('readData')
            }
        });
    },
    activate({commit, dispatch}) {
        commit('showWaitDialog');
        ChangeStatusMessage().then(jsonResponse => {
            commit('hideWaitDialog');
            console.log(jsonResponse.data);
            if (jsonResponse.data.status === 200) {
                dispatch('readData')
            }
        });
    },
    removeFilter({commit, dispatch}){
        commit('removeFilter', true);
        dispatch('readData');
    }
};

// mutations
const mutations = {
    ['removeFilter'](state){
        state.filter = {
            start_time: null,
            end_time: null
        }
    },
    ['setStartTime'](state, value) {
        state.filter.start_time = value;
    },
    ['setEndTime'](state, value) {
        state.filter.end_time = value;
    },
    ['setData'](state, data) {
        state.allData = data;
    },
    ['showAddDialog'](state) {
        state.AddDialog = true;
    },
    ['hideAddDialog'](state) {
        state.AddDialog = false;
    },
    ['showDeleteDialog'](state) {
        state.DeleteDialog = true;
    },
    ['hideDeleteDialog'](state) {
        state.DeleteDialog = false;
    },
    ['setSelectedItem'](state, selectedItem) {
        state.selectedItem = selectedItem;
    },
    ['setPending'](state, value) {
        state.pending = value;
    },
    ['setPending'](state, value) {
        state.pending = value;
    },
    ['setContent'](state, value) {
        state.selectedItem.content = value;
    },
    ['setTitle'](state, value) {
        state.selectedItem.title = value;
    },
    ['setForMarketer'](state, value) {
        state.selectedItem.for_marketers = value;
    },
    ['setForCarrier'](state, value) {
        state.selectedItem.for_carriers = value;
    },
    ['setForDriver'](state, value) {
        state.selectedItem.for_drivers = value;
    },
    ['setForCargoOwner'](state, value) {
        state.selectedItem.for_cargo_owners = value;
    },
    ['setAllDataLoading'](state, value) {
        state.allDataLoading = value;
    },
    ['showWaitDialog'](state) {
        state.WaitDialog = true;
    },
    ['hideWaitDialog'](state) {
        state.WaitDialog = false;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
