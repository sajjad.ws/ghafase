import {
    readCargoOwners,
    addOrEditCargoOwner,
    deleteCargoOwner,
    ChangeStatusCargoOwner,
    getCityByProvince
} from "../data/api";

const state = {
    allData: [],
    province: [],
    city: [],
    cityPending: false,
    allDataLoading: false,
    pending: false,
    AddDialog: false,
    DeleteDialog: false,
    WaitDialog: false,
    selectCity: false,
    AlertDialog: false,
    alertDialogMessage: "",
    selectedProvince: {id: null, name: null, pid: null, created_at: null, updated_at: null},
    selectedCity: {id: null, name: null},
    selectedItem: {
        id: null,
        first_name: null,
        last_name: null,
        phone_number: null,
        status: null,
        credit: null,
        address: null,
        national_code: null,
        latitude: null,
        longitude: null,
        postal_code: null,
        marketer_id: null,
        city_id: null,
        city: {id: null, name: null}
    },
    filter: {
        search: null,
    },
};

// getters
const getters = {};

// actions
const actions = {
    readData({commit}) {
        commit('setAllDataLoading', true);
        commit('setData', []);
        readCargoOwners().then(jsonResponse => {
            console.log(jsonResponse.data.data);
            commit('setAllDataLoading', false);
            commit('setData', jsonResponse.data.data.cargoOwner);
            commit('setProvince', jsonResponse.data.data.province);
        }).catch(e => {

        });
    },
    addOrEdit({commit, dispatch}) {
        commit('setPending', true);
        addOrEditCargoOwner().then(jsonResponse => {
            commit('setPending', false);
            if (jsonResponse.data.status === 200) {
                commit('hideAddDialog')
                dispatch('readData')
            } else if (jsonResponse.data.status === 226) {
                commit('showAlertDialog', jsonResponse.data.data);
            }
        }).catch(e => {
            commit('setPending', false);
        });
    },
    deleteOne({commit, dispatch}) {
        commit('setPending', true);
        deleteCargoOwner().then(jsonResponse => {
            commit('setPending', false);
            commit('hideDeleteDialog')
            if (jsonResponse.data.status === 200) {
                dispatch('readData')
            }
        });
    },
    activate({commit, dispatch}) {
        commit('showWaitDialog');
        ChangeStatusCargoOwner().then(jsonResponse => {
            commit('hideWaitDialog');
            console.log(jsonResponse.data);
            if (jsonResponse.data.status === 200) {
                dispatch('readData')
            }
        });
    },
    getCityByProvince({commit, state}) {
        commit('showCityPending');
        console.log("state", state)
        getCityByProvince(state.selectedProvince.id).then(jsonResponse => {
            commit('hideCityPending');
            console.log(jsonResponse.data);
            if (jsonResponse.data.status === 200) {
                commit('setCity', jsonResponse.data.data);
            }
        });
    },
    removeFilter({commit, dispatch}) {
        commit('removeFilter', true);
        dispatch('readData');
    }
};

// mutations
const mutations = {
    ['removeFilter'](state) {
        state.filter = {
            search: null,
        }
    },
    ['setFilterSearch'](state, value) {
        state.filter.search = value;
    },
    ['showAlertDialog'](state, data) {
        state.AlertDialog = true;
        state.alertDialogMessage = data;
    },
    ['hideAlertDialog'](state) {
        state.AlertDialog = false;
    },
    ['setData'](state, data) {
        state.allData = data;
    },
    ['setProvince'](state, data) {
        state.province = data;
    },
    ['setCity'](state, data) {
        state.city = data;
    },
    ['setSelectedProvince'](state, data) {
        state.selectedProvince = data;
    },
    ['setSelectedCity'](state, data) {
        state.selectedCity = data;
    },
    ['setSelectedItemCity'](state) {
        state.selectedItem.city = state.selectedCity;
    },
    ['showAddDialog'](state) {
        state.AddDialog = true;
    },
    ['hideAddDialog'](state) {
        state.AddDialog = false;
    },
    ['showDeleteDialog'](state) {
        state.DeleteDialog = true;
    },
    ['hideDeleteDialog'](state) {
        state.DeleteDialog = false;
    },
    ['setSelectedItem'](state, selectedItem) {
        console.log('selectedItem',selectedItem);
        state.selectedItem = selectedItem;
    },
    ['setPending'](state, value) {
        state.pending = value;
    },
    ['setFirstName'](state, value) {
        state.selectedItem.first_name = value;
    },
    ['setLastName'](state, value) {
        state.selectedItem.last_name = value;
    },
    ['setPostalCode'](state, value) {
        state.selectedItem.postal_code = value;
    },
    ['setNationalCode'](state, value) {
        state.selectedItem.national_code = value;
    },
    ['setCompanyName'](state, value) {
        state.selectedItem.company_name = value;
    },
    ['setTelephoneNumber'](state, value) {
        state.selectedItem.telephone_number = value;
    },
    ['setAddress'](state, value) {
        state.selectedItem.address = value;
    },
    ['setPhoneNumber'](state, value) {
        state.selectedItem.phone_number = value;
    },
    ['setEmail'](state, value) {
        state.selectedItem.email = value;
    },
    ['setAllDataLoading'](state, value) {
        state.allDataLoading = value;
    },
    ['showWaitDialog'](state) {
        state.WaitDialog = true;
    },
    ['hideWaitDialog'](state) {
        state.WaitDialog = false;
    },
    ['showSelectCity'](state) {
        state.selectCity = true;
    },
    ['hideSelectCity'](state) {
        state.selectCity = false;
    },
    ['showCityPending'](state) {
        state.cityPending = true;
    },
    ['hideCityPending'](state) {
        state.cityPending = false;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
