import {readAdvertises,addOrEditAdvertise,deleteAdvertise} from "../data/api";

const state = {
    allData: [],
    allDataLoading: false,
    pending: false,
    AddDialog: false,
    DeleteDialog:false,
    photoDialog: false,
    selectedItem:{
        id:null,
        title:null,
        size:null,
        media_url:null,
        image:null,
    }
};

// getters
const getters = {};

// actions
const actions = {
    readData({commit}) {
        commit('setAllDataLoading', true);
        readAdvertises().then(jsonResponse => {
            commit('setData', jsonResponse.data.data);
            commit('setAllDataLoading', false);
        }).catch(e=>console.log(e));
    },
    addOrEdit({commit,dispatch}){
        commit('setPending',true);
        addOrEditAdvertise().then(jsonResponse => {
            commit('setPending',false);
            if (jsonResponse.data.status === 200){
                commit('hideAddDialog')
                dispatch('readData')
            }
        }).catch(e => {
            commit('setPending', false);
        });
    },
    deleteOne({commit,dispatch}){
        commit('setPending',true);
        deleteAdvertise().then(jsonResponse => {
            commit('setPending',false);
            commit('hideDeleteDialog')
            if (jsonResponse.data.status === 200){
                dispatch('readData')
            }
        });
    },
};

// mutations
const mutations = {
    ['setData'](state, data) {
        state.allData = data;
    },
    ['showAddDialog'](state) {
        state.AddDialog = true;
    },
    ['hideAddDialog'](state) {
        state.AddDialog = false;
    },
    ['showDeleteDialog'](state) {
        state.DeleteDialog = true;
    },
    ['hideDeleteDialog'](state) {
        state.DeleteDialog = false;
    },
    ['setSelectedItem'](state, selectedItem) {
        state.selectedItem = selectedItem;
    },
    ['setPending'](state, value) {
        state.pending = value;
    },
    ['setTitle'](state, value) {
        state.selectedItem.title = value;
    },
    ['setTitle'](state, value) {
        state.selectedItem.title = value;
    },
    ['setSize'](state, value) {
        state.selectedItem.size = value;
    },
    ['setImage'](state, value) {
        state.selectedItem.image = value;
    },
    ['setAllDataLoading'](state, value) {
        state.allDataLoading = value;
    },
    ['showPhotoDialog'](state) {
        state.photoDialog = true;
    },
    ['hidePhotoDialog'](state) {
        state.photoDialog = false;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
