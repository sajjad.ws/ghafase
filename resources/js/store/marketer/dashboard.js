import {readDashboardMarketer} from "../../data/api";

const state = {
    dataLoading: false,
    selectedItem: {
        all: 0,
        today: 0,
        week: 0,
        month: 0,
    },
};

// getters
const getters = {};

// actions
const actions = {
    readData({commit,state}) {
        commit('setDataLoading', true);
        readDashboardMarketer().then(jsonResponse => {
            commit('setSelectedItem', jsonResponse.data.data);
            commit('setDataLoading', false);
        });
    },
};

// mutations
const mutations = {
    ['setSelectedItem'](state, data) {
        state.selectedItem = data;
    },
    ['setDataLoading'](state, value) {
        state.dataLoading = value;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
