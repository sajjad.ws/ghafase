import {readCarriersMarketer, addOrEditCarrier, deleteCarrier, ChangeStatusCarrier, getCityByProvince} from "../../data/api";

const state = {
    allData: [],
    province: [],
    city: [],
    cityPending: false,
    allDataLoading: false,
    pending: false,
    AddDialog: false,
    DeleteDialog: false,
    WaitDialog: false,
    selectCity: false,
    selectedProvince: {id: null, name: null, pid: null, created_at: null, updated_at: null},
    selectedCity: {id: null, name: null},
    selectedItem: {
        id: null,
        freight_name: null,
        owner_first_name: null,
        owner_last_name: null,
        username: null,
        new_password: null,
        owner_phone_number: null,
        telephone_number: null,
        status: null,
        credit: null,
        point: null,
        marketer_id: null,
        city_id: null,
        city: {id: null, name: null}
    },
    filter: {
        search: null,
    },
};

// getters
const getters = {};

// actions
const actions = {
    readData({commit}) {
        commit('setAllDataLoading', true);
        commit('setData', []);
        readCarriersMarketer().then(jsonResponse => {
            console.log(jsonResponse.data.data);
            commit('setData', jsonResponse.data.data.carrier);
            commit('setProvince', jsonResponse.data.data.province);
            commit('setAllDataLoading', false);
        }).catch(e => console.log(e));
    },
    addOrEdit({commit, dispatch}) {
        commit('setPending', true);
        addOrEditCarrier().then(jsonResponse => {
            commit('setPending', false);
            if (jsonResponse.data.status === 200) {
                commit('hideAddDialog')
                dispatch('readData')
            }
        }).catch(e => {
            commit('setPending', false);
        });
    },
    deleteOne({commit, dispatch}) {
        commit('setPending', true);
        deleteCarrier().then(jsonResponse => {
            commit('setPending', false);
            commit('hideDeleteDialog')
            if (jsonResponse.data.status === 200) {
                dispatch('readData')
            }
        });
    },
    activate({commit, dispatch}) {
        commit('showWaitDialog');
        ChangeStatusCarrier().then(jsonResponse => {
            commit('hideWaitDialog');
            console.log(jsonResponse.data);
            if (jsonResponse.data.status === 200) {
                dispatch('readData')
            }
        });
    },
    getCityByProvince({commit , state}) {
        commit('showCityPending');
        console.log("state",state)
        getCityByProvince(state.selectedProvince.id).then(jsonResponse => {
            commit('hideCityPending');
            console.log(jsonResponse.data);
            if (jsonResponse.data.status === 200) {
                commit('setCity', jsonResponse.data.data);
            }
        });
    },
    removeFilter({commit, dispatch}) {
        commit('removeFilter', true);
        dispatch('readData');
    }
};

// mutations
const mutations = {
    ['removeFilter'](state) {
        state.filter = {
            search: null,
        }
    },
    ['setFilterSearch'](state, value) {
        state.filter.search = value;
    },
    ['setData'](state, data) {
        state.allData = data;
    },
    ['setProvince'](state, data) {
        state.province = data;
    },
    ['setCity'](state, data) {
        state.city = data;
    },
    ['setSelectedProvince'](state, data) {
        state.selectedProvince = data;
    },
    ['setSelectedCity'](state, data) {
        state.selectedCity = data;
    },
    ['setSelectedItemCity'](state) {
        state.selectedItem.city = state.selectedCity;
    },
    ['showAddDialog'](state) {
        state.AddDialog = true;
    },
    ['hideAddDialog'](state) {
        state.AddDialog = false;
    },
    ['showDeleteDialog'](state) {
        state.DeleteDialog = true;
    },
    ['hideDeleteDialog'](state) {
        state.DeleteDialog = false;
    },
    ['setSelectedItem'](state, selectedItem) {
        state.selectedItem = selectedItem;
    },
    ['setPending'](state, value) {
        state.pending = value;
    },
    ['setFreightName'](state, value) {
        state.selectedItem.freight_name = value;
    },
    ['setFirstName'](state, value) {
        state.selectedItem.owner_first_name = value;
    },
    ['setLastName'](state, value) {
        state.selectedItem.owner_last_name = value;
    },
    ['setUserName'](state, value) {
        state.selectedItem.username = value;
    },
    ['setPassword'](state, value) {
        state.selectedItem.new_password = value;
    },
    ['setPhoneNumber'](state, value) {
        state.selectedItem.phone_number = value;
    },
    ['setTelephoneNumber'](state, value) {
        state.selectedItem.te = value;
    },
    ['setAllDataLoading'](state, value) {
        state.allDataLoading = value;
    },
    ['showWaitDialog'](state) {
        state.WaitDialog = true;
    },
    ['hideWaitDialog'](state) {
        state.WaitDialog = false;
    },
    ['showSelectCity'](state) {
        state.selectCity = true;
    },
    ['hideSelectCity'](state) {
        state.selectCity = false;
    },
    ['showCityPending'](state) {
        state.cityPending = true;
    },
    ['hideCityPending'](state) {
        state.cityPending = false;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
