import {getMarketerID} from "../../data/localStorage"

const state = {
    panelVisibleItems: [],
    isLoadingPanelVisibleItems: true,
    selectedMarketer: {
        id: null,
    },
};

// getters
const getters = {};

// actions
const actions = {
    getCurrentOperator({commit,state}) {
        commit('setPanelVisibleItems');
        commit('setSelectedMarketer',getMarketerID());
        console.log(state.selectedMarketer);
    }
};

// mutations
const mutations = {
    ['setPanelVisibleItems'](state) {
        // state.panelVisibleItems.push({title: 'داشبورد', icon: 'person', path: "marketerMain/marketerDashboard"})
        state.panelVisibleItems.push({title: 'صاحبان بار', icon: 'person', path: "marketerMain/marketerCargoOwner"})
        state.panelVisibleItems.push({title: 'رانندگان', icon: 'person', path: "marketerMain/marketerDriver"})
        state.panelVisibleItems.push({title: 'باربری ها', icon: 'person', path: "marketerMain/marketerCarrier"})
    },
    ['setSelectedMarketer'](state, data) {
        console.log(data);
        state.selectedMarketer = {
            id:data
        };
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
