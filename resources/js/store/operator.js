import {readOperators,addOrEditOperator,deleteOperator} from "../data/api";

const state = {
    allData: [],
    AllRole:[],
    allDataLoading: false,
    pending: false,
    AddDialog: false,
    DeleteDialog:false,
    selectedItem:{
        id:null,
        first_name:null,
        last_name:null,
        email:null,
        password:null,
        status:null,
        role:{id:null,name:null}
    }
};

// getters
const getters = {};

// actions
const actions = {
    readData({commit}) {
        commit('setAllDataLoading', true);
        readOperators().then(jsonResponse => {
            console.log(jsonResponse.data.data);
            commit('setRoles', jsonResponse.data.data.role);
            commit('setData', jsonResponse.data.data.operator);
            commit('setAllDataLoading', false);
        }).catch(e=>console.log(e));
    },
    addOrEdit({commit,dispatch}){
        commit('setPending',true);
        addOrEditOperator().then(jsonResponse => {
            commit('setPending',false);
            if (jsonResponse.data.status === 200){
                commit('hideAddDialog')
                dispatch('readData')
            }
        }).catch(e => {
            commit('setPending', false);
        });
    },
    deleteOne({commit,dispatch}){
        commit('setPending',true);
        deleteOperator().then(jsonResponse => {
            commit('setPending',false);
            commit('hideDeleteDialog')
            if (jsonResponse.data.status === 200){
                dispatch('readData')
            }
        });
    },
};

// mutations
const mutations = {
    ['setData'](state, data) {
        state.allData = data;
    },
    ['setRoles'](state, data) {
        state.AllRole = data;
    },
    ['showAddDialog'](state) {
        state.AddDialog = true;
    },
    ['hideAddDialog'](state) {
        state.AddDialog = false;
    },
    ['showDeleteDialog'](state) {
        state.DeleteDialog = true;
    },
    ['hideDeleteDialog'](state) {
        state.DeleteDialog = false;
    },
    ['setSelectedItem'](state, selectedItem) {
        state.selectedItem = selectedItem;
    },
    ['setPending'](state, value) {
        state.pending = value;
    },
    ['setTitle'](state, value) {
        state.selectedItem.title = value;
    },
    ['setFirstName'](state, value) {
        state.selectedItem.first_name = value;
    },
    ['setLastName'](state, value) {
        state.selectedItem.last_name = value;
    },
    ['setEmail'](state, value) {
        state.selectedItem.email = value;
    },
    ['setPassword'](state, value) {
        state.selectedItem.password = value;
    },
    ['setRole'](state, value) {
        state.selectedItem.role = value;
    },
    ['setAllDataLoading'](state, value) {
        state.allDataLoading = value;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
