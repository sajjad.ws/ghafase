import {readMarketers, addOrEditMarketer, deleteMarketer, ChangeStatusMarketer} from "../data/api";

const state = {
    allData: [],
    allDataLoading: false,
    pending: false,
    AddDialog: false,
    DeleteDialog: false,
    WaitDialog: false,
    selectedItem: {
        id: null,
        first_name: null,
        last_name: null,
        username: null,
        new_password: null,
        status: null,
        phone_number: null
    },
    filter: {
        search: null,
    },
};

// getters
const getters = {};

// actions
const actions = {
    readData({commit}) {
        commit('setAllDataLoading', true);
        commit('setData', []);
        readMarketers().then(jsonResponse => {
            console.log(jsonResponse.data.data);
            commit('setData', jsonResponse.data.data);
            commit('setAllDataLoading', false);
        }).catch(e => console.log(e));
    },
    addOrEdit({commit, dispatch}) {
        commit('setPending', true);
        addOrEditMarketer().then(jsonResponse => {
            commit('setPending', false);
            if (jsonResponse.data.status === 200) {
                commit('hideAddDialog')
                dispatch('readData')
            }
        }).catch(e => {
            commit('setPending', false);
        });
    },
    deleteOne({commit, dispatch}) {
        commit('setPending', true);
        deleteMarketer().then(jsonResponse => {
            commit('setPending', false);
            commit('hideDeleteDialog')
            if (jsonResponse.data.status === 200) {
                dispatch('readData')
            }
        });
    },

    activate({commit, dispatch}) {
        commit('showWaitDialog');
        ChangeStatusMarketer().then(jsonResponse => {
            commit('hideWaitDialog');
            console.log(jsonResponse.data);
            if (jsonResponse.data.status === 200) {
                dispatch('readData')
            }
        });
    },
    removeFilter({commit, dispatch}) {
        commit('removeFilter', true);
        dispatch('readData');
    }
};

// mutations
const mutations = {
    ['removeFilter'](state) {
        state.filter = {
            search: null,
        }
    },
    ['setFilterSearch'](state, value) {
        state.filter.search = value;
    },
    ['setData'](state, data) {
        state.allData = data;
    },
    ['setRoles'](state, data) {
        state.AllRole = data;
    },
    ['showAddDialog'](state) {
        state.AddDialog = true;
    },
    ['hideAddDialog'](state) {
        state.AddDialog = false;
    },
    ['showDeleteDialog'](state) {
        state.DeleteDialog = true;
    },
    ['hideDeleteDialog'](state) {
        state.DeleteDialog = false;
    },
    ['setSelectedItem'](state, selectedItem) {
        state.selectedItem = selectedItem;
    },
    ['setPending'](state, value) {
        state.pending = value;
    },
    ['setTitle'](state, value) {
        state.selectedItem.title = value;
    },
    ['setFirstName'](state, value) {
        state.selectedItem.first_name = value;
    },
    ['setLastName'](state, value) {
        state.selectedItem.last_name = value;
    },
    ['setUserName'](state, value) {
        state.selectedItem.username = value;
    },
    ['setPassword'](state, value) {
        state.selectedItem.new_password = value;
    },
    ['setPhoneNumber'](state, value) {
        state.selectedItem.phone_number = value;
    },
    ['setAllDataLoading'](state, value) {
        state.allDataLoading = value;
    },
    ['showWaitDialog'](state) {
        state.WaitDialog = true;
    },
    ['hideWaitDialog'](state) {
        state.WaitDialog = false;
    },
    ['setNationalCode'](state, value) {
        state.selectedItem.national_code = value;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
