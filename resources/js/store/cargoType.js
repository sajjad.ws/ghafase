import {readCargoTypes,addOrEditCargoType,deleteCargoType} from "../data/api";

const state = {
    allData: [],
    allDataLoading: false,
    pending: false,
    AddDialog: false,
    DeleteDialog:false,
    AlertDialog: false,
    alertDialogMessage: "",
    selectedItem:{
        id:null,
        name:null,
        commission_amount:null,
    }
};

// getters
const getters = {};

// actions
const actions = {
    readData({commit}) {
        commit('setAllDataLoading', true);
        readCargoTypes().then(jsonResponse => {
            commit('setData', jsonResponse.data.data);
            commit('setAllDataLoading', false);
        }).catch(e=>console.log(e));
    },
    addOrEdit({commit,dispatch}){
        commit('setPending',true);
        addOrEditCargoType().then(jsonResponse => {
            commit('setPending',false);
            if (jsonResponse.data.status === 200){
                commit('hideAddDialog')
                dispatch('readData')
            }
        }).catch(e => {
            commit('setPending', false);
        });
    },
    deleteOne({commit,dispatch}){
        commit('setPending',true);
        deleteCargoType().then(jsonResponse => {
            commit('setPending',false);
            commit('hideDeleteDialog')
            if (jsonResponse.data.status === 200){
                dispatch('readData')
            } else if (jsonResponse.data.status === 226){
                commit('showAlertDialog' ,jsonResponse.data.data);
            }
        });
    },
};

// mutations
const mutations = {
    ['setData'](state, data) {
        state.allData = data;
    },
    ['showAddDialog'](state) {
        state.AddDialog = true;
    },
    ['hideAddDialog'](state) {
        state.AddDialog = false;
    },
    ['showDeleteDialog'](state) {
        state.DeleteDialog = true;
    },
    ['hideDeleteDialog'](state) {
        state.DeleteDialog = false;
    },
    ['setSelectedItem'](state, selectedItem) {
        state.selectedItem = selectedItem;
    },
    ['setPending'](state, value) {
        state.pending = value;
    },
    ['setTitle'](state, value) {
        state.selectedItem.title = value;
    },
    ['setName'](state, value) {
        state.selectedItem.name = value;
    },
    ['setCommissionAmount'](state, value) {
        state.selectedItem.commission_amount = value;
    },
    ['setAllDataLoading'](state, value) {
        state.allDataLoading = value;
    },
    ['showAlertDialog'](state , data) {
        state.AlertDialog = true;
        state.alertDialogMessage = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
