import {fetchLogin} from "../data/api";
import {getToken, removeToken, saveToken} from "../data/localStorage";
import router from '../router';

const state = {
    isLoggedIn: !!getToken(),
    pending: false,
    showPassword: false,
    errorMessage: false,
    password: '',
    username: '',
    mode: 'login'
};

// getters
const getters = {
    isLoggedIn: state => {
        return state.isLoggedIn
    }
};

// actions
const actions = {
    login({commit}) {
        commit('setErrorMsg', false);
        commit('login');
        fetchLogin().then(jsonResponse => {
            saveToken(jsonResponse.headers.authorization);
            commit('loginSuccess');
            router.push('/')
        }).catch(e => {
            commit('setErrorMsg', true);
        });
        removeToken();
    },
    logout({commit}) {
        commit('logout');
        removeToken();
        router.push('/login')
    },
    forgetPassword() {
        router.push('/forgetPassword')
    }
};

// mutations
const mutations = {
    ['login'](state) {
        state.pending = true;
    },
    ['loginSuccess'](state) {
        state.isLoggedIn = true;
        state.pending = false;
    },
    ['setErrorMsg'](state, val) {
        state.errorMessage = val;
        state.pending = false;
    },
    ['logout'](state) {
        state.isLoggedIn = false;
    },
    ['setUsername'](state, username) {
        state.username = username;
    },
    ['setPassword'](state, password) {
        state.password = password;
    },
    ['togglePassword'](state) {
        state.showPassword = !state.showPassword;
    },
    ['loginMode'](state) {
        state.mode = 'login';
    },
    ['signUpMode'](state) {
        state.mode = 'signUp';
    },
    ['toggleMode'](state) {
        state.mode = state.mode === 'signUp' ? 'login' : 'signUp';
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
