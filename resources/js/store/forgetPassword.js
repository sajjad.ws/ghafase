import {sendMail} from "../data/api";
import {getToken} from "../data/localStorage";
import router from '../router';

const state = {
    pending: false,
    email: '',
};

// getters
const getters = {

};

// actions
const actions = {
    sendMail({commit}) {
        commit('setPending',true);
        sendMail().then(jsonResponse => {
            commit('setPending',false);
            alert('ایمیل ارسال شد')
        });
    },
};

// mutations
const mutations = {
    ['setPending'](state,val) {
        state.pending = val;
    },
    ['loginSuccess'](state) {
        state.isLoggedIn = true;
        state.pending = false;
    },
    ['setEmail'](state, email) {
        state.email = email;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
