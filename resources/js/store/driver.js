import {readDrivers, addOrEditDriver, deleteDriver, ChangeStatusDriver, getCityByProvince, deleteDriverPhoto} from "../data/api";

const state = {
    allData: [],
    province: [],
    city: [],
    vehicle: [],
    cityPending: false,
    allDataLoading: false,
    pending: false,
    AddDialog: false,
    DeleteDialog: false,
    WaitDialog: false,
    selectCity: false,
    photoDialog: false,
    imageData: null,
    selectedProvince: {id: null, name: null, pid: null, created_at: null, updated_at: null},
    selectedCity: {id: null, name: null},
    selectedVehicle: {id: null, name: null},
    selectedItem: {
        id: null,
        first_name: null,
        last_name: null,
        phone_number: null,
        status: null,
        credit: null,
        point: null,
        plate1: null,
        plate2: null,
        plate3: null,
        plate4: null,
        international_plate1: null,
        international_plate2: null,
        international_plate3: null,
        international_plate4: null,
        photo_url:null,
        is_aboard_eager: null,
        marketer_id: null,
        city_id: null,
        vehicle_id: null,
        vehicle : {id:null,name:null,max_capacity:null,vehicle_group_id:null},
        city: {id: null, name: null}
    },
    filter: {
        search: null,
    },
};

// getters
const getters = {};

// actions
const actions = {
    readData({commit}) {
        commit('setAllDataLoading', true);
        commit('setData', []);
        readDrivers().then(jsonResponse => {
            console.log(jsonResponse.data.data);
            commit('setData', jsonResponse.data.data.driver);
            commit('setProvince', jsonResponse.data.data.province);
            commit('setVehicles', jsonResponse.data.data.vehicle);
            commit('setAllDataLoading', false);
        }).catch(e => console.log(e));
    },
    addOrEdit({commit, dispatch}) {
        commit('setPending', true);
        addOrEditDriver().then(jsonResponse => {
            commit('setPending', false);
            if (jsonResponse.data.status === 200) {
                commit('hideAddDialog')
                dispatch('readData')
            }
        }).catch(e => {
            commit('setPending', false);
        });
    },
    removePhoto({commit, dispatch}) {
        commit('showWaitDialog');
        deleteDriverPhoto(state.selectedItem.id).then(jsonResponse => {
            commit('hideWaitDialog')
            commit('setSelectedItemNull');
            if (jsonResponse.data.status === 200) {
                dispatch('readData')
            }
        }).catch(e => {
            commit('hideWaitDialog')
            commit('setSelectedItemNull');
        });
    },
    deleteOne({commit, dispatch}) {
        commit('setPending', true);
        deleteDriver().then(jsonResponse => {
            commit('setPending', false);
            commit('hideDeleteDialog')
            if (jsonResponse.data.status === 200) {
                dispatch('readData')
            }
        });
    },
    activate({commit, dispatch}) {
        commit('showWaitDialog');
        ChangeStatusDriver().then(jsonResponse => {
            commit('hideWaitDialog');
            console.log(jsonResponse.data);
            if (jsonResponse.data.status === 200) {
                dispatch('readData')
            }
        });
    },
    getCityByProvince({commit , state}) {
        commit('showCityPending');
        console.log("state",state)
        getCityByProvince(state.selectedProvince.id).then(jsonResponse => {
            commit('hideCityPending');
            console.log(jsonResponse.data);
            if (jsonResponse.data.status === 200) {
                commit('setCity', jsonResponse.data.data);
            }
        });
    },
    removeFilter({commit, dispatch}) {
        commit('removeFilter', true);
        dispatch('readData');
    },

};

// mutations
const mutations = {
    ['setSelectedItemNull'](state){
        state.selectedItem = {
            id: null,
                first_name: null,
                last_name: null,
                phone_number: null,
                status: null,
                credit: null,
                point: null,
                plate1: null,
                plate2: null,
                plate3: null,
                plate4: null,
                international_plate1: null,
                international_plate2: null,
                international_plate3: null,
                international_plate4: null,
                photo_url:null,
                is_aboard_eager: null,
                marketer_id: null,
                city_id: null,
                vehicle_id: null,
                vehicle : {id:null,name:null,max_capacity:null,vehicle_group_id:null},
            city: {id: null, name: null}
        }
    },
    ['removeFilter'](state) {
        state.filter = {
            search: null,
        }
    },
    ['setFilterSearch'](state, value) {
        state.filter.search = value;
    },
    ['setData'](state, data) {
        state.allData = data;
    },
    ['setProvince'](state, data) {
        state.province = data;
    },
    ['setCity'](state, data) {
        state.city = data;
    },
    ['setVehicles'](state, data) {
        state.vehicle = data;
    },
    ['setSelectedProvince'](state, data) {
        state.selectedProvince = data;
    },
    ['setSelectedCity'](state, data) {
        state.selectedCity = data;
    },
    ['setSelectedItemCity'](state) {
        state.selectedItem.city = state.selectedCity;
    },
    ['setVehicle'](state, data) {
        state.selectedItem.vehicle = data;
    },
    ['showAddDialog'](state) {
        state.AddDialog = true;
    },
    ['hideAddDialog'](state) {
        state.AddDialog = false;
    },
    ['showDeleteDialog'](state) {
        state.DeleteDialog = true;
    },
    ['hideDeleteDialog'](state) {
        state.DeleteDialog = false;
    },
    ['setSelectedItem'](state, selectedItem) {
        state.selectedItem = selectedItem;
    },
    ['setPending'](state, value) {
        state.pending = value;
    },
    ['setFirstName'](state, value) {
        state.selectedItem.first_name = value;
    },
    ['setLastName'](state, value) {
        state.selectedItem.last_name = value;
    },
    ['setPostalCode'](state, value) {
        state.selectedItem.postal_code = value;
    },
    ['setNationalCode'](state, value) {
        state.selectedItem.national_code = value;
    },
    ['setAddress'](state, value) {
        state.selectedItem.address = value;
    },
    ['setPhoneNumber'](state, value) {
        state.selectedItem.phone_number = value;
    },
    ['setEmail'](state, value) {
        state.selectedItem.email = value;
    },
    ['setPlate1'](state, value) {
        state.selectedItem.plate1 = value;
    },
    ['setPlate2'](state, value) {
        state.selectedItem.plate2 = value;
    },
    ['setPlate3'](state, value) {
        state.selectedItem.plate3 = value;
    },
    ['setPlate4'](state, value) {
        state.selectedItem.plate4 = value;
    },
    ['setInternationalPlate1'](state, value) {
        state.selectedItem.international_plate1 = value;
    },
    ['setInternationalPlate2'](state, value) {
        state.selectedItem.international_plate2 = value;
    },
    ['setInternationalPlate3'](state, value) {
        state.selectedItem.international_plate3 = value;
    },
    ['setInternationalPlate4'](state, value) {
        state.selectedItem.international_plate4 = value;
    },
    ['setAllDataLoading'](state, value) {
        state.allDataLoading = value;
    },
    ['showWaitDialog'](state) {
        state.WaitDialog = true;
    },
    ['hideWaitDialog'](state) {
        state.WaitDialog = false;
    },
    ['showSelectCity'](state) {
        state.selectCity = true;
    },
    ['hideSelectCity'](state) {
        state.selectCity = false;
    },
    ['showCityPending'](state) {
        state.cityPending = true;
    },
    ['hideCityPending'](state) {
        state.cityPending = false;
    },
    ['showPhotoDialog'](state) {
        state.photoDialog = true;
    },
    ['hidePhotoDialog'](state) {
        state.photoDialog = false;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
