import {readTransactions, searchOwners, searchDrivers, searchCarriers, getCityByProvince} from "../data/api";

const state = {
    allData: [],
    province: [],
    city: [],
    searchedOwners: [],
    searchedDrivers: [],
    searchedCarriers: [],
    types:['پرداخت آنلاین','شارژ کیف پول','پرداخت از کیف پول'],
    allDataLoading: false,
    pending: false,
    AddDialog: false,
    DeleteDialog:false,
    isLoadingCarrier:false,
    isLoadingDriver:false,
    origin:false,
    destination:false,
    cityPending: false,
    selectedProvince: {id: null, name: null, pid: null, created_at: null, updated_at: null},
    selectedItem:{
        id:null,
        user_type:null,
        user_id:null,
        type:null,
        amount:null,
        discount_percent:null,
        summation:null,
        value:null,
        ref_id:null,
    },
    filter: {
        status: null,
        driver: null,
        carrier: null,
        owner: null,
        carrierPhoneNumber: null,
        driverPhoneNumber: null,
        ownerPhoneNumber: null,
        phone_number: null,
        start_time: null,
        end_time: null,
        originCity:{id: null, name: null},
        destinationCity:{id: null, name: null},
    },
};

// getters
const getters = {};

// actions
const actions = {
    readData({commit}) {
        commit('setAllDataLoading', true);
        commit('setData', []);
        readTransactions().then(jsonResponse => {
            console.log(jsonResponse.data.data);
            commit('setData', jsonResponse.data.data.credit_transaction);
            commit('setProvince', jsonResponse.data.data.province);
            commit('setAllDataLoading', false);
        }).catch(e=>console.log(e));
    },
    searchOwner({commit}, val) {
        console.log(val);
        if (val === null) {
            return;
        }
        if (val.length === 0) {
            commit('setSearchedOwners', []);
        }
        if (val.length < 3) {
            return;
        }
        commit('setIsLoadingOwner', true);
        searchOwners(val).then(jsonResponse => {
            commit('setIsLoadingOwner', false);
            commit('setSearchedOwners', jsonResponse.data.data)
        });
    },
    searchDriver({commit}, val) {
        if (val === null) {
            return;
        }
        if (val.length === 0) {
            commit('setSearchedDrivers', []);
        }
        if (val.length < 3) {
            return;
        }
        commit('setIsLoadingDriver', true);
        searchDrivers(val).then(jsonResponse => {
            commit('setIsLoadingDriver', false);
            commit('setSearchedDrivers', jsonResponse.data.data)
        });
    },
    searchCarrier({commit}, val) {
        if (val === null) {
            return;
        }
        if (val.length === 0) {
            commit('setSearchedCarriers', []);
        }
        if (val.length < 3) {
            return;
        }
        commit('setIsLoadingCarrier', true);
        searchCarriers(val).then(jsonResponse => {
            commit('setIsLoadingCarrier', false);
            commit('setSearchedCarriers', jsonResponse.data.data)
        });
    },
    removeFilter({commit, dispatch}){
        commit('removeFilter', true);
        dispatch('readData');
    },
    getCityByProvince({commit , state}) {
        commit('showCityPending');
        console.log("state",state);
        getCityByProvince(state.selectedProvince.id).then(jsonResponse => {
            commit('hideCityPending');
            console.log(jsonResponse.data);
            if (jsonResponse.data.status === 200) {
                commit('setCity', jsonResponse.data.data);
            }
        });
    },
    setOrigin({commit,dispatch},data){
        console.log('action',data);
        commit('setOrigin', data);
        commit('hideOrigin');
    },
    setDestination({commit,dispatch},data){
        console.log('action',data);
        commit('setDestination', data);
        commit('hideDestination');
    }
};

// mutations
const mutations = {
    ['setData'](state, data) {
        state.allData = data;
    },
    ['removeFilter'](state){
        state.filter = {
            carrier: null,
            driver: null,
            owner: null,
            receiverPhoneNumber: null,
            ownerPhoneNumber: null,
            phone_number: null,
            start_time: null,
            end_time: null,
            originCity:{id: null, name: null},
            destinationCity:{id: null, name: null},
        }
    },
    ['setAllDataLoading'](state, value) {
        state.allDataLoading = value;
    },
    ['setProvince'](state, data) {
        state.province = data;
    },
    ['setOrigin'](state, data) {
        state.filter.originCity = data;
    },
    ['setDestination'](state, data) {
        state.filter.destinationCity = data;
    },
    ['setSelectedOwner'](state, data) {
        state.filter.driver = null;
        state.filter.carrier = null;
        state.filter.owner = data;
    },
    ['setSelectedDriver'](state, data) {
        state.filter.owner = null;
        state.filter.carrier = null;
        state.filter.driver = data;
    },
    ['setSelectedCarrier'](state, data) {
        state.filter.driver = null;
        state.filter.owner = null;
        state.filter.carrier = data;
    },
    ['setStartTime'](state, value) {
        state.filter.start_time = value;
    },
    ['setEndTime'](state, value) {
        state.filter.end_time = value;
    },
    ['setSearchedOwners'](state, value) {
        state.searchedOwners = value;
    },
    ['setSearchedDrivers'](state, value) {
        state.searchedDrivers = value;
    },
    ['setSearchedCarriers'](state, value) {
        state.searchedCarriers = value;
    },
    ['setIsLoadingOwner'](state, value) {
        state.isLoadingOwner = value;
    },
    ['setSelectedProvince'](state, data) {
        state.selectedProvince = data;
    },
    ['setIsLoadingCarrier'](state, value) {
        state.isLoadingCarrier = value;
    },
    ['setIsLoadingDriver'](state, value) {
        state.isLoadingDriver = value;
    },
    ['setCity'](state, data) {
        state.city = data;
    },
    ['showCityPending'](state) {
        state.cityPending = true;
    },
    ['hideCityPending'](state) {
        state.cityPending = false;
    },
    ['showOrigin'](state) {
        state.origin = true;
    },
    ['hideOrigin'](state) {
        state.origin = false;
    },
    ['showDestination'](state) {
        state.destination = true;
    },
    ['hideDestination'](state) {
        state.destination = false;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
