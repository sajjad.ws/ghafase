import {readRoles, addOrEditRole, deleteRole} from "../data/api";

const state = {
        allData: [],
        allDataLoading: false,
        pending: false,
        AddDialog: false,
        DeleteDialog: false,
        selectedItem: {
            id: null,
            name: null,
            cargo_owners_permission: 0,
            drivers_permission: 0,
            carriers_permission: 0,
            marketers_permission: 0,
            messages_permission: 0,
            vehicle_permission: 0,
            cargo_type_permission: 0,
            discount_permission: 0,
            setting_permission: 0,
            order_report_permission: 0,
            transaction_report_permission: 0,
            dashboard_permission: 0,
            advertising_box_permission: 0,
            static_content_permission: 0,
            role_permission: 0,
            operator_permission: 0,
            regulation_permission: 0,
        }
    }
;

// getters
const getters = {};

// actions
const actions = {
    readData({commit}) {
        commit('setAllDataLoading', true);
        readRoles().then(jsonResponse => {
            commit('setData', jsonResponse.data.data);
            commit('setAllDataLoading', false);
        }).catch(e => console.log(e));
    },
    addOrEdit({commit, dispatch}) {
        commit('setPending', true);
        addOrEditRole().then(jsonResponse => {
            commit('setPending', false);
            if (jsonResponse.data.status === 200) {
                commit('hideAddDialog')
                dispatch('readData')
            }
        }).catch(e => {
            commit('setPending', false);
        });
    },
    deleteOne({commit, dispatch}) {
        commit('setPending', true);
        deleteRole().then(jsonResponse => {
            commit('setPending', false);
            commit('hideDeleteDialog')
            if (jsonResponse.data.status === 200) {
                dispatch('readData')
            }
        });
    },
};

// mutations
const mutations = {
    ['setData'](state, data) {
        state.allData = data;
    },
    ['showAddDialog'](state) {
        state.AddDialog = true;
    },
    ['hideAddDialog'](state) {
        state.AddDialog = false;
    },
    ['showDeleteDialog'](state) {
        state.DeleteDialog = true;
    },
    ['hideDeleteDialog'](state) {
        state.DeleteDialog = false;
    },
    ['setSelectedItem'](state, selectedItem) {
        state.selectedItem = selectedItem;
    },
    ['setPending'](state, value) {
        state.pending = value;
    },
    ['setTitle'](state, value) {
        state.selectedItem.title = value;
    },
    ['setName'](state, value) {
        state.selectedItem.name = value;
    },
    ['setCargoOwnersPermission'](state, value) {
        state.selectedItem.cargo_owners_permission = value;
    },
    ['setDriversPermission'](state, value) {
        state.selectedItem.drivers_permission = value;
    },
    ['setCarriersPermission'](state, value) {
        state.selectedItem.carriers_permission = value;
    },
    ['setMarketersPermission'](state, value) {
        state.selectedItem.marketers_permission = value;
    },
    ['setMessagesPermission'](state, value) {
        state.selectedItem.messages_permission = value;
    },
    ['setVehiclePermission'](state, value) {
        state.selectedItem.vehicle_permission = value;
    },
    ['setCargoTypePermission'](state, value) {
        state.selectedItem.cargo_type_permission = value;
    },
    ['setDiscountPermission'](state, value) {
        state.selectedItem.discount_permission = value;
    },
    ['setSettingPermission'](state, value) {
        state.selectedItem.setting_permission = value;
    },
    ['setOrderReportPermission'](state, value) {
        state.selectedItem.order_report_permission = value;
    },
    ['setTransactionReportPermission'](state, value) {
        state.selectedItem.transaction_report_permission = value;
    },
    ['setDashboardPermission'](state, value) {
        state.selectedItem.dashboard_permission = value;
    },
    ['setAdvertisingBoxPermission'](state, value) {
        state.selectedItem.advertising_box_permission = value;
    },
    ['setStaticContentPermission'](state, value) {
        state.selectedItem.static_content_permission = value;
    },
    ['setRolePermission'](state, value) {
        state.selectedItem.role_permission = value;
    },
    ['setOperatorPermission'](state, value) {
        state.selectedItem.operator_permission = value;
    },
    ['setRegulationPermission'](state, value) {
        state.selectedItem.regulation_permission = value;
    },
    ['setAllDataLoading'](state, value) {
        state.allDataLoading = value;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
