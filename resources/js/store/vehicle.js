import {readVehicles, addOrEditVehicle, deleteVehicle} from "../data/api";

const state = {
    allData: [],
    allVehicleGroups: [],
    allDataLoading: false,
    pending: false,
    AddDialog: false,
    DeleteDialog: false,
    AlertDialog: false,
    alertDialogMessage: "",
    selectedVehicleGroup: {id: null, name: null},
    selectedItem: {
        id: null,
        name: null,
        max_capacity: null,
        vehicle_group: {id: null, name: null}
    },
};

// getters
const getters = {};

// actions
const actions = {
    readData({commit}) {
        commit('setAllDataLoading', true);
        readVehicles().then(jsonResponse => {
            console.log(jsonResponse.data.data.vehicle);
            commit('setData', jsonResponse.data.data.vehicle);
            commit('setVehicleGroup', jsonResponse.data.data.vehicle_group);
            commit('setAllDataLoading', false);
        }).catch(e => console.log(e));
    },
    addOrEdit({commit, dispatch}) {
        commit('setPending', true);
        addOrEditVehicle().then(jsonResponse => {
            commit('setPending', false);
            if (jsonResponse.data.status === 200) {
                commit('hideAddDialog')
                dispatch('readData')
            }
        }).catch(e => {
            commit('setPending', false)
        });
    },
    deleteOne({commit, dispatch}) {
        commit('setPending', true);
        deleteVehicle().then(jsonResponse => {
            commit('setPending', false);
            commit('hideDeleteDialog')
            if (jsonResponse.data.status === 200) {
                dispatch('readData')
            } else if (jsonResponse.data.status === 226){
                commit('showAlertDialog' ,jsonResponse.data.data);
            }
        });
    },
};

// mutations
const mutations = {
    ['setData'](state, data) {
        state.allData = data;
    },
    ['setVehicleGroup'](state, data) {
        state.allVehicleGroups = data;
    },
    ['showAlertDialog'](state , data) {
        state.AlertDialog = true;
        state.alertDialogMessage = data;
    },
    ['hideAlertDialog'](state) {
        state.AlertDialog = false;
    },
    ['showAddDialog'](state) {
        state.AddDialog = true;
    },
    ['hideAddDialog'](state) {
        state.AddDialog = false;
    },
    ['showDeleteDialog'](state) {
        state.DeleteDialog = true;
    },
    ['hideDeleteDialog'](state) {
        state.DeleteDialog = false;
    },
    ['setSelectedItem'](state, selectedItem) {
        state.selectedItem = selectedItem;
    },
    ['setPending'](state, value) {
        state.pending = value;
    },
    ['setTitle'](state, value) {
        state.selectedItem.title = value;
    },
    ['setName'](state, value) {
        state.selectedItem.name = value;
    },
    ['setMaxCapacity'](state, value) {
        state.selectedItem.max_capacity = value;
    },

    ['setSelectedVehicleGroup'](state, value) {
        state.selectedItem.vehicle_group = value;
    },
    ['setAllDataLoading'](state, value) {
        state.allDataLoading = value;
    },
    ['showAlertDialog'](state , data) {
        state.AlertDialog = true;
        state.alertDialogMessage = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
