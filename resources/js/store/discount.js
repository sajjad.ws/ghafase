import {readDiscounts,addOrEditDiscount,deleteDiscount} from "../data/api";

const state = {
    allData: [],
    discountType:[{name:'ریالی',type:0},{name:'درصدی',type:1}],
    cargoType:[],
    vehicleType:[],
    allDataLoading: false,
    pending: false,
    AddDialog: false,
    DeleteDialog:false,
    selectedItem:{
        id:null,
        title:null,
        start_time:null,
        end_time:null,
        discount_type:null,
        discount_type_model:null,
        status:null,
        discount_value:null,
        for_carriers:null,
        for_drivers:null,
        for_cargo_owners:null,
        vehicle:null,
        cargo:null,
    }
};

// getters
const getters = {};

// actions
const actions = {
    readData({commit}) {
        commit('setAllDataLoading', true);
        readDiscounts().then(jsonResponse => {
            commit('setData', jsonResponse.data.data.discount);
            commit('setCargo', jsonResponse.data.data.cargo);
            commit('setVehicle', jsonResponse.data.data.vehicle);
            console.log(jsonResponse.data.data);
            commit('setAllDataLoading', false);
        }).catch(e=>console.log(e));
    },
    addOrEdit({commit,dispatch}){
        commit('setPending',true);
        addOrEditDiscount().then(jsonResponse => {
            commit('setPending',false);
            if (jsonResponse.data.status === 200){
                commit('hideAddDialog')
                dispatch('readData')
            }
        }).catch(e => {
            commit('setPending', false);
        });
    },
    deleteOne({commit,dispatch}){
        commit('setPending',true);
        deleteDiscount().then(jsonResponse => {
            commit('setPending',false);
            commit('hideDeleteDialog')
            if (jsonResponse.data.status === 200){
                dispatch('readData')
            }
        });
    },
};

// mutations
const mutations = {
    ['setData'](state, data) {
        state.allData = data;
    },
    ['setCargo'](state, data) {
        state.cargoType = data;
    },
    ['setVehicle'](state, data) {
        state.vehicleType = data;
    },
    ['setCargoType'](state, data) {
        state.selectedItem.cargo = data;
    },
    ['setVehicleType'](state, data) {
        state.selectedItem.vehicle = data;
    },
    ['showAddDialog'](state) {
        state.AddDialog = true;
    },
    ['hideAddDialog'](state) {
        state.AddDialog = false;
    },
    ['showDeleteDialog'](state) {
        state.DeleteDialog = true;
    },
    ['hideDeleteDialog'](state) {
        state.DeleteDialog = false;
    },
    ['setSelectedItem'](state, selectedItem) {
        state.selectedItem = selectedItem;
    },
    ['setPending'](state, value) {
        state.pending = value;
    },
    ['setTitle'](state, value) {
        state.selectedItem.title = value;
    },
    ['setTitle'](state, value) {
        state.selectedItem.title = value;
    },
    ['setDiscountType'](state, value) {
        state.selectedItem.discount_type = value;
    },
    ['setStartTime'](state, value) {
        state.selectedItem.start_time = value;
    },
    ['setEndTime'](state, value) {
        state.selectedItem.end_time = value;
    },
    ['setStatus'](state, value) {
        state.selectedItem.status = value;
    },
    ['setDiscountValue'](state, value) {
        state.selectedItem.discount_value = value;
    },
    ['setForCarrier'](state, value) {
        state.selectedItem.for_carriers = value;
    },
    ['setForDriver'](state, value) {
        state.selectedItem.for_drivers = value;
    },
    ['setForCargoOwner'](state, value) {
        state.selectedItem.for_cargo_owners = value;
    },
    ['setDiscountTypeModel'](state, value) {
        state.selectedItem.discount_type_model = value;
    },
    ['setAllDataLoading'](state, value) {
        state.allDataLoading = value;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
