import {sendRoute} from "../../data/api";

const state = {
    centerOrigin: null,
    centerDestination: null,
    api_token : null,
};

// getters
const getters = {

};

// actions
const actions = {
    sendRoute({commit}) {
        commit('setPending',true);
        sendRoute().then(jsonResponse => {
            commit('setPending',false);
            alert('ایمیل ارسال شد')
        });
    },
};

// mutations
const mutations = {
    ['setCenterOrigin'](state,val) {
        state.centerOrigin = val;
    },
    ['setCenterDestination'](state,val) {
        state.centerDestination = val;
    },
    ['setApiToken'](state,val) {
        state.api_token = val;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
