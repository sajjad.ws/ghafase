export function saveToken(token) {
    return localStorage.setItem("token", "Bearer " + token);
}

export function removeToken() {
    return localStorage.removeItem("token");
}

export function getToken() {
    return localStorage.getItem("token")
}

export function saveMarketerID(marketerID) {
    console.log(marketerID);
    return localStorage.setItem("marketer_id", marketerID);
}

export function getMarketerID() {
    return localStorage.getItem("marketer_id")
}