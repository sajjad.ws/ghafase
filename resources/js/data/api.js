import axios from "axios";
import {getToken, getMarketerID} from "../data/localStorage";
import store from '../store/index';
import {convertDateTimeToGregorian} from "./convertors";

export function post(url, creds) {
    return axios.post(url, creds, {
            headers: {
                Authorization: getToken()
            }
        }
    );

}

export function get(url) {
    return axios.get(url, {
            headers: {
                Authorization: getToken()
            }
        }
    );
}

export function getCityByProvince(pid) {
    console.log(pid);
    return post('/api/v1/panel/get-city-by-province', {
        pid: pid
    });
}

export function fetchLogin() {
    let state = store.state.login;
    return post('/api/v1/panel/login', {
        email: state.username,
        password: state.password
    });
}

export function resetPassword() {
    let state = store.state.resetPassword;
    return post('/api/v1/panel/change-password', {
        username: state.username,
        password: state.password
    });
}

export function sendMail() {
    let state = store.state.forgetPassword;
    return post('/api/v1/panel/send-mail', {
        email: state.email,
    });
}

export function readCargoTypes() {
    return post('/api/v1/panel/read-cargo-types', null);
}

export function addOrEditCargoType() {
    let state = store.state.cargoType;
    console.log(state);
    return post('/api/v1/panel/add-edit-cargo-type', {
        id: state.selectedItem.id,
        name: state.selectedItem.name,
        commission_amount: state.selectedItem.commission_amount,
    });
}

export function deleteCargoType() {
    let state = store.state.cargoType;
    return post('/api/v1/panel/delete-cargo-type', {
        id: state.selectedItem.id,
    });
}

export function readVehicleGroups() {
    return post('/api/v1/panel/read-vehicle-group', null);
}

export function addOrEditVehicleGroup() {
    let state = store.state.vehicleGroup;
    console.log(state);
    return post('/api/v1/panel/add-edit-vehicle-group', {
        id: state.selectedItem.id,
        name: state.selectedItem.name,
        commission_amount: state.selectedItem.commission_amount,
    });
}

export function deleteVehicleGroup() {
    let state = store.state.vehicleGroup;
    return post('/api/v1/panel/delete-vehicle-group', {
        id: state.selectedItem.id,
    });
}

export function readVehicles() {
    return post('/api/v1/panel/read-vehicles', null);
}

export function addOrEditVehicle() {
    let state = store.state.vehicle;
    console.log(state);
    return post('/api/v1/panel/add-edit-vehicle', {
        id: state.selectedItem.id,
        name: state.selectedItem.name,
        max_capacity: state.selectedItem.max_capacity,
        vehicle_group_id: state.selectedItem.vehicle_group.id,
    });
}

export function deleteVehicle() {
    let state = store.state.vehicle;
    return post('/api/v1/panel/delete-vehicle', {
        id: state.selectedItem.id,
    });
}

export function readRoles() {
    return post('/api/v1/panel/read-roles', null);
}

export function addOrEditRole() {
    let state = store.state.role;
    console.log(state);
    return post('/api/v1/panel/add-edit-role', {
        id: state.selectedItem.id,
        name: state.selectedItem.name,
        cargo_owners_permission: state.selectedItem.cargo_owners_permission,
        drivers_permission: state.selectedItem.drivers_permission,
        carriers_permission: state.selectedItem.carriers_permission,
        marketers_permission: state.selectedItem.marketers_permission,
        messages_permission: state.selectedItem.messages_permission,
        vehicle_permission: state.selectedItem.vehicle_permission,
        cargo_type_permission: state.selectedItem.cargo_type_permission,
        discount_permission: state.selectedItem.discount_permission,
        setting_permission: state.selectedItem.setting_permission,
        order_report_permission: state.selectedItem.order_report_permission,
        transaction_report_permission: state.selectedItem.transaction_report_permission,
        dashboard_permission: state.selectedItem.dashboard_permission,
        advertising_box_permission: state.selectedItem.advertising_box_permission,
        static_content_permission: state.selectedItem.static_content_permission,
        role_permission: state.selectedItem.role_permission,
        operator_permission: state.selectedItem.operator_permission,
        regulation_permission: state.selectedItem.regulation_permission,
    });
}

export function deleteRole() {
    let state = store.state.role;
    return post('/api/v1/panel/delete-role', {
        id: state.selectedItem.id,
    });
}

export function readOperators() {
    return post('/api/v1/panel/read-operators', null);
}

export function addOrEditOperator() {
    let state = store.state.operator;
    console.log(state);
    return post('/api/v1/panel/add-edit-operator', {
        id: state.selectedItem.id,
        first_name: state.selectedItem.first_name,
        last_name: state.selectedItem.last_name,
        email: state.selectedItem.email,
        password: state.selectedItem.password,
        role_id: state.selectedItem.role.id,

    });
}

export function deleteOperator() {
    let state = store.state.operator;
    return post('/api/v1/panel/delete-operator', {
        id: state.selectedItem.id,
    });
}


export function readAdvertises() {
    return post('/api/v1/panel/read-advertising-boxes', null);
}

export function addOrEditAdvertise() {
    let state = store.state.advertise;
    const formData = new FormData();
    console.log(state);
    if (state.selectedItem.id !== null) formData.append('id', state.selectedItem.id);
    formData.append('title', state.selectedItem.title);
    formData.append('size', state.selectedItem.size);
    formData.append('image', state.selectedItem.image);
    return post('/api/v1/panel/add-edit-advertising-box', formData);
}

export function deleteAdvertise() {
    let state = store.state.advertise;
    return post('/api/v1/panel/delete-advertising-box', {
        id: state.selectedItem.id,
    });
}


export function readDiscounts() {
    return post('/api/v1/panel/read-discounts', null);
}

export function addOrEditDiscount() {
    let state = store.state.discount;
    return post('/api/v1/panel/add-edit-discount', {
        id: state.selectedItem.id,
        title: state.selectedItem.title,
        start_time: state.selectedItem.start_time,
        end_time: state.selectedItem.end_time,
        discount_type: state.selectedItem.discount_type_model.type,
        status: state.selectedItem.status,
        discount_value: state.selectedItem.discount_value,
        for_carriers: state.selectedItem.for_carriers,
        for_drivers: state.selectedItem.for_drivers,
        for_cargo_owners: state.selectedItem.for_cargo_owners,
        vehicle: state.selectedItem.vehicle,
        cargo: state.selectedItem.cargo,

    });
}

export function deleteDiscount() {
    let state = store.state.discount;
    return post('/api/v1/panel/delete-discount', {
        id: state.selectedItem.id,
    });
}

export function readMessages() {
    let state = store.state.message;
    return post('/api/v1/panel/read-messages', {
        start_time: state.filter.start_time !== null ? convertDateTimeToGregorian(state.filter.start_time) : null,
        end_time: state.filter.end_time !== null ? convertDateTimeToGregorian(state.filter.end_time) : null,
    });
}

export function addOrEditMessage() {
    let state = store.state.message;
    return post('/api/v1/panel/add-edit-message', {
        id: state.selectedItem.id,
        title: state.selectedItem.title,
        content: state.selectedItem.content,
        for_marketers: state.selectedItem.for_marketers,
        for_carriers: state.selectedItem.for_carriers,
        for_drivers: state.selectedItem.for_drivers,
        for_cargo_owners: state.selectedItem.for_cargo_owners,
    });
}

export function deleteMessage() {
    let state = store.state.message;
    return post('/api/v1/panel/delete-message', {
        id: state.selectedItem.id,
    });
}

export function ChangeStatusMessage() {
    let state = store.state.message;
    return post('/api/v1/panel/change-status-message', {
        id: state.selectedItem.id,
        status: state.selectedItem.status,
    });
}

export function readMarketers() {
    let state = store.state.marketer;
    return post('/api/v1/panel/read-marketers', {
        search: state.filter.search == null ? null : state.filter.search
    });
}

export function addOrEditMarketer() {
    let state = store.state.marketer;
    console.log(state);
    return post('/api/v1/panel/add-edit-marketer', {
        id: state.selectedItem.id,
        first_name: state.selectedItem.first_name,
        last_name: state.selectedItem.last_name,
        username: state.selectedItem.username,
        password: state.selectedItem.password,
        phone_number: state.selectedItem.phone_number,
        national_code: state.selectedItem.national_code,
    });
}

export function deleteMarketer() {
    let state = store.state.marketer;
    return post('/api/v1/panel/delete-marketer', {
        id: state.selectedItem.id,
    });
}

export function ChangeStatusMarketer() {
    let state = store.state.marketer;
    return post('/api/v1/panel/change-status-marketer', {
        id: state.selectedItem.id,
        status: state.selectedItem.status,
    });
}

export function readTransactions() {

    let state = store.state.transaction;
    let user_id = null;
    let user_type = null;
    if (state.filter.owner !== null) {
        let owner = state.filter.owner;
        user_id = owner.id;
        if (owner.hasOwnProperty("freight_name")) user_type = 2;
        else user_type = 0;
    }
    if (state.filter.driver !== null) {
        let driver = state.filter.driver;
        user_id = driver.id;
        user_type = 1;
    }
    if (state.filter.carrier !== null) {
        let carrier = state.filter.carrier;
        user_id = carrier.id;
        user_type = 2;
    }
    return post('/api/v1/panel/read-credit-transactions', {
        start_time: state.filter.start_time !== null ? convertDateTimeToGregorian(state.filter.start_time) : null,
        end_time: state.filter.end_time !== null ? convertDateTimeToGregorian(state.filter.end_time) : null,
        origin_city_id: state.filter.originCity.id == null ? null : state.filter.originCity.id,
        destination_city_id: state.filter.destinationCity.id == null ? null : state.filter.destinationCity.id,
        user_id: user_id,
        user_type: user_type,
    });
}

export function readCarriers() {
    let state = store.state.carrier;
    return post('/api/v1/panel/read-carriers', {
        search: state.filter.search == null ? null : state.filter.search,
        selected_city_id: state.filter.selectedCity.id == null ? null : state.filter.selectedCity.id,
    });
}

export function addOrEditCarrier() {
    let state = store.state.carrier;
    console.log(state);
    return post('/api/v1/panel/add-edit-carrier', {
        id: state.selectedItem.id,
        freight_name: state.selectedItem.freight_name,
        owner_first_name: state.selectedItem.owner_first_name,
        owner_last_name: state.selectedItem.owner_last_name,
        username: state.selectedItem.username,
        password: state.selectedItem.password,
        owner_phone_number: state.selectedItem.owner_phone_number,
        telephone_number: state.selectedItem.telephone_number,
        credit: state.selectedItem.credit,
        point: state.selectedItem.point,
        marketer_id: state.selectedItem.marketer_id,
        city_id: state.selectedItem.city.id,
    });
}

export function deleteCarrier() {
    let state = store.state.carrier;
    return post('/api/v1/panel/delete-carrier', {
        id: state.selectedItem.id,
    });
}

export function ChangeStatusCarrier() {
    let state = store.state.carrier;
    console.log(state.selectedItem);
    return post('/api/v1/panel/change-status-carrier', {
        id: state.selectedItem.id,
        status: state.selectedItem.status,
    });
}


export function readCargoOwners() {
    let state = store.state.cargoOwner;
    return post('/api/v1/panel/read-cargo-owners', {
        search: state.filter.search == null ? null : state.filter.search
    });
}

export function addOrEditCargoOwner() {
    let state = store.state.cargoOwner;
    console.log(state);
    return post('/api/v1/panel/add-edit-cargo-owner', {
        id: state.selectedItem.id,
        first_name: state.selectedItem.first_name,
        last_name: state.selectedItem.last_name,
        email: state.selectedItem.email,
        address: state.selectedItem.address,
        phone_number: state.selectedItem.phone_number,
        telephone_number: state.selectedItem.telephone_number,
        national_code: state.selectedItem.national_code,
        postal_code: state.selectedItem.postal_code,
        company_name: state.selectedItem.company_name,
        latitude: state.selectedItem.latitude,
        longitude: state.selectedItem.longitude,
        credit: state.selectedItem.credit,
        marketer_id: state.selectedItem.marketer_id,
        city_id: state.selectedItem.city.id,
    });
}

export function deleteCargoOwner() {
    let state = store.state.cargoOwner;
    return post('/api/v1/panel/delete-cargo-owner', {
        id: state.selectedItem.id,
    });
}

export function ChangeStatusCargoOwner() {
    let state = store.state.cargoOwner;
    console.log(state.selectedItem);
    return post('/api/v1/panel/change-status-cargo-owner', {
        id: state.selectedItem.id,
        status: state.selectedItem.status,
    });
}

export function readDrivers() {
    let state = store.state.driver;
    return post('/api/v1/panel/read-drivers', {
        search: state.filter.search == null ? null : state.filter.search
    });
}

export function addOrEditDriver() {
    let state = store.state.driver;
    console.log(state);
    return post('/api/v1/panel/add-edit-driver', {
        id: state.selectedItem.id,
        first_name: state.selectedItem.first_name,
        last_name: state.selectedItem.last_name,
        phone_number: state.selectedItem.phone_number,
        national_code: state.selectedItem.national_code,
        credit: state.selectedItem.credit,
        point: state.selectedItem.point,
        plate1: state.selectedItem.plate1,
        plate2: state.selectedItem.plate2,
        plate3: state.selectedItem.plate3,
        plate4: state.selectedItem.plate4,
        international_plate1: state.selectedItem.international_plate1,
        international_plate2: state.selectedItem.international_plate2,
        international_plate3: state.selectedItem.international_plate3,
        international_plate4: state.selectedItem.international_plate4,
        is_aboard_eager: state.selectedItem.is_aboard_eager,
        vehicle_id: state.selectedItem.vehicle_id,
        marketer_id: state.selectedItem.marketer_id,
        city_id: state.selectedItem.city.id,
        photo_url: state.selectedItem.photo_url,
    });
}

export function deleteDriver() {
    let state = store.state.driver;
    return post('/api/v1/panel/delete-driver', {
        id: state.selectedItem.id,
    });
}

export function ChangeStatusDriver() {
    let state = store.state.driver;
    console.log(state.selectedItem);
    return post('/api/v1/panel/change-status-driver', {
        id: state.selectedItem.id,
        status: state.selectedItem.status,
    });
}


export function updateDriverProfilePhoto(id) {
    return post('/api/v1/panel/remove-photo-driver', {
        id: id,
    });
}

export function deleteDriverPhoto(id) {
    return post('/api/v1/panel/remove-photo-driver', {
        id: id,
    });
}


export function readCargos() {
    let state = store.state.cargo;
    let owner_id = null;
    let owner_type = null;
    let receiver_id = null;
    let receiver_type = null;
    if (state.filter.owner !== null) {
        let owner = state.filter.owner;
        owner_id = owner.id;
        if (owner.hasOwnProperty("freight_name")) owner_type = 1;
        else owner_type = 0;
    }
    if (state.filter.receiver !== null) {
        let receiver = state.filter.receiver;
        receiver_id = receiver.id;
        if (receiver.hasOwnProperty("freight_name")) receiver_type = 1;
        else receiver_type = 0;
    }
    return post('/api/v1/panel/read-cargo-request', {
        last_status: state.filter.status,
        start_time: state.filter.start_time !== null ? convertDateTimeToGregorian(state.filter.start_time) : null,
        end_time: state.filter.end_time !== null ? convertDateTimeToGregorian(state.filter.end_time) : null,
        owner_id: owner_id,
        owner_type: owner_type,
        receiver_id: receiver_id,
        receiver_type: receiver_type,
    });
}

export function addOrEditCargo() {
    let state = store.state.cargo;
    console.log(state);
    return post('/api/v1/panel/add-edit-cargo-request', {
        id: state.selectedItem.id,
        origin_address: state.selectedItem.origin_address,
        destination_address: state.selectedItem.destination_address,
        origin_latitude: state.selectedItem.origin_latitude,
        origin_longitude: state.selectedItem.origin_longitude,
        destination_latitude: state.selectedItem.destination_latitude,
        destination_longitude: state.selectedItem.destination_longitude,
        cargo_weight: state.selectedItem.cargo_weight,
        cargo_weight_min: state.selectedItem.cargo_weight_min,
        cargo_weight_max: state.selectedItem.cargo_weight_max,
        cargo_cost: state.selectedItem.cargo_cost,
        cargo_cost_min: state.selectedItem.cargo_cost_min,
        cargo_cost_max: state.selectedItem.cargo_cost_max,
        owner_type: state.selectedItem.owner_type,
        owner_id: state.selectedItem.owner_id,
        price: state.selectedItem.price,
        point: state.selectedItem.point,
        description: state.selectedItem.description,
        message_to_driver: state.selectedItem.message_to_driver,
        loading_start_time: state.selectedItem.loading_start_time,
        loading_end_time: state.selectedItem.loading_end_time,
        evacuation_start_time: state.selectedItem.evacuation_start_time,
        evacuation_end_time: state.selectedItem.evacuation_end_time,
        last_status: state.selectedItem.last_status,
        receiver_type: state.selectedItem.receiver_type,
        receiver_id: state.selectedItem.receiver_id,
    });
}

export function deleteCargo() {
    let state = store.state.cargo;
    return post('/api/v1/panel/delete-cargo-request', {
        id: state.selectedItem.id,
    });
}

export function cancelCargo() {
    let state = store.state.cargo;
    return post('/api/v1/panel/change-status-cargo-request', {
        id: state.selectedItem.id,
        status: 6,
    });
}

export function searchOwners(ownerPhoneNumber) {
    return post('/api/v1/panel/search-owners', {
        phone_number: ownerPhoneNumber,
    });
}

export function searchDrivers(driverPhoneNumber) {
    return post('/api/v1/panel/search-drivers', {
        phone_number: driverPhoneNumber,
    });
}

export function searchCarriers(carrierPhoneNumber) {
    return post('/api/v1/panel/search-carriers', {
        phone_number: carrierPhoneNumber,
    });
}

export function searchReceivers(receiverPhoneNumber) {
    return post('/api/v1/panel/search-receivers', {
        phone_number: receiverPhoneNumber,
    });
}


export function readSetting() {
    return post('/api/v1/panel/read-setting', null);
}

export function editSetting() {
    let state = store.state.setting;
    return post('/api/v1/panel/edit-setting', {
        id: state.selectedItem.id,
        driver_regulation: state.selectedItem.driver_regulation,
        carrier_regulation: state.selectedItem.carrier_regulation,
        cargo_registration_regulation: state.selectedItem.cargo_registration_regulation,
        driver_confirmation_regulation: state.selectedItem.driver_confirmation_regulation,
        cost_estimation_status: state.selectedItem.cost_estimation_status,
        cost_estimation_value: state.selectedItem.cost_estimation_value,
        carrier_driver_introduction: state.selectedItem.carrier_driver_introduction,
        cargo_cancelable_time: state.selectedItem.cargo_cancelable_time,
        driver_confirmation_time: state.selectedItem.driver_confirmation_time,
        system_status: state.selectedItem.system_status,
        is_active_registrar_commission: state.selectedItem.is_active_registrar_commission,
        is_active_receiver_commission: state.selectedItem.is_active_receiver_commission,
    });
}


export function readDashboard() {
    return post('/api/v1/panel/read-dashboard', null);
}

/**=============================*/

export function readDashboardMarketer() {
    return post('/api/v1/panel/read-dashboard-marketer', {
        marketer_id: store.state.marketerMain.selectedMarketer.id
    });
}

export function readCargoOwnersMarketer() {
    let state = store.state.cargoOwner;
    return post('/api/v1/panel/read-cargo-owners-marketer', {
        search: state.filter.search == null ? null : state.filter.search,
        marketer_id: getMarketerID()
    });
}

export function readCarriersMarketer() {
    let state = store.state.carrier;
    return post('/api/v1/panel/read-carriers-marketer', {
        search: state.filter.search == null ? null : state.filter.search,
        marketer_id: getMarketerID()
    });
}

export function readDriversMarketer() {
    let state = store.state.driver;
    return post('/api/v1/panel/read-drivers-marketer', {
        search: state.filter.search == null ? null : state.filter.search,
        marketer_id: getMarketerID()
    });
}

export function sendRoute() {
    let state = store.state.mapCargoAddress;
    return post('/api/v1/cargo-owner/send-route', {
        'api_token' : state.api_token,
        'origin_lat': state.centerOrigin.lat,
        'origin_lng': state.centerOrigin.lng,
        'destination_lat': state.centerDestination.lat,
        'destination_lng': state.centerDestination.lng,
    });
}
