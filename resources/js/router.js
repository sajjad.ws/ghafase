import Vue from "vue"
import VueRouter from "vue-router"
import LoginPage from './ui/pages/login';
import MainPage from './ui/pages/main';
import store from './store/index';
import CargoOwner from './ui/pages/cargoOwner';
import Advertise from './ui/pages/advertise';
import Cargo from './ui/pages/cargo';
import CargoType from './ui/pages/cargoType';
import Carrier from './ui/pages/carrier';
import Dashboard from './ui/pages/dashboard';
import Driver from './ui/pages/driver';
import Marketer from './ui/pages/marketer';
import Message from './ui/pages/message';
import Operator from './ui/pages/operator';
import Role from './ui/pages/role';
import Setting from './ui/pages/setting';
import Transaction from './ui/pages/transaction';
import Vehicle from './ui/pages/vehicle';
import Discount from './ui/pages/discount';
import VehicleGroup from './ui/pages/vehicleGroup';
import MarketerMain from './ui/pages/marketer/main';
import MarketerCargoOwner from './ui/pages/marketer/cargoOwner';
import MarketerCarrier from './ui/pages/marketer/carrier';
import MarketerDriver from './ui/pages/marketer/driver';
import MarketerDashboard from './ui/pages/marketer/dashboard';
import ForgetPassword from './ui/pages/forgetPassword';
import resetPassword from './ui/pages/resetPassword';
import cargoOwnerMapCargoAddress from './ui/pages/cargoOwner/mapCargoAddress';

Vue.use(VueRouter);

function guard(to, from, next){
    if(store.state.login.isLoggedIn === true) {
        next(); // allow to enter route
    } else{
        next('/login'); // go to '/login';
    }
}

export default new VueRouter({
    mode: 'history',

    routes: [

        {
            path: '',
            name: 'main',
            component: MainPage,
            beforeEnter: guard,
            children: [
                {
                    path: 'cargoOwner',
                    components: {
                        default: CargoOwner,
                    }
                },
                {
                    path: 'advertise',
                    components: {
                        default: Advertise,
                    }
                },
                {
                    path: 'cargoType',
                    components: {
                        default: CargoType,
                    }
                },
                {
                    path: 'carrier',
                    components: {
                        default: Carrier,
                    }
                },
                {
                    path: 'dashboard',
                    components: {
                        default: Dashboard,
                    }
                },
                {
                    path: 'driver',
                    components: {
                        default: Driver,
                    }
                },
                {
                    path: 'marketer',
                    components: {
                        default: Marketer,
                    }
                },
                {
                    path: 'message',
                    components: {
                        default: Message,
                    }
                },
                {
                    path: 'operator',
                    components: {
                        default: Operator,
                    }
                },
                {
                    path: 'role',
                    components: {
                        default: Role,
                    }
                },
                {
                    path: 'setting',
                    components: {
                        default: Setting,
                    }
                },
                {
                    path: 'transaction',
                    components: {
                        default: Transaction,
                    }
                },
                {
                    path: 'vehicle',
                    components: {
                        default: Vehicle,
                    }
                },
                {
                    path: 'vehicleGroup',
                    components: {
                        default: VehicleGroup,
                    }
                },
                {
                    path: 'discount',
                    components: {
                        default: Discount,
                    }
                },
                {
                    path: 'cargo',
                    components: {
                        default: Cargo,
                    }
                },
            ]
        },
        {
            path: '/login',
            name: 'Login',
            component: LoginPage
        },
        {
            path: '/forgetPassword',
            name: 'forgetPassword',
            components: {
                default : ForgetPassword,
            }
        },
        {
            path: '/resetPassword',
            name: 'resetPassword',
            components: {
                default : resetPassword,
            }
        },
        {
            path: '/cargo-owner-map-cargo-address',
            name: 'cargoOwnerMapCargoAddress',
            components: {
                default : cargoOwnerMapCargoAddress,
            }
        },
        {
            path: '/marketerMain',
            name: 'MarketerMain',
            component: MarketerMain,
            beforeEnter: guard,
            children: [
                {
                    path: 'marketerCargoOwner',
                    components: {
                        default: MarketerCargoOwner,
                    }
                },
                {
                    path: 'marketerDriver',
                    components: {
                        default: MarketerDriver,
                    }
                },
                {
                    path: 'marketerCarrier',
                    components: {
                        default: MarketerCarrier,
                    }
                },
                {
                    path: 'marketerDashboard',
                    components: {
                        default: MarketerDashboard,
                    }
                },
            ]
        },
    ]
})
