<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/**
 * Cargo Owner App Api
 */

use App\Driver;
use App\Http\Helpers\UploadHelper;
use Illuminate\Support\Facades\Input;

Route::post('v1/buyer/receive-phone-number', 'Api\BuyerController@receivePhoneNumber');
Route::post('v1/buyer/verify-phone-number', 'Api\BuyerController@verifyPhoneNumber');
Route::post('v1/buyer/get-products', 'Api\BuyerController@getProducts');
Route::post('v1/buyer/get-stores', 'Api\BuyerController@getStores');
Route::get('v1/buyer/get-initial-data', 'Api\BuyerController@getInitialData');

Route::middleware('auth:cargo_owner')->prefix('v1/buyer')->group(function () {
    Route::post('/edit-profile', 'Api\BuyerController@editProfile');
    Route::post('/get-cargo-owner-messages', 'Api\BuyerController@getCargoOwnerMessages');
    Route::post('/remove-messages', 'Api\BuyerController@removeMessage');
    Route::post('/edit-cargo-request', 'Api\BuyerController@editCargoRequest');
    Route::post('/add-request-by-cargo-owner', 'Api\BuyerController@addRequestByCargoOwner');
    Route::post('/get-cargo-owner-request-history', 'Api\BuyerController@getCargoOwnerRequestHistory');
    Route::post('/get-cargo-owner-transactions-history', 'Api\BuyerController@getCargoOwnerTransactionsHistory');
    Route::post('/get-cargo-owner-hover-requests', 'Api\BuyerController@getCargoOwnerHoverRequests');
    Route::post('/get-cargo-owner-active-requests', 'Api\BuyerController@getCargoOwnerActiveRequests');
    Route::post('/change-request-status', 'Api\BuyerController@changeRequestStatus');
    Route::post('/accept-driver-by-owner', 'Api\BuyerController@acceptDriverByOwner');
    Route::post('/accept-carrier-by-owner', 'Api\BuyerController@acceptCarrierByOwner');
    Route::post('/reject-driver-by-owner', 'Api\BuyerController@rejectDriverByOwner');
    Route::post('/cancel-by-owner', 'Api\BuyerController@cancelByOwner');
    Route::post('/confirm-by-owner', 'Api\BuyerController@confirmByOwner');
    Route::post('/get-price', 'Api\BuyerController@getPrice');
    Route::post('/add-comment', 'Api\BuyerController@addComment');
    Route::post('/get-driver', 'Api\BuyerController@getDriver');
    Route::post('/send-route', 'Api\BuyerController@sendRoute');

});

/**
 * Driver App Api
 */

Route::post('v1/driver/receive-phone-number', 'Api\DriverController@receivePhoneNumber');
Route::post('v1/driver/verify-phone-number', 'Api\DriverController@verifyPhoneNumber');
Route::get('v1/driver/get-initial-data', 'Api\DriverController@getInitialData');
Route::post('v1/driver/get-driver-hover-requests', 'Api\DriverController@getDriverHoverRequests');

Route::middleware('auth:driver')->prefix('v1/driver')->group(function () {
    Route::post('/edit-profile', 'Api\DriverController@editProfile');
    Route::post('/get-driver', 'Api\DriverController@getDriver');
    Route::post('/get-driver-messages', 'Api\DriverController@getDriverMessages');
    Route::post('/remove-messages', 'Api\DriverController@removeMessage');
    Route::post('/accept-request-by-driver', 'Api\DriverController@acceptRequestByDriver');
    Route::post('/get-driver-request-history', 'Api\DriverController@getDriverRequestHistory');
    Route::post('/get-driver-transactions-history', 'Api\DriverController@getDriverTransactionsHistory');
    Route::post('/get-driver-active-requests', 'Api\DriverController@getDriverActiveRequests');
    Route::post('/change-request-status', 'Api\DriverController@changeRequestStatus');
    Route::post('/accept-request-by-driver', 'Api\DriverController@acceptRequestByDriver');
    Route::post('/cancel-by-driver', 'Api\DriverController@cancelByDriver');
    Route::post('/start', 'Api\DriverController@start');
    Route::post('/end', 'Api\DriverController@end');
    Route::post('/update-profile-photo', 'Api\DriverController@updateProfilePhoto');
    Route::post('/add-comment', 'Api\DriverController@addComment');

});

/**
 * Carrier App Api
 */
Route::post('v1/carrier/add-carrier', 'Api\CarrierController@addCarrier');
Route::post('v1/carrier/login', 'Api\CarrierController@logIn');
Route::post('v1/carrier/forget-password', 'Api\CarrierController@forgetPassword');
Route::get('v1/carrier/get-initial-data', 'Api\CarrierController@getInitialData');
Route::post('v1/carrier/get-receiver-carrier-hover-requests', 'Api\CarrierController@getReceiverCarrierHoverRequests');

Route::post('v1/carrier/get-carrier-hover-requests', 'Api\CarrierController@getCarrierHoverRequests');

Route::post('v1/test', function (){

    $image = Input::file('equipment_url');

    //dd($image);
    try {
        $image = UploadHelper::uploadFile($image, 'driver_photo');
        return $image;
    } catch (\Exception $e) {
        return $e;
    }


});

Route::middleware('auth:carrier')->prefix('v1/carrier')->group(function () {
    Route::post('/edit-profile', 'Api\CarrierController@editProfile');
    Route::post('/get-carrier', 'Api\CarrierController@getCarrier');
    Route::post('/edit-cargo-request', 'Api\CarrierController@editCargoRequest');
    Route::post('/add-request-by-carrier', 'Api\CarrierController@addRequestByCarrier');
    Route::post('/get-carrier-messages', 'Api\CarrierController@getCarrierMessages');
    Route::post('/remove-messages', 'Api\CarrierController@removeMessage');
    Route::post('/get-carrier-request-history-owner', 'Api\CarrierController@getCarrierRequestHistoryOwner');
    Route::post('/get-carrier-request-history-receiver', 'Api\CarrierController@getCarrierRequestHistoryReceiver');
    Route::post('/get-carrier-transactions-history', 'Api\CarrierController@getCarrierTransactionsHistory');
    Route::post('/get-carrier-active-requests', 'Api\CarrierController@getCarrierActiveRequests');
    Route::post('/get-owner-carrier-hover-requests', 'Api\CarrierController@getOwnerCarrierHoverRequests');
    Route::post('/get-owner-carrier-active-requests', 'Api\CarrierController@getOwnerCarrierActiveRequests');
    Route::post('/change-request-status', 'Api\CarrierController@changeRequestStatus');
    Route::post('/accept-driver-by-owner', 'Api\CarrierController@acceptDriverByOwner');
    Route::post('/accept-carrier-by-owner', 'Api\CarrierController@acceptCarrierByOwner');
    Route::post('/reject-driver-by-owner', 'Api\CarrierController@rejectDriverByOwner');
    Route::post('/cancel-by-owner', 'Api\CarrierController@cancelByOwner');
    Route::post('/cancel-by-carrier', 'Api\CarrierController@cancelByCarrier');
    Route::post('/get-price', 'Api\CarrierController@getPrice');
    Route::post('/start', 'Api\CarrierController@start');
    Route::post('/end', 'Api\CarrierController@end');
    Route::post('/confirm-by-owner', 'Api\CarrierController@confirmByOwner');
    Route::post('/accept-request-by-carrier', 'Api\CarrierController@acceptRequestByCarrier');
    Route::post('/introduction-driver-by-carrier', 'Api\CarrierController@introductionDriverByCarrier');
    Route::post('/add-comment-owner', 'Api\CarrierController@addCommentOwner');
    Route::post('/add-comment-carrier', 'Api\CarrierController@addCommentCarrier');
    Route::post('/get-driver', 'Api\CarrierController@getDriver');
    //
    Route::post('/get-receiver-carrier-active-requests', 'Api\CarrierController@getReceiverCarrierActiveRequests');
    Route::post('/get-owner-carrier-hover-requests', 'Api\CarrierController@getOwnerCarrierHoverRequests');
    Route::post('/get-owner-carrier-active-requests', 'Api\CarrierController@getOwnerCarrierActiveRequests');
});

/**
 * Marketer App Api
 */
Route::post('v1/marketer/add-marketer-test', 'Api\MarketerController@addMarketerTest');
Route::post('v1/marketer/login', 'Api\MarketerController@logIn');
Route::post('v1/marketer/forget-password', 'Api\MarketerController@forgetPassword');
Route::get('v1/marketer/get-initial-data', 'Api\MarketerController@getInitialData');

Route::middleware('auth:marketer')->prefix('v1/marketer')->group(function () {
    Route::post('/edit-profile', 'Api\MarketerController@editProfile');
    Route::post('/get-marketer', 'Api\MarketerController@getMarketer');
    Route::post('/add-cargo-owner', 'Api\MarketerController@addCargoOwner');
    Route::post('/get-cargo-owners-by-marketer', 'Api\MarketerController@getCargoOwnersByMarketer');
    Route::post('/add-driver', 'Api\MarketerController@addDriver');
    Route::post('/get-drivers-by-marketer', 'Api\MarketerController@getDriversByMarketer');
    Route::post('/add-carrier', 'Api\MarketerController@addCarrier');
    Route::post('/get-carriers-by-marketer', 'Api\MarketerController@getCarriersByMarketer');
    Route::post('/get-cargo-owner-request-history-by-marketer', 'Api\MarketerController@getCargoOwnerRequestHistoryByMarketer');
    Route::post('/get-driver-request-history-by-marketer', 'Api\MarketerController@getDriverRequestHistoryByMarketer');
    Route::post('/get-carrier-request-history-by-marketer', 'Api\MarketerController@getCarrierRequestHistoryByMarketer');
    Route::post('/get-marketer-messages', 'Api\MarketerController@getMarketerMessages');
    Route::post('/remove-messages', 'Api\MarketerController@removeMessage');

});

/**
 * Panel Api
 */
Route::post('v1/panel/register', 'Api\AdminController@register');
Route::post('v1/panel/login', 'Api\AdminController@login');
Route::post('v1/panel/send-mail', 'Api\AdminController@sendMail');
Route::post('v1/panel/change-password', 'Api\AdminController@changePassword');

Route::middleware('auth:operator')->prefix('v1/panel')->group(function () {
    Route::post('/get-city-by-province', 'Api\AdminController@getCityByProvince');

    Route::post('/read-drivers', 'Api\AdminController@readDrivers');
    Route::post('/add-edit-driver', 'Api\AdminController@addOrEditDriver');
    Route::post('/delete-driver', 'Api\AdminController@deleteDriver');
    Route::post('/change-status-driver', 'Api\AdminController@changeStatusDriver');
    Route::post('/update-driver-profile-photo', 'Api\AdminController@updateDriverProfilePhoto');
    Route::post('/remove-photo-driver', 'Api\AdminController@removePhotoDriver');

    Route::post('/read-cargo-owners', 'Api\AdminController@readCargoOwners');
    Route::post('/add-edit-cargo-owner', 'Api\AdminController@addOrEditCargoOwner');
    Route::post('/delete-cargo-owner', 'Api\AdminController@deleteCargoOwner');
    Route::post('/change-status-cargo-owner', 'Api\AdminController@changeStatusCargoOwner');

    Route::post('/read-carriers', 'Api\AdminController@readCarriers');
    Route::post('/add-edit-carrier', 'Api\AdminController@addOrEditCarrier');
    Route::post('/delete-carrier', 'Api\AdminController@deleteCarrier');
    Route::post('/change-status-carrier', 'Api\AdminController@changeStatusCarrier');

    Route::post('/read-marketers', 'Api\AdminController@readMarketers');
    Route::post('/add-edit-marketer', 'Api\AdminController@addOrEditMarketer');
    Route::post('/delete-marketer', 'Api\AdminController@deleteMarketer');
    Route::post('/change-status-marketer', 'Api\AdminController@changeStatusMarketer');

    Route::post('/read-vehicle-group', 'Api\AdminController@readVehicleGroups');
    Route::post('/add-edit-vehicle-group', 'Api\AdminController@addOrEditVehicleGroup');
    Route::post('/delete-vehicle-group', 'Api\AdminController@deleteVehicleGroup');

    Route::post('/read-vehicles', 'Api\AdminController@readVehicles');
    Route::post('/add-edit-vehicle', 'Api\AdminController@addOrEditVehicle');
    Route::post('/delete-vehicle', 'Api\AdminController@deleteVehicle');

    Route::post('/read-cargo-request', 'Api\AdminController@readCargoRequests');
    Route::post('/add-edit-cargo-request', 'Api\AdminController@addOrEditCargoRequest');
    Route::post('/delete-cargo-request', 'Api\AdminController@deleteCargoRequest');
    Route::post('/change-status-cargo-request', 'Api\AdminController@changeStatusCargoRequest');
    Route::post('/search-owners', 'Api\AdminController@searchOwners');
    Route::post('/search-receivers', 'Api\AdminController@searchReceivers');
    Route::post('/search-drivers', 'Api\AdminController@searchDrivers');
    Route::post('/search-carriers', 'Api\AdminController@searchCarriers');

    Route::post('/read-discounts', 'Api\AdminController@readDiscounts');
    Route::post('/add-edit-discount', 'Api\AdminController@addOrEditDiscount');
    Route::post('/delete-discount', 'Api\AdminController@deleteDiscount');
    Route::post('/change-status-discount', 'Api\AdminController@changeStatusDiscount');

    Route::post('/read-dashboard', 'Api\AdminController@readDashboard');
    Route::post('/read-setting', 'Api\AdminController@readSetting');
    Route::post('/edit-setting', 'Api\AdminController@editSetting');

    Route::post('/read-roles', 'Api\AdminController@readRoles');
    Route::post('/add-edit-role', 'Api\AdminController@addOrEditRole');
    Route::post('/delete-role', 'Api\AdminController@deleteRole');

    Route::post('/read-cargo-types', 'Api\AdminController@readCargoTypes');
    Route::post('/add-edit-cargo-type', 'Api\AdminController@addOrEditCargoType');
    Route::post('/delete-cargo-type', 'Api\AdminController@deleteCargoType');

    Route::post('/read-operators', 'Api\AdminController@readOperators');
    Route::post('/add-edit-operator', 'Api\AdminController@addOrEditOperator');
    Route::post('/delete-operator', 'Api\AdminController@deleteOperator');
    Route::post('/change-status-operator', 'Api\AdminController@changeStatusOperator');

    Route::post('/read-advertising-boxes', 'Api\AdminController@readAdvertisingBoxes');
    Route::post('/add-edit-advertising-box', 'Api\AdminController@addOrEditAdvertisingBox');
    Route::post('/delete-advertising-box', 'Api\AdminController@deleteAdvertisingBox');

    Route::post('/read-credit-transactions', 'Api\AdminController@readCreditTransactions');
    Route::post('/add-edit-credit-transaction', 'Api\AdminController@addOrEditCreditTransaction');
    Route::post('/delete-credit-transaction', 'Api\AdminController@deleteCreditTransaction');

    Route::post('/read-messages', 'Api\AdminController@readMessages');
    Route::post('/add-edit-message', 'Api\AdminController@addOrEditMessage');
    Route::post('/delete-message', 'Api\AdminController@deleteMessage');
    Route::post('/change-status-message', 'Api\AdminController@changeStatusMessage');
    //
    Route::post('/read-dashboard-marketer', 'Api\AdminController@readDashboardMarketer');
    Route::post('/read-cargo-owners-marketer', 'Api\AdminController@readCargoOwnersMarketer');
    Route::post('/read-carriers-marketer', 'Api\AdminController@readCarriersMarketer');
    Route::post('/read-drivers-marketer', 'Api\AdminController@readDriversMarketer');

});


