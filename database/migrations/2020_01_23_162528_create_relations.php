<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->foreign('category_id',"fk_products_category_id")->references('id')->on('categories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('store_id',"fk_products_store_id")->references('id')->on('stores')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->foreign('buyer_id',"fk_orders_buyer_id")->references('id')->on('buyers')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('store_id',"fk_orders_store_id")->references('id')->on('stores')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('order_product', function (Blueprint $table) {
            $table->foreign('order_id',"fk_order_product_order_id")->references('id')->on('orders')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('product_id',"fk_order_product_product_id")->references('id')->on('products')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('buyers', function (Blueprint $table) {
            $table->foreign('city_id',"fk_buyers_city_id")->references('id')->on('cities')->onDelete('cascade')->onUpdate('cascade');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relations');
    }
}
